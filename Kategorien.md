# Navigation

Kategorie|Fragen
---|---
Berichtigung von Karten|1, 4-8, 37-39, 42
Karten, Bücher, etc.|15, 16, 29, 40, 65, 83
WGS 84|2, 3, 5, 99, 101
GPS|2, 45, 46, 89-99, 101
Kompass|51, 115-118
Feuer|11-14, 18-28, 68
Schifffahrtszeichen|12, 13, 64, 66-69
NfS|9, 10, 36, 37-39
BfS|30-32, 65
NWN|33-35, 65
Gezeiten, Wassertiefe und Strom|44, 52, 70-88
Kurse|17, 41, 46, 47, 56-62
Orte|48, 49, 100
Loten|54, 55, 63, 77, 86
Peilen|50, 54, 55, 103
Radar|102, 103, 104, 112
AIS|105-112
Sonstiges|43, 53
Ausrüstung| 113, 114

# Schifffahrtsrecht

Kategorie|Fragen
---|---
Alkohol|2-4, 83, 84
Unfall|91-95
Flaggen|86, 99, 100
Schutzzonen|101, 102
Müll|103-110

# Seemannschaft I

Kategorie|Fragen
---|---
Rumpfmaterial|1-4, 82
CE-Kennzeichen|5-7
Rigg, Ruder, etc.|8-11
Segel|12-20
Lenzepumpe|21, 22, 32
Tauwerk|24, 25, 74-79
Ersatzteile und Werkzeug|26-29, 71
Notfall-Ausrüstung|30-32
WC|34, 35, 129
Krängung|36-40, 43-45
Scheinbarer Wind|41, 42
Segeltrimm|44-52
Pflege des Unterwasserschiffes|53-54, 80, 82
Motor|55-71
Elektro|72, 73, 81
Anlegen|78, 79, 89, 90, 95-101, 106
Groß bedienen|84-86
Schweres Wetter|101-105
MOB|109-113
Sicherheitsausrüstung|114-119
Feuer|33, 120-123
Vorbeitung vor dem Auslaufen|124-130
Seenot|131-139
Ankern|23, 130, 141-156
Nähe zu großen Schiffen|157-163
sonstiges|83
