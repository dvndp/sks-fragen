# Nummer 1:

`Worauf müssen Sie als Schiffsführer vor Reiseantritt hinsichtlich der Seekarten und Seebücher achten?`

Auf Vollständigkeit der Unterlagen und deren Berichtigung auf den neuesten Stand.

# Nummer 2:

`Warum muss in der GPS-Navigation das jeweilige Kartendatum unbedingt berücksichtigt werden?`

Weil sich das von GPS verwendete Bezugssystem WGS 84 (World Geodetic System 1984) von anderen verwendeten Bezugssystemen (Kartendatum) unterscheiden kann.

# Nummer 3:

`Welche Differenzen können zwischen WGS 84 und anderen Bezugssystemen auftreten?`

Die Differenzen von φ und λ liegen im Allgemeinen in der Größenordnung von 0,1 kbl bis 1 kbl, also etwa von 20 bis 200 m. Es können größere Unterschiede auftreten.

# Nummer 4:

`Wo finden Sie in der Seekarte Angaben über das benutzte Bezugssystem und ggf. entsprechende Korrekturhinweise?`

Am Kartenrand unter dem Titel.

# Nummer 5:

`Wie lautet ggf. der Korrekturhinweis bezüglich GPS in der Seekarte, wenn das benutzte Kartendatum (z. B. ED 50) und WGS 84 nicht übereinstimmen?`

Durch Satellitennavigation (z. B. GPS) erhaltene Positionen im WGS 84 sind 0, ... Minuten nordwärts/südwärts und 0, ... Minuten westwärts/ostwärts zu verlegen, um mit dieser Karte übereinzustimmen.

# Nummer 6:

`Woran erkennen Sie, bis wann eine deutsche Seekarte "amtlich" berichtigt ist?`

Am Berichtigungsstempel des BSH oder einer amtlichen Seekartenberichtigungsstelle.

# Nummer 7:

`Woran erkennen Sie, bis wann eine britische Seekarte "amtlich" berichtigt ist?`

Am Berichtigungsstempel auf der Rückseite der Seekarte.

# Nummer 8:

`Was bedeutet der Stempel auf der britischen Seekarte: Corrected up to N.T.M. 3595 1998?`

Seekarte ist berichtigt bis zur Mitteilung Nummer 3595 der Admiralty Notices to Mariners (N.T.M.) in 1998.

# Nummer 9:

`Welche Angaben enthalten die Nachrichten für Seefahrer (NfS)?`

In den NfS werden für die sichere Schiffsführung wichtige Maßnahmen, Ereignisse und Veränderungen auf den Seeschifffahrtsstraßen, auf der Hohen See sowie in den Hoheitsgewässern anderer Staaten im europäischen und angrenzenden Bereich bekannt gegeben.

# Nummer 10:

`In welcher Sprache werden die Nachrichten für Seefahrer (NfS) verfasst?`

Die Angaben erfolgen in deutscher und in englischer Sprache.

# Nummer 11:

`Welche Angaben enthalten deutsche und britische Leuchtfeuerverzeichnisse?`

Beschreibung der Leuchtfeuer, Feuerschiffe und Großtonnen sowie deren geographische Lage.

# Nummer 12:

`Welche schwimmenden Schifffahrtszeichen werden in der britischen List of Lights und in deutschen Leuchtfeuerverzeichnissen nicht angegeben?`

Tonnen kleiner als 8 m Höhe.

# Nummer 13:

`Wo finden Sie Angaben über die Merkmale der Schifffahrtszeichen?`

In den Leuchtfeuerverzeichnissen bzw. in der List of Lights sowie auszugsweise in den Seekarten.

In der Karte 1/INT 1 des BSH.

Schwimmende Schifffahrtszeichen zusätzlich in der Anlage I zur SeeSchStrO (z. B. Tonnen des Lateral- bzw. Kardinalsystems).

# Nummer 14:

`Worauf beziehen sich die Höhenangaben der Leuchtfeuer in Leuchtfeuerverzeichnissen in der Nord- und Ostsee?`

In Gewässern mit Gezeiten (z. B. Nordsee) auf mittleres Hochwasser, in gezeitenlosen Gewässern (z. B. Ostsee) auf mittleren Wasserstand.

# Nummer 15:

`Wo finden Sie Angaben über Brückensignale?`

In den See- und Hafenhandbüchern und in den Seekarten.

# Nummer 16:

`Welche Themen (Grobgliederung) enthalten die Seehandbücher des BSH?`

Schifffahrtsangelegenheiten,

Naturverhältnisse,

Küstenkunde und Segelanweisungen.

# Nummer 17:

`Wie werden Richtungsangaben in nautischen Veröffentlichungen gemacht?`

Rechtweisend in Grad,

im Uhrzeigersinn (rechtsherum) zählend.

# Nummer 18:

`In welchem Quadranten liegt der rote Warnsektor eines Leitfeuers mit der Angabe rot 030°-042° (Begründung!)`

Im Südwest-Quadranten. Angegeben sind die Peilungen zum Leuchtfeuer.

# Nummer 19:

`Was sind Leitfeuer (direction lights)?`

Leitfeuer sind Einzelfeuer, die durch Sektoren verschiedener Farbe oder Kennung (Leit- oder Warnsektoren) im Allgemeinen ein Fahrwasser, eine Hafeneinfahrt oder einen freien Seeraum zwischen Untiefen bezeichnen.

# Nummer 20:

`1. Was sind Richtfeuer (leading lights)? 2. Wann befindet man sich in einer Richtlinie eines Richtfeuers?`

Richtfeuer sind Feuer, die als Unter- und Oberfeuer in Deckpeilung als Richtlinie beispielsweise einen Kurs im Fahrwasser, durch eine Hafeneinfahrt oder im freien Seeraum zwischen Untiefen bezeichnen.

Ein Schiff befindet sich in der Richtlinie, wenn Unter- und Oberfeuer senkrecht unter-/übereinander erscheinen.

# Nummer 21:

`Was ist ein Torfeuer?`

Ein Torfeuer besteht aus zwei Feuern gleicher Höhe, gleicher Lichtstärke und gleicher Kennung, die zu beiden Seiten der Fahrwasserachse einander genau gegenüber (rechtwinklig zur Fahrwasserachse) und von der Fahrwasserachse gleich weit entfernt angeordnet sind.

# Nummer 22:

`Was ist die "Tragweite" eines Feuers?`

Unter Tragweite versteht man denjenigen Abstand, in dem ein Feuer einen eben noch deutlichen Lichteindruck im Auge des Beobachters hervorruft.

# Nummer 23:

`Was ist die "Nenntragweite" eines Feuers?`

Nenntragweite ist die Tragweite eines Feuers für einen definierten Wert bei einer meteorologischen Sichtweite am Tage von 10 sm.

# Nummer 24:

`Wovon hängt die "Tragweite" eines Feuers ab?`

Sie hängt u. a. ab

von der Lichtstärke (Helligkeit) des Feuers und

vom Sichtwert (Lichtdurchlässigkeit der Atmosphäre).

# Nummer 25:

`In der Seekarte finden Sie bei einem Leuchtfeuer die Eintragung: 18 M. Was bedeutet diese Angabe?`

Es ist die Nenntragweite, hier 18 Seemeilen.

# Nummer 26:

`Was ist die "Sichtweite" eines Feuers? Wovon hängt sie ab?`

Sichtweite ist die Entfernung, auf die ein Leuchtfeuer über die Erdkrümmung (Kimm) hinweg vom Beobachter gesehen werden kann.

Sie hängt ab

von der Feuerhöhe und

von der Augeshöhe des Beobachters.

# Nummer 27:

`Wie müssen sich Tragweite und Sichtweite zueinander verhalten, damit das Verfahren zur Ortsbestimmung "Feuer in der Kimm" angewandt werden kann?`

Die Tragweite muss mindestens gleich der Sichtweite sein.

# Nummer 28:

`Wo findet man Tabellen zur Ermittlung des Abstandes eines Feuers in der Kimm?`

In deutschen und britischen Leuchtfeuerverzeichnissen.

# Nummer 29:

`Wo sind die in Seekarten verwendeten Symbole und Abkürzungen erklärt?`

In der Karte 1/INT des BSH.

# Nummer 30:

`Wer veröffentlicht die Bekanntmachungen für Seefahrer (BfS) und was umfassen diese Veröffentlichungen?`

Die BfS werden von den jeweils zuständigen Behörden der Wasser- und Schifffahrtsverwaltung des Bundes bzw. der Länder veröffentlicht.

Sie enthalten alle wichtigen Maßnahmen und Ereignisse auf den Seeschifffahrtsstraßen und der ausschließlichen Wirtschaftszone Deutschlands.

# Nummer 31:

`Wie werden die Bekanntmachungen für Seefahrer (BfS) der Sportschifffahrt zur Kenntnis gebracht?`

Die BfS werden an den amtlichen Aushangstellen (z. B. bei Wasser- und Schifffahrtsämtern, Hafenverwaltungen, WSP-Dienststellen, Schleusen, Yachthäfen) für das betreffende Seegebiet, in dem die Aushangstelle liegt, und für die angrenzenden Reviere und Gebiete sowie im Internet unter www.elwis.de zur Kenntnis gebracht.

# Nummer 32:

`Zählen Sie die am häufigsten vorkommenden Ereignisse und Maßnahmen auf, über die die Bekanntmachungen für Seefahrer (BfS) unterrichten.`

Änderungen an Befeuerung, Betonnung und Landmarken,

veränderte Wassertiefen,

Wracke, Schifffahrtshindernisse, Rohrleitungen usw.,

Bauarbeiten, Baggerarbeiten, militärische Übungen und damit zusammenhängende Sperrungen oder Behinderungen.

# Nummer 33:

`Wer gibt die nautischen Warnnachrichten (NWN) heraus und von wem werden sie verbreitet?`

Nautische Warnnachrichten (NWN) werden von den Verkehrszentralen für deren Zuständigkeitsbereich und von dem ständig besetzten Seewarndienst Emden für das gesamte deutsche Warngebiet zur Verbreitung über Funk herausgegeben. Der Rundfunksender Deutschlandfunk verbreitet alle über Funk abgegebenen NWN.

# Nummer 34:

`Was bedeutet der Zusatz "vital" bei einer nautischen Warnnachricht (NWN)?`

Die NWN erhält den Zusatz "vital", wenn die Warnung auf eine lebensbedrohende Gefahr hinweist.

# Nummer 35:

`Welche Besonderheit bezüglich des Zusatzes "vital" bei einer nautischen Warnnachricht (NWN) gibt es für die Sportschifffahrt?`

Vitale nautische Warnnachrichten für die Sportschifffahrt werden während der Zeit vom 1. April bis zum 31. Oktober zur Verbreitung über ausgewählte private und öffentlich-rechtliche Rundfunkanstalten weitergeleitet.

# Nummer 36:

`Wer gibt die Nachrichten für Seefahrer (NfS) heraus und wie und wie oft erfolgt die Herausgabe?`

Die NfS werden vom BSH in Heftform und im Internet herausgegeben und erscheinen einmal wöchentlich.

# Nummer 37:

`Welche Unterlage steht Ihnen zur Verfügung zur Berichtigung von britischen Seekarten, die nicht von den NfS erfasst werden?`

Die britischen Notices to Mariners.

# Nummer 38:

`1. Was sind P-Nachrichten? 2. Wie verfährt man mit diesen Nachrichten im Berichtigungsverfahren? (Begründung!)`

P-Nachrichten sind solche, die eine bevorstehende (preliminary) Maßnahme ankündigen.

Wegen der begrenzten Geltungsdauer werden keine Berichtigungen auf der Grundlage von P-Nachrichten vom BSH bzw. von amtlichen Seekartenberichtigungsstellen durchgeführt. Deshalb müssen vor Gebrauch jeder Seekarte die noch gültigen P-Nachrichten erfasst und in der Karte vermerkt werden.

# Nummer 39:

`1. Was sind T-Nachrichten? 2. Wie verfährt man mit diesen Nachrichten im Berichtigungsverfahren? (Begründung!)`

T-Nachrichten sind solche, die über einen zeitweiligen (temporary) Zustand unterrichten.

Wegen der begrenzten Geltungsdauer werden keine Berichtigungen auf der Grundlage von T-Nachrichten vom BSH bzw. amtlichen Seekartenberichtigungsstellen durchgeführt. Deshalb müssen vor Gebrauch jeder Seekarte die noch gültigen T-Nachrichten erfasst und in der Karte vermerkt werden.

# Nummer 40:

`Worauf muss beim Ansteuern einer Küste bei der Auswahl von Seekarten geachtet werden? Begründen Sie Ihre Antwort.`

Seekarten mit größtmöglichem Maßstab verwenden. Nur in diesen Karten sind alle Schifffahrtszeichen und weitere für die Navigation wichtigen Informationen eingetragen.

# Nummer 41:

`Was müssen Sie bei Kursberechnungen hinsichtlich der in der Seekarte angegebenen Ortsmissweisungen beachten?`

Die für ein bestimmtes Jahr angegebene Missweisung muss mittels der in der Seekarte angegebenen jährlichen Änderung für das aktuelle Jahr berichtigt werden.

# Nummer 42:

`Was müssen Sie bei der Benutzung von deutschen "Sportbootkarten" beachten?`

Sie werden nach dem Druck weder vom BSH noch von den Seekartenvertriebsstellen berichtigt. Sie müssen also vom Nutzer nach dem Kauf vor Benutzung über die NtS auf den aktuellen Stand berichtigt werden.

# Nummer 43:

`Nach welcher Faustregel können Sie m/s in Knoten umrechnen?`

"Doppelt so viele Knoten (kn) wie m/s" oder "m/s multipliziert mit 2 = kn".

# Nummer 44:

`Was müssen Sie beachten, wenn Sie die mit Loggen ermittelte Fahrt z. B. für das Arbeiten in Seekarten berücksichtigen wollen?`

Die üblichen Logmethoden liefern ausschließlich die "Fahrt durchs Wasser (FdW)". Um die "Fahrt über Grund (FüG)" zu ermitteln, müssen Stromrichtung und Stromgeschwindigkeit berücksichtigt werden.

# Nummer 45:

`Welche Fahrt zeigen GPS-Geräte an?`

Die Fahrt über Grund (FüG).

# Nummer 46:

`Welchen Kurs zeigen GPS-Geräte an?`

Den Kurs über Grund (KüG).

# Nummer 47:

`Warum müssen Sie Ihre Position regelmäßig in die Seekarte eintragen?`

Um Abweichungen von der Kurslinie frühzeitig und sicher zu erkennen und um ggf. den Kurs zu berichtigen.

# Nummer 48:

`Was ist die Besteckversetzung (BV)?`

Richtung (rw) und Entfernung (in sm) vom Koppelort (Ok) zum beobachteten Ort (Ob), bezogen auf den gleichen Zeitpunkt.

# Nummer 49:

`Welche Ursachen kann die Besteckversetzung (BV) haben?`

Die BV kann folgende Ursachen haben:

ungenaues Steuern und Koppeln,

Kursfehler (z. B. ungenaue Steuertafel) und

fehlende oder unvollständige Berücksichtigung von Strom und Wind.

# Nummer 50:

`Warum sollte der Winkel zwischen zwei Peilungen nicht kleiner als 30° und nicht größer als 150° sein?`

Damit der gefundene Standort eine ausreichend sichere Positionsbestimmung ergibt.

# Nummer 51:

`Warum sind regelmäßige Kompasskontrollen erforderlich?`

Zur Überprüfung der Funktionsfähigkeit des Kompasses und der Werte in der Ablenkungstabelle.

# Nummer 52:

`Wodurch können auch in gezeitenlosen Revieren erhebliche Wasserstandsschwankungen und Strömungen (z. B. Triftstrom) hervorgerufen werden?`

Durch Stärke, Dauer und Richtung des Windes oder "Zurückschwappen" aufgestauter Wassermassen (z. B. Ostsee).

# Nummer 53:

`Welche navigatorischen Vorbereitungen treffen Sie vor einer Fahrt in Dunkelheit?`

Kurse und Kursänderungspunkte möglichst vorausbestimmen,

Untiefen und Hindernisse in der Karte besonders kennzeichnen,

in der Seekarte markieren, welche Leuchtfeuer wann und wo in der Kimm erscheinen und

Wegstrecke nach unbefeuerten Tonnen absuchen.

# Nummer 54:

`Welche Möglichkeiten der terrestrischen Ortsbestimmung muss man kennen?`

Kreuzpeilung,

Peilung und Abstand (Feuer in der Kimm, Radarabstand),

Peilung und Lotung.

# Nummer 55:

`Nennen Sie zwei Möglichkeiten der Ortsbestimmung, wenn Sie nur ein Objekt mit bekannten Merkmalen (z. B. Leuchtturm) in Sicht haben.`

Peilung und Abstand (Feuer in der Kimm, Radarabstand),

Peilung und Lotung.

# Nummer 56:

`Welche Nordrichtungen werden in der Navigation unterschieden? Erläutern Sie diese kurz.`

rwN: rechtweisend Nord ist die Richtung eines Meridians zum geographischen Nordpol.

mwN: missweisend Nord ist die Richtung des erdmagnetischen Feldes zum magnetischen Nordpol, abhängig von Schiffsort und Datum (Jahr). In diese Richtung stellt sich eine ungestörte Magnet-(Kompass-)nadel ein.

MgN: ist die Richtung zu Magnetkompass-Nord. In diese Richtung zeigt die durch das schiffsmagnetische Feld beeinflusste Kompassnadel an Bord.

# Nummer 57:

`Nennen Sie die Winkel zwischen den Nordrichtungen rechtweisend Nord (rwN), missweisend Nord (mwN) und Magnetkompass-Nord (MgN).`

Mw: Missweisung ist der Winkel von rwN nach mwN.

Abl: Ablenkung (Abl) oder Deviation (Dev) ist der Winkel von mwN nach MgN.

# Nummer 58:

`Nennen Sie den Winkel zwischen den Nordrichtungen rwN und MgN.`

Der Winkel von rwN nach MgN ist die Fehlweisung (Fw; Abl + Mw = Fw).

# Nummer 59:

`Wo finden Sie die erforderlichen Werte der Missweisung? Worauf ist dabei zu achten?`

Die Missweisung findet sich in der Seekarte eingedruckt für ein bestimmtes Jahr.

Dieser Wert muss mit der ebenfalls in der Seekarte angegebenen jährlichen Änderung auf das Jahr der Benutzung berichtigt werden.

# Nummer 60:

`Wo finden Sie die erforderlichen Werte der Ablenkung (Abl)? Worauf ist dabei zu achten?`

Die Abl wird einer Ablenkungstabelle entnommen.

Die Abl ist abhängig vom anliegenden Kurs.

# Nummer 61:

`Warum muss für jedes Fahrzeug eine eigene Ablenkungstabelle (Steuertafel) erstellt werden?`

Die Ablenkungstabelle kann auf jedem Schiff andere Werte haben.

# Nummer 62:

`Worauf müssen Sie achten, wenn eine Magnetkompasspeilung (MgP) auf eine rechtweisende Peilung (rwP) beschickt werden soll?`

Abl für den anliegenden MgK (Magnetkompasskurs) aus der Steuertafel (Ablenkungstabelle) entnehmen; an den so erhaltenen mwK (missweisenden Kurs) die für das laufende Jahr der Seekarte entnommene Mw anbringen.

# Nummer 63:

`Unter welchen Voraussetzungen ergibt sich eine brauchbare Standlinie aus einer Lotung?`

Der Meeresgrund muss ausreichend regelmäßig und ausreichend steil ansteigen/abfallen.

# Nummer 64:

`Neben den Fahrwassertonnen liegen auf den Seeschifffahrtsstraßen weitere Tonnen aus, die für die Sportschifffahrt besonders wichtig sind. Welche Schifffahrtszeichen sind das?`

Sonderzeichen zur Bezeichnung von Sperrgebieten und Kardinalzeichen für allgemeine Gefahrenstellen.

# Nummer 65:

`Aus welchen nautischen Publikationen können Sie Sperr- und Verbotsgebiete mit ihren Grenzen ersehen?`

Aus den Seekarten, Bekanntmachungen für Seefahrer (BfS) und Nautischen Warnnachrichten (NWN).

# Nummer 66:

`Welche Sonderzeichen kennzeichnen Reeden, besondere Gebiete oder Stellen, z. B. Warngebiete?`

Gelbe Fasstonnen, Leuchttonnen, Spierentonnen oder Stangen.

# Nummer 67:

`Welche Sonderzeichen kennzeichnen Sperrgebiete?`

Gelbe Fasstonnen, Leuchttonnen, Spierentonnen oder Stangen mit einem breiten roten Band. Beschriftung auf Fasstonne oder Leuchttonne mit schwarzen Buchstaben: "Sperrgebiet" oder "Sperr-G".

# Nummer 68:

`Welche Farbe haben Feuer auf Sonderzeichen, wenn vorhanden?`

Farbe gelb.

# Nummer 69:

`Was bedeutet das Ausliegen der folgenden Schifffahrtszeichen: weiße Fasstonne, Kugeltonne oder Stange mit einem - von oben gesehen - rechtwinkligen gelben Kreuz bzw. bei Stangen mit einem breiten gelben Band?`

Fahrverbot für Maschinenfahrzeuge und Wassermotorräder auf wegen Badebetrieb gesperrten Wasserflächen.

# Nummer 70:

`Wie stehen Sonne und Mond winkelmäßig zur Erde bei Springzeit und bei Nippzeit (die Springverspätung soll hier unberücksichtigt bleiben)?`

Bei Springzeit befinden sich Mond und Sonne in einer Ebene mit der Erde, bei Nippzeit stehen die Verbindungslinien Erde/Sonne und Erde/Mond im rechten Winkel zueinander.

# Nummer 71:

`Erklären Sie den Begriff "Alter der Gezeit".`

Das Alter der Gezeit gibt an, in welcher Phase (Nippzeit, Mittzeit, Springzeit) sich das aktuelle Tidengeschehen befindet.

# Nummer 72:

`Warum findet man z. B. bei Bezugsorten in der Nordsee bzw. im Englischen Kanal zeitweise nur ein Hoch- bzw. Niedrigwasser pro Tag?`

Die Umlaufzeit des Mondes um die Erde dauert im Mittel 24 h 50 min. (Mondtag) gegenüber dem Sonnentag (= 24 h). Deshalb "rutscht" das letzte HW oder NW zeitweise in den nächsten Tag.

# Nummer 73:

`Weshalb und wie können die tatsächlichen Wasserstände von den Angaben in den Gezeitentafeln teilweise erheblich abweichen?`

Durch Wind und/oder durch sehr hohen bzw. sehr niedrigen Luftdruck können erhebliche Wasserstandsänderungen entstehen. HWH bzw. NWH können höher oder niedriger sein als angegeben, die Hoch- und Niedrigwasserzeit kann füher oder später eintreten als angegeben.

# Nummer 74:

`Worauf beziehen sich die Tiefenangaben in Seekarten in den deutschen Gewässern der Ost- und Nordsee?`

Auf Kartennull (KN).

# Nummer 75:

`Was ist Kartennull?`

Kartennull (KN) ist die Bezugsfläche für die Tiefenangaben in einer Seekarte.

# Nummer 76:

`Wie ist Kartennull (KN) in der Ost- und Nordsee und im Englischen Kanal definiert? Wo finden Sie die entsprechenden Angaben zur Kartennullebene?`

In der Ostsee entspricht KN dem mittleren Wasserstand.
In der Nordsee und im Englischen Kanal entspricht KN dem niedrigstmöglichen Gezeitenwasserstand (LAT = Lowest Astronomical Tide).
In der jeweiligen Seekarte ist die Kartennullebene beschrieben.

# Nummer 77:

`Was müssen Sie bedenken, wenn Sie die Wassertiefe außerhalb der Niedrigwasserzeit loten?`

Beim folgenden Niedrigwasser wird die Wassertiefe geringer sein als zum Zeitpunkt der Lotung.

# Nummer 78:

`Was ist die Kartentiefe?`

Die Kartentiefe (KT) ist die auf Kartennull bezogene Wassertiefe. Kartentiefe ist Wassertiefe abzüglich Höhe der Gezeit.

# Nummer 79:

`Mit welcher Wassertiefe können Sie bei einer Lotung normalerweise mindestens rechnen?`

Mit der Kartentiefe.

# Nummer 80:

`Welche Bedeutung hat die Angabe "Springzeit" für die Wasserstände in Gezeitengebieten?`

Zur Springzeit sind besonders hohe Hochwasser und besonders niedrige Niedrigwasser zu erwarten.

# Nummer 81:

`Welche Bedeutung hat die Angabe "Nippzeit" für die Wasserstände in Gezeitengebieten?`

Zur Nippzeit sind besonders niedrige Hochwasser und besonders hohe Niedrigwasser zu erwarten.

# Nummer 82:

`Welche Bedeutung haben die Angaben "Nippzeit" bzw. "Springzeit" für die Gezeitenströme?`

Zur Springzeit setzen die Gezeitenströme z. T. deutlich stärker als zur Nippzeit.

# Nummer 83:

`Wo können Sie Informationen über Gezeitenströme in Küstengewässern finden?`

In Gezeitenstromatlanten, Seehandbüchern,

in Seekarten aus Gezeitenstromtabellen, die bezogen sind auf die Hochwasserzeiten des dort genannten Bezugsortes.

# Nummer 84:

`Auf einer Seekarte finden Sie in Küstennähe die Tiefenangabe 23. Was bedeutet das?`

Der Ort der Zahl liegt 2,3 m über Kartennull und kann trockenfallen.

# Nummer 85:

`In welchem Zusammenhang stehen Kartentiefe (KT), Wassertiefe (WT) und Höhe der Gezeit (H)?`

WT - H = KT oder KT + H = WT (Lösung auch als Skizze möglich).

# Nummer 86:

`Warum ist es in Tidengewässern wichtig, die Uhrzeit einer Lotung festzuhalten?`

Um anhand der Gezeitentafel feststellen zu können, ob das Wasser steigt oder fällt.

# Nummer 87:

`Was ist ein Pegel?`

Eine Skala zur Anzeige des Wasserstandes.

# Nummer 88:

`Welchen Einfluss kann der Wind auf die Gezeiten haben?`

Der Wind kann Strömungen und Wasserstandsänderungen hervorrufen, die zu den Gezeitenströmen und den Gezeiten hinzutreten.

# Nummer 89:

`Nennen Sie drei wichtige Vorzüge von GPS.`

GPS arbeitet weltweit.

Die Positionsanzeige ist jederzeit verfügbar.

Der Positionsfehler ist gering.

# Nummer 90:

`Wie groß ist die typische und realistische Genauigkeit von Positionen, die mit GPS und DGPS ermittelt werden?`

GPS: 10 - 20 m bei einer Wahrscheinlichkeit von etwa 95 %.

DGPS: 1 - 10 m bei einer Wahrscheinlichkeit von etwa 95 %.

# Nummer 91:

`Wo muss man mit ungenauen Anzeigen des GPS rechnen?`

Bei Abschattung der GPS-Antenne.

In der Nähe von Flughäfen und in der Nähe von Fernsehsendern.

In der Nähe von Marineeinrichtungen.

Bei Nutzung von UKW-Geräten und anderen elektronischen/elektrischen Geräten an Bord.

# Nummer 92:

`Was bedeutet die Abkürzung GPS?`

Global Positioning System.

# Nummer 93:

`Was ist das Grundprinzip von GPS?`

Durch Laufzeitmessungen von GPS-Signalen vom Satelliten zum Empfänger und damit durch Abstandsmessungen zu den Satelliten wird die Ortsbestimmung ermöglicht.

# Nummer 94:

`Was bedeutet die Abkürzung DGPS und nach welchem Prinzip arbeitet DGPS?`

DGPS = Differential Global Positioning System.
Hierbei handelt es sich um eine regionale Verbesserung der Ortsbestimmung. Dabei werden von Referenzstationen über Funk Korrekturwerte für die GPS-Messwerte an die Schiffe übertragen.

# Nummer 95:

`Was ist bei Anbringung einer GPS-Antenne zu beachten?`

Sie muss ringsum freie Sicht (ohne Abschattungen) haben.

Einwandfreie Erdung.

# Nummer 96:

`Was bewirkt die Bedienung der MOB-Taste bei GPS-Geräten?`

Die Position zur Zeit des Tastendrucks wird gespeichert.

Rechtweisende Peilung (rwP) und Distanz zu diesem Punkt werden angezeigt.

# Nummer 97:

`Was bedeutet die Aussage: "Die Ortsgenauigkeit beträgt 100 m mit einer Wahrscheinlichkeit von 95 %"?`

Das Schiff befindet sich mit einer Wahrscheinlichkeit von 95 % in einem Fehlerkreis von 100 m Radius um den beobachteten Ort. Also: jede 20. Ortsbestimmung (5 %) ist ungenauer als ca. 100 m.

# Nummer 98:

`Wie kann man feststellen, ob die GPS-Position genau bzw. zuverlässig ist?`

Durch den vom Empfänger angezeigten HDOP (horizontal dilution of precision = Satellitenverteilung).

Durch die vom Empfänger angezeigte Anzahl der getrackten Satelliten.

Durch Vergleich mit anderen Navigationssystemen und der Koppelposition.

# Nummer 99:

`Was ist bei Eintragung eines GPS-Ortes in eine Seekarte zu beachten?`

Das Bezugssystem muss übereinstimmen. Dieses kann geschehen durch:

Auswahl und Einstellung des Kartenbezugssystems im Empfänger.

Manuelle Verschiebung des GPS-Ortes um die in der Seekarte angegebenen N/S- und E/W-Korrektur.

Verwendung von Seekarten, die auf dem System WGS 84 beruhen.

# Nummer 100:

`Was ist ein Wegpunkt?`

Geographische Koordination eines anzusteuernden Punktes.

# Nummer 101:

`Was bedeutet WGS 84 und was wird damit erreicht?`

Globales Bezugssystem "World Geodetic System 1984".

Mit diesem System (= Referenzellipsoid bei GPS) wird weltweit eine optimale Anpassung an die reale Form des gesamten Erdkörpers erreicht.

# Nummer 102:

`Wodurch können Radarechos von kleinen Fahrzeugen und Tonnen auf den Sichtschirmen von Radargeräten "verschwinden"?`

Durch Seegang und/oder Niederschlag,

durch falsche Bedienung,

durch zu große Entfernung,

durch Gieren des eigenen Fahrzeugs bei relativ vorausorientierter Radarstellung (head up).

# Nummer 103:

`Wie kann man mit Radar den eigenen Schiffsort bestimmen?`

Peilung des Objektes gibt einen Peilstrahl als Standlinie.

Abstandsmessung mit dem VRM (Variable Range Marker) gibt einen Abstandskreis als Standlinie.

# Nummer 104:

`Wie kann man gegebenenfalls verhindern, dass sich Echoanzeigen von Zielen (z. B. 2 Tonnen, 2 Molenköpfe) überlappen?`

Kurze Impulslänge wählen.

Messbereich verkleinern.

# Nummer 105:

`Was bedeutet der Begriff AIS auf See?`

AIS bezeichnet das automatische Identifizierungssystem (Automatic Identification System).

# Nummer 106:

`Welche Aufgaben hat AIS?`

Alle ausgerüsteten Schiffe senden automatisch (also ohne Aufforderung und menschliches Eingreifen) in regelmäßigen kurzen Abständen ihre Identität und einen schiffsbezogenen Datensatz. Außerdem können bei Bedarf sicherheitsrelevante Nachrichten ("safety related messages") von Bord oder von Landstationen gesendet werden.

# Nummer 107:

`Welche Reichweite hat ein AIS-Bordgerät und wovon ist sie abhängig?`

Die Reichweite und Ausbreitungsbedingungen entsprechen denen von UKW. Bei Handelsschiffen kann man von 20 bis 30 sm ausgehen. Die Reichweite ist abhängig von der Antennenhöhe.

# Nummer 108:

`Wie kann die Reichweite eines AIS-Bordgerätes landseitig erhöht werden und wie wirken sich dabei Hindernisse (z. B. Berge) aus?`

Unter bestimmten Umständen kann die Reichweite heraufgesetzt werden (z. B. mit Hilfe von "Relaisstationen"), wobei ggf. auch abschattende Hindernisse umgangen werden können.

# Nummer 109:

`Welche AIS-Daten werden von Schiffen aus der Berufsschifffahrt gesendet?`

Statische Daten: ID, Rufzeichen, Länge und Breite des Schiffes u. a.

Dynamische Daten (i. W. Sensordaten): UTC, Position, Heading, Kurs und Fahrt über Grund, ggf. Rate-of-turn, Fahrtstatus (z. B. Maschinenfahrzeug mit Fahrt durchs Wasser, Ankerlieger, manövrierbehindertes Fahrzeug).

Reisebezogene Daten: Tiefgang, Zielort (Destinaton), ETA u. a.

# Nummer 110:

`Wann kann man sich auf die Verfügbarkeit und Anzeige vn AIS-Signalen anderer Fahrzeuge verlassen? Nennen Sie die wesentlichen Voraussetzungen!`

Andere Fahrzeuge werden nur angezeigt, wenn das Fahrzeug auch sendet, d. h., wenn

das Fahrzeug mit AIS ausgerüstet ist,

das sendende Fahrzeug AIS nicht abgeschaltet hat (darf der Kapitän allerdings nur bei bestimmten zwingenden Gründen),

GPS aktiv ist und

aus Kapazitätsgründen (z. B. durch zu viele Schiffe in einem Seegebiet = "target overflow") keine Fahrzeuge ausgeschlossen werden.

# Nummer 111:

`Wie ist die Genauigkeit von AIS-Daten zu beurteilen (Position und manuell eingegebene Daten)?`

Position:
Mit AIS wird zusätzlich zur GPS-Position eines Schiffes die Information übertragen, ob es sich um einen GPS- oder DGPS-Ort handelt. Ist die GPS-Position eines Schiffes falsch, wird diese falsche Position auf allen anderen Schiffen angezeigt.

Manuell eingegebene Daten:
Es muss damit gerechnet werden, dass Zielort, Tiefgang, Fahrtstatus u. a. falsch sind, wenn sie - z. B. aus Nachlässigkeit - nicht von der Schiffsführung aufdatiert werden.

# Nummer 112:

`Welche besondere Bedeutung hat AIS für die Sportschifffahrt im Vergleich mit der Radaranzeige auf anderen Schiffen?`

Sportfahrzeuge werden häufig auf den Radargeräten anderer Schiffe nicht sicher angezeigt bzw. die Anzeigen gehen im Seegangsclutter oder in der Informationsfülle unter. Da jetzt auf vielen Schiffen die AIS-Daten zusätzlich im Radar dargestellt werden, besteht die Gefahr, dass Sportfahrzeuge noch weniger auffällig sind, wenn sie nicht selbst mit AIS ausgerüstet sind.

# Nummer 113:

`Welche Navigationsgeräte sollten Sie auf einer Yacht auch bei Kurzfahrten nahe der Küste mindestens an Bord haben?`

Steuerkompass, Peilkompass, Lot, Log, Uhr.

# Nummer 114:

`Was gehört zur navigatorischen Mindestausrüstung einer Yacht in Küstengewässern?`
Nennen Sie mindestens 6 Beispiele.

Steuerkompass,

Peileinrichtung,

terrestrisches oder satellitengestütztes Funknavigationsgerät,

Log,

Lot,

Fernglas,

Barometer,

Weltempfänger für Rundfunk,

Seebücher und auf den neuesten Stand berichtigte Seekarten für das zu befahrende Seegebiet,

Logbuch,

Uhr/Zeitmesser.

# Nummer 115:

`Welchen Vorteil hat ein Kugelkompass gegenüber einem Fernglaskompass?`

Der Kugelkompass kann auch bei größerer Krängung noch als Messinstrument benutzt werden.

Die Kugelform verbessert die Ablesbarkeit der Kompassrose (Vergrößerungseffekt).

# Nummer 116:

`Was beeinflusst die Ablenkung eines Kompasses dauerhaft?`

Veränderung des magnetischen Zustandes an Bord, z. B. Einbauten und Lageänderung von Ausrüstungsgegenständen.

# Nummer 117:

`Was beeinflusst die Ablenkung eines Kompasses vorübergehend?`

Elektronische Geräte (z. B. Radio, Handy), magnetisierte Gegenstände (z. B. Werkzeug, Peilkompass) und Gleichstromleitungen in der Nähe des Kompasses.

# Nummer 118:

`Welchen Abstand muss magnetisierbares Material vom Magnetkompass haben?`

Mindestens 1 Meter.
