# Nummer 1:

`Was sind "Sicherheitszonen" im Sinne der Verordnung zu den KVR?`

Sicherheitszonen sind Wasserflächen im Umkreis von 500 m um Plattformen, Bohrinseln, Forschungsanlagen u. a., die nicht befahren werden dürfen.

# Nummer 2:

`Die Verordnung zu den KVR verbietet die Führung eines Fahrzeugs, wenn man infolge des Genusses alkoholischer Getränke in der sicheren Führung des Fahrzeugs behindert ist. Welchen örtlichen Geltungsbereich hat die vorgenannte Verordnung?`

Die Verordnung gilt auf Seeschifffahrtsstraßen und für Schiffe, die die Bundesflagge führen, seewärts der Begrenzung des Küstenmeeres der Bundesrepublik Deutschland (also weltweit), soweit nicht in den Hoheitsgewässern anderer Staaten abweichende Regelungen gelten.

# Nummer 3:

`Wer darf laut Verordnung zu den KVR ein Fahrzeug nicht führen oder als Mitglied der Crew eine andere Tätigkeit des Brücken- oder Decksdienstes nicht ausüben (allgemein ohne Zahlen zu beantworten)?`

Wer infolge körperlicher oder geistiger Mängel oder des Genusses alkoholischer Getränke oder anderer berauschender Mittel in der sicheren Führung eines Fahrzeugs oder in der sicheren Ausübung einer anderen Tätigkeit des Brücken- oder Decksdienstes behindert ist.

# Nummer 4:

`Welche Atem- bzw. Blutalkoholkonzentration darf laut Verordnung zu den KVR nicht erreicht werden, damit kein Verbot für ein Führen eines Fahrzeugs oder als Mitglied der Crew für ein Ausüben des Brückendienstes besteht?`

0,25 mg/l oder mehr Alkohol in der Atemluft oder 0,5 Promille oder mehr Alkohol im Blut oder eine Alkoholmenge, die zu einer solchen Atem- oder Blutalkoholkonzentration führt.

# Nummer 5:

`Die KVR regeln u. a. das Verhalten der Schiffsführungen bei Kollisionsgefahr. Was ist im Rahmen der Verantwortlichkeit bei der Auslegung und Befolgung der KVR zu berücksichtigen?`

Bei der Auslegung und Befolgung der KVR sind stets alle Gefahren der Schifffahrt und des Zusammenstoßes sowie alle besonderen Umstände einschließlich Behinderungen der betroffenen Fahrzeuge gebührend zu berücksichtigen, die zum Abwenden unmittelbarer Gefahr ggf. auch ein Abweichen von diesen Regeln erfordern können (z. B. Abweichen von der Kurshaltepflicht, wenn der Ausweichpflichtige nicht angemessen handelt).

# Nummer 6:

`Welche Grundregeln für das Verhalten im Verkehr verlangen die KVR, die ein Schiffsführer zu berücksichtigen hat, auch wenn keine konkrete Regel anwendbar ist?`

Die KVR befreien nicht von den Folgen, die durch unzureichende Einhaltung der KVR oder unzureichende Vorsichtsmaßnahmen entstehen, d. h., allgemeine seemännische Praxis oder besondere Umstände des Falles können über die Mindestanforderungen der KVR hinausgehende Maßnahmen erfordern.

# Nummer 7:

`Was sind Verkehrstrennungsgebiete? Wie sind sie zu befahren?`

Verkehrstrennungsgebiete sind Schifffahrtswege, die durch Trennlinien oder Trennzonen in Einbahnwege geteilt sind.

Diese dürfen nur in Fahrtrichtung rechts der Trennlinie/Trennzone befahren werden, aber unter Nutzung der vollen Breite des Einbahnweges.

# Nummer 8:

`Was ist ein "manövrierunfähiges Fahrzeug"?`

Manövrierunfähig ist ein Fahrzeug, das wegen außergewöhnlicher Umstände (z. B. Ruderbruch) nicht regelgerecht manövrieren und daher einem anderen Fahrzeug nicht ausweichen kann.

# Nummer 9:

`Was ist ein "manövrierbehindertes Fahrzeug"?`

Manövrierbehindert ist ein Fahrzeug, das durch die Art seines Einsatzes behindert ist (z. B. Bagger, Kabelleger), regelgerecht zu manövrieren, und daher einem anderen Fahrzeug nicht ausweichen kann.

# Nummer 10:

`Nennen Sie mindestens 3 Beispiele für "manövrierbehinderte Fahrzeuge".`

Tonnenleger, Kabelleger, Rohrleger im Einsatz,

Bagger, Vermessungsfahrzeuge im Einsatz,

Versorger im Einsatz,

Flugzeugträger im Einsatz,

Minenräumfahrzeuge im Einsatz,

Fahrzeuge während eines Schleppvorganges, bei dem das schleppende Fahrzeug und sein Anhang erheblich behindert sind, vom Kurs abzuweichen.

# Nummer 11:

`Was ist unter "sicherer Geschwindigkeit" zu verstehen?`

Das Fahrzeug muss jederzeit innerhalb einer solchen Entfernung zum Stehen gebracht werden können, dass ein Zusammenstoß vermieden wird.

# Nummer 12:

`Ab welcher Länge müssen Sportfahrzeuge mit den Lichtern/Signalkörpern ausgerüstet sein, die bei Manövrierunfähigkeit zu setzen sind?`

Fahrzeuge ab 12 m Länge.

# Nummer 13:

`Sie sehen in der Dämmerung in der Nordsee in der Zufahrt zur Jade einen großen Tanker mit der üblichen Lichterführung, auf dem kurze Zeit später die Lichter rot-weiß-rot senkrecht übereinander zusätzlich zu den Fahrtlichtern gesetzt werden.`
Welche rechtliche Bedeutung hat die geänderte Signalgebung für Sie?

Beim Erreichen des Geltungsbereiches der SeeSchStrO kennzeichnet sich der Tanker als Wegerechtschiff, das als manövrierbehindertes Fahrzeug gilt. Diesem so gekennzeichneten Fahrzeug muss im Falle einer Kollisionsgefahr ausgewichen werden.

# Nummer 14:

`Welche Lichter müssen manövrierbehinderte Fahrzeuge (außer Minenräumfahrzeuge) führen`
1. ohne Fahrt durchs Wasser (FdW),
2. mit FdW,
3. vor Anker?

Ohne FdW:
rot-weiß-rot senkrecht übereinander.

Mit FdW:
rot-weiß-rot senkrecht übereinander und Lichter eines Maschinenfahrzeugs (Topplicht[er]), Seitenlichter, Hecklicht.

Vor Anker:
rot-weiß-rot senkrecht übereinander und Ankerlicht(er).

# Nummer 15:

`Wie sind manövrierbehinderte und manövrierunfähige Fahrzeuge am Tage bezeichnet?`

Manövrierbehinderte Fahrzeuge:
Ball-Rhombus-Ball senkrecht übereinander.

Manövrierunfähige Fahrzeuge:
zwei schwarze Bälle senkrecht übereinander.

# Nummer 16:

`Wie müssen Sie Ihr Fahrzeug unter Segel bei Tage und bei Nacht kennzeichnen, wenn Sie gleichzeitig mit Maschinenkraft fahren?`

Bei Nacht Lichterführung eines Maschinenfahrzeugs entsprechender Größe,

bei Tage einen Kegel - Spitze unten - im Vorschiff gut sichtbar.

# Nummer 17:

`Was müssen Sie hinsichtlich der Zeiten der Lichterführung beachten?`

Die Lichter müssen geführt werden

zwischen Sonnenuntergang und Sonnenaufgang,

bei verminderter Sicht auch zwischen Sonnenaufgang und Sonnenuntergang.

# Nummer 18:

`Sie sehen am Tage ein Fahrzeug, augenscheinlich kürzer als 50 m, mit dem Sichtzeichen "schwarzer Rhombus", dahinter in gleichbleibendem Abstand ein weiteres Fahrzeug mit dem gleichen Signalkörper.`
1. Worum handet es sich?
2. Wie sind die Fahrzeuge bei Nacht gekennzeichnet?

Es handelt sich um einen Schleppverband länger als 200 m (Heck des Schleppers - Heck des Anhangs).

Der Schlepper führt nachts drei weiße Topplichter senkrecht übereinander, Seitenlichter, Hecklicht und das gelbe Schlepplicht über dem Hecklicht. Der Anhang führt Seitenlichter und Hecklicht.

# Nummer 19 (Abbildung fehlt):

`Man hört bei Nebel folgendes Schallsignal mit der Pfeife`

Schallsignal lang-kurz-kurz

(lang-kurz-kurz), unmittelbar gefolgt von
Schalsignal lang-kurz-kurz-kurz

(lang-kurz-kurz-kurz) etwa jede Minute.
Worum handelt es sich dabei?

Es ist das Schallsignal eines Schleppverbandes in Fahrt (schleppendes Fahrzeug lang-kurz-kurz; Anhang lang-kurz-kurz-kurz).

# Nummer 20:

`Bei Nebel im Küstenbereich fahrend, hört man etwa jede Minute folgendes Signal: drei Glockenschläge, dann ca. 5 Sekunden lang rasches Läuten einer Glocke, dann drei Glockenschläge. Wer gibt dieses Signal?`

Dieses Signal gibt ein Fahrzeug auf Grund unter 100 m Länge.

# Nummer 21 (Abbildung fehlt):

`Sie sehen ein Fahrzeug mit folgender Lichterführung: Lichterführung Treibnetzfischer in Fahrt oder vor Anker mit ausgebrachtem Fanggerät, das waagerecht weiter als 150 m ins Wasser reicht`

1. Worum handelt es sich?
2. Welches Schallsignal müsste dieses Fahrzeug bei unsichtigem Wetter geben?
Treibnetzfischer (Fahrzeug, das nicht trawlt) in Fahrt oder vor Anker mit ausgebrachtem Fanggerät, das waagerecht weiter als 150 m ins Wasser reicht. (Das untere weiße Licht kann auch das Hecklicht sein).

Schallsignal
Schallsignal lang-kurz-kurz

(lang-kurz-kurz) mindestens alle 2 Minuten.

# Nummer 22 (Abbildung fehlt):

`Sie sehen nachts auf See 2 rote Lichter senkrecht übereinander:`
Lichterführung manövrierunfähiges Fahrzeug in Fahrt ohne Fahrt durchs Wasser
Worum handelt es sich?

Um ein manövrierunfähiges Fahrzeug in Fahrt ohne Fahrt durchs Wasser.

# Nummer 23 (Abbildung fehlt):

`Die Lichteranordnung eines Fahrzeugs ändert sich plötzlich von`

Lichterführung manövrierunfähiges Fahrzeug in Fahrt ohne Fahrt durchs Wasser

in

Lichterführung manövrierunfähiges Fahrzeug in Fahrt ohne Fahrt durchs Wasser hat Fahrt durchs Wasser aufgenommen, jetzt sieht man auch das Backbord-Seitenlicht

Was schließen Sie daraus?

Ein manövrierunfähiges Fahrzeug in Fahrt ohne Fahrt durchs Wasser (FdW) hat FdW aufgenommen, da man jetzt auch das Backbord-Seitenlicht sieht.

# Nummer 24:

`Was bestimmen die KVR über das Ausguckhalten?`

Es muss jederzeit durch Sehen und Hören sowie durch jedes andere verfügbare Mittel gehöriger Ausguck gehalten werden, der einen vollständigen Überblick über die Lage und die Möglichkeit der Gefahr eines Zusammenstoßes gibt.

# Nummer 25:

`Was bestimmen die KVR für das Verhalten von Fahrzeugen von weniger als 20 m Länge oder von Segelfahrzeugen im Fahrwasser einer Seeschifffahrtsstraße?`

Fahrzeuge von weniger als 20 m Länge oder Segelfahrzeuge dürfen nicht die Durchfahrt eines Fahrzeuges behindern, das nur innerhalb eines engen Fahrwassers oder einer Fahrrinne sicher fahren kann.
Sie müssen, wenn es die Umstände erfordern, frühzeitig Maßnahmen ergreifen, um genügend Raum für die sichere Durchfahrt des anderen Fahrzeugs zu lassen.

# Nummer 26:

`Was ist eine "Küstenverkehrszone"?`

Das Gebiet zwischen der Küste und der landwärtigen Grenze eines Verkehrstrennungsgebietes.

# Nummer 27:

`Welche Fahrzeuge dürfen die Küstenverkehrszone benutzen, ohne einen Hafen innerhalb der Küstenverkehrszone anzusteuern?`

Fahrzeuge von weniger als 20 m Länge und Segelfahrzeuge.

# Nummer 28:

`Wie müssen Maschinenfahrzeuge ohne Radar bei verminderter Sicht ihre Fahrweise einrichten?`

Maschinenfahrzeuge müssen mit sicherer Geschwindigkeit fahren, die den gegebenen Umständen und Bedingungen der verminderten Sicht angepasst ist.

# Nummer 29:

`Wie müssen sich Segelfahrzeuge ohne Radar bei verminderter Sicht verhalten? Was gehört dabei zu den Regeln guter Seemannschaft?`

Segelfahrzeuge müssen mit sicherer Geschwindigkeit fahren, die den gegebenen Umständen und Bedingungen der verminderten Sicht angepasst ist.

Bei Segelfahrzeugen, die eine Maschine an Bord haben, gehört das Bereithalten der Maschine zu den Regeln guter Seemannschaft.

# Nummer 30:

`Wie müssen sich Fahrzeuge ohne Radar bei verminderter Sicht verhalten, wenn sie voraus das Schallsignal eines anderen Fahrzeuges hören?`

Jedes Fahrzeug, das anscheinend vorlicher als querab das Schallsignal eines anderen Fahrzeuges hört, muss seine Fahrt auf das für die Erhaltung der Steuerfähigkeit geringstmögliche Maß verringern. Erforderlichenfalls muss es jegliche Fahrt wegnehmen und in jedem Fall mit äußerster Vorsicht manövrieren, bis die Gefahr eines Zusammenstoßes vorüber ist.

# Nummer 31:

`Sie segeln in der Nordsee bei guter Sicht. Ihnen kommt in stehender Peilung ein Maschinenfahrzeug entgegen, das keine Anstalten macht, seiner Ausweichpflicht nachzukommen. Geben Sie in einer sinnvollen Reihenfolge an, was von Ihnen zu unternehmen ist. Welche dieser Maßnahmen sind zwingend vorgeschrieben?`

Über Funk versuchen, das andere Fahrzeug auf seine Ausweichpflicht aufmerksam zu machen.

Schallsignal: mindestens fünf kurze, rasch aufeinander folgende Pfeifentöne geben.

Ggf. Ergänzung zu 2.: Lichtsignal von mindestens fünf kurzen, rasch aufeinander folgenden Blitzen.

Manöver des sogenannten "vorletzten Augenblicks" fahren.

Manöver des sogenannten "letzten Augenblicks" fahren.
Zwingend vorgeschrieben sind die Maßnahmen nach 2. und 5.

# Nummer 32:

`Auf einem Segelfahrzeug unter Motor sieht man nachts fast recht voraus ein näher kommendes Fahrzeug mit folgender Lichterführung: oben ein weißes Licht, seitlich darunter ein grünes Licht, zeitweise rechts von dem grünen Licht auf gleicher Höhe auch ein rotes Licht. Um was für ein Fahrzeug handelt es sich, was ist von Ihnen und was ist auf dem anderen Fahrzeug zu unternehmen?`

Es handelt sich um ein Maschinenfahrzeug von weniger als 50 m Länge, das im Seegang oder durch schlechtes Steuern giert. Man muss annehmen, dass sich zwei Maschinenfahrzeuge auf entgegengesetzten oder fast entgegengesetzten Kursen nähern und die Möglichkeit der Gefahr eines Zusammenstoßes besteht. Beide Fahrzeuge müssen den Kurs nach Steuerbord ändern und dieses durch einen kurzen Ton anzeigen.

# Nummer 33:

`Wie muss man sich verhalten, wenn man gezwungen ist, ein Verkehrstrennungsgebiet zu queren?`

Kielrichtung (rwK) muss möglichst rechtwinklig zur allgemeinen Verkehrsrichtung zeigen.

# Nummer 34:

`Wie muss man sich verhalten, wenn man einen betonnten Schifffahrtsweg (z. B. in der Ostsee) queren will?`

Die Ausweichregeln der KVR beachten.

# Nummer 35:

`Wie ist die Gefahr eines Zusammenstoßes sicher erkennbar?`

Wenn die Kompasspeilung zu einem anderen Fahrzeug steht und sie sich einander nähern.

# Nummer 36:

`Wie müssen Sie Ausweichmanöver durchführen?`

Möglichst frühzeitig,

durchgreifend, sodass das andere Fahrzeug rasch Ihre Absicht erkennen kann, und um sich gut klar zu halten.

# Nummer 37:

`Wie müssen Sie sich verhalten, nachdem Sie ein vorgeschriebenes Ausweichmanöver eingeleitet haben?`

Der Erfolg des Manövers ist laufend zu überprüfen, bis das andere Fahrzeug klar passiert ist.

# Nummer 38:

`Sie segeln mit Wind von Steuerbord und sehen nachts in Luv ein einzelnes rotes Licht, das in stehender Peilung näher kommt.`
1. Was ist das für ein Licht?
2. Wer muss ausweichen? (Begründung!)

Das Licht ist das Backbordlicht eines Segelfahrzeugs in Fahrt.

Das Segelfahrzeug in Luv muss ausweichen, entweder weil es den Wind von Backbord hat oder weil es - wenn mit Wind von Steuerbord segelnd - luvwärts steht.

# Nummer 39:

`Sie segeln mit Wind von Backbord und sehen nachts in Luv ein einzelnes grünes Licht, das in stehender Peilung näher kommt.`
1. Was ist das für ein Licht?
2. Wer muss ausweichen? (Begründung!)

Das Licht ist das Steuerbordlicht eines Segelfahrzeugs in Fahrt.

Ihr Fahrzeug muss als leewärtiges Fahrzeug ausweichen, weil Sie (mit Wind von Backbord segelnd) nicht erkennen können, von welcher Seite das andere Fahrzeug den Wind hat.

# Nummer 40:

`Sie segeln nachts mit raumem Wind und machen gute Fahrt. Sie sehen an Steuerbord voraus ein einzelnes weißes Licht in (nahezu) stehender Peilung. Näher kommend verschwindet das weiße Licht gelegentlich und es erscheint stattdessen in etwa gleicher Höhe und links davon ein rotes Licht. Jeweils kurzfristig sind beide Lichter gleichzeitig zu sehen.`
1. Worum handelt es sich bei diesen Lichtern?
2. Wer muss ausweichen? (Begründung!)

Man sieht Hecklicht und/oder Backbord-Seitenlicht eines Segelfahrzeugs in Fahrt, das im Seegang giert.

Ihr Fahrzeug nähert sich aus dem Hecksektor des anderen Fahrzeugs. Es steht eben auf dessen Sektorengrenze und muss als überholendes Fahrzeug ausweichen. Im Zweifel (hier Sektorengrenze!) muss man sich als Überholer betrachten.

# Nummer 41:

`Sie segeln nachts mit raumem Wind und sehen nahezu achteraus ein Fahrzeug mit der Lichterführung rot-weiß-rot senkrecht übereinander, das näher kommt. Zusätzlich sehen Sie neben zwei weißen Topplichtern links ein grünes und rechts ein rotes Licht auf gleicher Höhe.`
1. Was bedeuten diese Lichter?
2. Wer muss ausweichen? (Begründung!)

Man sieht ein manövrierbehindertes Fahrzeug mit FdW (Topplichter, Seitenlichter).

Dieses Fahrzeug nähert sich im Hecklichtsektor und muss deshalb als Überholer ausweichen.

# Nummer 42:

`Welcher Zeitpunkt ist im freien Seeraum entscheidend für die Verantwortlichkeit (hier = Ausweichpflicht!) der Fahrzeuge untereinander?`

Der Augenblick des ersten Insichtkommens. Eine spätere Änderung der Lage der Fahrzeuge zueinander verändert nicht die Verantwortlichkeit.

# Nummer 43:

`Ein anderes Fahrzeug muss Ihnen ausweichen. Welche Verpflichtung nach KVR haben Sie? Was unternehmen Sie, wenn das andere Fahrzeug nicht ausweicht?`

Mein Fahrzeug ist "Kurshalter", d. h. es muss Kurs und Geschwindigkeit beibehalten.

Mein Fahrzeug darf zur Abwendung eines Zusammenstoßes manövrieren, sobald erkennbar wird, dass das andere Fahrzeug nicht angemessen (= regelgerecht) manövriert ("Manöver des vorletzten Augenblicks!")

Mein Fahrzeug muss zweckdienlich manövrieren, wenn ein Manöver des Ausweichpflichtigen allein einen Zusammenstoß nicht mehr vermeiden kann ("Manöver des letzten Augenblicks!")

# Nummer 44:

`Welchen Fahrzeugen muss ein Segelfahrzeug ausweichen?`

Einem manövrierunfähigen Fahrzeug,

einem manövrierbehinderten Fahrzeug,

einem fischenden Fahrzeug,

ggf. einem anderen Segelfahrzeug, abhängig von der Segelstellung in Bezug auf den Wind.

# Nummer 45:

`1. Wie muss sich ein Sportfahrzeug gegenüber einem tiefgangbehinderten Fahrzeug verhalten? 2. Schlagen Sie entsprechende Maßnahmen/Manöver vor.`

Das Sportfahrzeug muss vermeiden, die sichere Durchfahrt eines tiefgangbehinderten Fahrzeugs zu behindern.

Dieses kann durch eine frühzeitige Kursänderung, Geschwindigkeitsänderung oder beides geschehen.

# Nummer 46:

`Wo unterliegt Ihr Segelfahrzeug bzw. Ihre Motoryacht unter 20 m Länge einem Behinderungsverbot?`

In engen Fahrwassern,

auf dem Einbahnweg eines Verkehrstrennungsgebietes (VTG) gegenüber Maschinenfahrzeugen im VTG.

# Nummer 47:

`Welchen Abstand muss man von Minenräumfahrzeugen halten?`

Mindestens 1 000 m.

# Nummer 48:

`Auf einer Motoryacht A sieht man nachts etwa recht voraus Topplicht und beide Seitenlichter eines Fahrzeugs B. Die Lichter werden rasch heller.`

1. Was ist B?
2. Wie ist die Situation zu klären?

B ist ein Maschinenfahrzeug von weniger als 50 m Länge in Fahrt.

A und B müssen ihren Kurs so nach Steuerbord ändern, dass sie einander an der Backbord-Seite passieren. Dabei müssen A und B das Signal "ein kurzer Ton" geben.

# Nummer 49 (Abbildung fehlt):

`Auf einer Motoryacht A erkennt man nachts etwa 2 Strich an Backbord folgende Lichter des Fahrzeugs B, die rasch näher kommen.`

Lichterführung Maschinenfahrzeug von weniger als 50 m Länge in Fahrt dessen Steuerbord-Seite man sieht


Die Kompasspeilung zum Fahrzeug B ändert sich dabei nur geringfügig.
1. Worum handelt es sich bei Fahrzeug B?
2. Wer muss ausweichen?
3. Was muss Fahrzeug A tun?
B ist ein Maschinenfahrzeug von weniger als 50 m Länge in Fahrt, dessen Steuerbord-Seite man sieht.

B muss ausweichen, weil es die Motoryacht A an seiner Steuerbord-Seite hat.

Die Motoryacht A muss Kurs und Geschwindigkeit beibehalten.

# Nummer 50:

`Welchen Fahrzeugen muss eine Motoryacht ausweichen?`

Manövrierunfähigen Fahrzeugen,

manövrierbehinderten Fahrzeugen,

fischenden Fahrzeugen,

Segelfahrzeugen,

ggf. einem anderen Maschinenfahrzeug.

# Nummer 51 (Abbildung fehlt):

`Auf einer Motoryacht A sieht man nachts etwa querab an Steuerbord ein einzelnes weißes Licht in (nahezu) stehender Kompasspeilung. Näher kommend erkennt man unterhalb des weißen Lichtes und etwas rechts davon ein rotes Licht (Fahrzeug B).`

Lichterführung Topplicht und später Backbord-Seitenlicht eines Maschinenfahrzeuges von weniger als 50 m Länge in Fahrt


1. Worum handelt es sich?
2. Was müssen jeweils beide Fahrzeuge tun? (Begründung!)
Topplicht und später Backbord-Seitenlicht eines Maschinenfahrzeuges B von weniger als 50 m Länge in Fahrt.

A muss ausweichen, weil es B an seiner Steuerbord-Seite hat. A muss das Signal "ein kurzer Ton" geben.

B muss Kurs und Geschwindigkeit beibehalten.

# Nummer 52:

`Eine Motoryacht, Länge 8 m, treibt nachts manövrierunfähig in der Nordsee und sieht ein großes Fahrzeug direkt auf sich zukommen. Welche Maßnahmen hat die Motoryacht zu ergreifen?`

Ein Fahrzeug von weniger als 12 m Länge, das die zwei roten Rundumlichter senkrecht übereinander nicht führt, muss folgende Maßnahmen ergreifen:
Durch jedes andere verfügbare Mittel anzeigen, dass es manövrierunfähig ist, z. B. über UKW-Sprechfunk oder durch ein Schallsignal oder das Lichtsignal lang-kurz-kurz.

Bei weiterer Annäherung das andere Fahrzeug mit einer starken Handlampe anleuchten und so auf sich aufmerksam machen.

Führen eines weißen Rundumlichtes, das mit keinem anderen Licht verwechselt werden kann.

Abfeuern eines Signals "weißer Stern" oder "Blitz-Knall".

Sofort bei Eintritt der Manövrierunfähigkeit Verkehrszentrale informieren (wenn vorhanden).

# Nummer 53:

`Wie sind Fahrwasser in der Seeschifffahrtsstraßen-Ordnung im Sinne der KVR eingestuft?`

Fahrwasser der Seeschifffahrtsstraßen gelten als enge Fahrwasser im Sinne der KVR.

# Nummer 54:

`Erläutern Sie den Begriff "durchgehende Schifffahrt" auf einem Fahrwasser einer Seeschifffahrtsstraße.`

Die durchgehende Schifffahrt umfasst alle Fahrzeuge, die deutlich dem Fahrwasserverlauf einer Seeschifffahrtsstraße folgen. Dies erlaubt nach allgemeiner Verkehrsauffassung ein Abweichen von höchstens ± 10° von der Richtung des Fahrwassers. Dabei ist es gleichgültig, zu welchem Zweck das Fahrzeug betrieben wird.

# Nummer 55:

`Was fordern die Grundregeln für das Verhalten im Verkehr?`

Jeder Verkehrsteilnehmer

muss die Sicherheit und Leichtigkeit des Verkehrs gewährleisten,

darf andere (nicht nur Verkehrsteilnehmer!) nicht schädigen, gefährden oder mehr als unvermeidbar behindern oder belästigen.

# Nummer 56:

`Welche verkehrsrechtliche Verantwortung hat der Schiffsführer?`

Befolgung der Vorschriften im Verkehr, u. a. KVR, SeeSchStrO.

Ausrüstung/Einrichtung seines Fahrzeugs zum Führen und Zeigen von Lichtern und Signalkörpern und Geben von Schallsignalen.

# Nummer 57:

`Was sind Seeschifffahrtsstraßen im Sinne der SeeSchStrO?`

Seeschifffahrtsstraßen im Sinne dieser Verordnung sind:
Wasserflächen zwischen der Küstenlinie bei mittlerem Hochwasser oder der seewärtigen Begrenzung der Binnenwasserstraßen und einer Linie von drei Seemeilen seewärts der Basislinie,
die durchgehend durch laterale Zeichen (Tonnen) begrenzten Wasserflächen der seewärtigen Teile der Fahrwasser im Küstenmeer,

Wasserflächen zwischen den Ufern bestimmter Binnenwasserstraßen.

# Nummer 58:

`Was sind Fahrwasser im Sinne der SeeSchStrO?`

Fahrwasser sind die Teile der Wasserflächen, die durch Tonnen (laterale Zeichen) begrenzt oder gekennzeichnet sind oder die, soweit das nicht der Fall ist, auf den Binnenwasserstraßen für die durchgehende Schifffahrt bestimmt sind.

# Nummer 59:

`Welche verkehrsrechtlichen Bestimmungen gelten auf deutschen Seeschifffahrtsstraßen?`

Auf deutschen Seeschifffahrtsstraßen gelten:
die KVR,
die Seeschifffahrtsstraßen-Ordnung, ggf. die Bekanntmachungen der Wasser- und Schifffahrtsdirektionen (WSD) Nord und Nordwest,
ggf. die Hafenordnungen.

# Nummer 60:

`Wo und unter welcher Bedingung gelten im Geltungsbereich der SeeSchStrO die KVR?`

Die KVR gelten im gesamten Geltungsbereich der SeeSchStrO innerhalb und außerhalb der Fahrwasser, soweit die SeeSchStrO nicht ausdrücklich etwas anderes bestimmt (z. B. Vorfahrt, Grundsatz des Vorranges der spezielleren Rechtsvorschrift vor der allgemeineren).

# Nummer 61:

`Wie haben Segelfahrzeuge in einem Fahrwasser der SeeSchStrO untereinander auszuweichen, wenn sie nicht deutlich der Richtung eines Fahrwassers folgen?`

Sie haben untereinander nach den Regeln der KVR auszuweichen, wenn sie dadurch vorfahrtberechtigte Fahrzeuge nicht gefährden oder behindern.

# Nummer 62:

`Auf der Elbe hören Sie nachts vor sich von einem Fahrzeug, das zusätzlich zu seinen Fahrtlichtern ein rotes Rundumlicht führt, fortwährend das Schallsignal kurz-lang. Um welches Schallsignal handelt es sich, wann ist es zu geben und wie verhalten Sie sich?`

Es handelt sich um das Bleib-weg-Signal, das von einem Fahrzeug gegeben wird, bei dem bestimmte gefährliche Güter oder radioaktive Stoffe frei werden oder drohen frei zu werden oder es besteht Explosionsgefahr.

Man hat sich mit seinem Fahrzeug möglichst weit von dem anderen Fahrzeug zu entfernen (sicherer Abstand) und darf keine elektrischen Schalter bedienen. Kein offenes Feuer.

# Nummer 63:

`Wann ist von einem Fahrzeug auf einer Seeschifffahrtsstraße das "allgemeine Gefahr- und Warnsignal" zu geben und wie lautet es?`

Gefährdet ein Fahrzeug ein anderes Fahrzeug oder wird es durch dieses selbst gefährdet, hat es, soweit möglich, rechtzeitig das Schallsignal zu geben: ein langer Ton, vier kurze Töne; ein langer Ton, vier kurze Töne.

# Nummer 64:

`Nennen Sie die speziellen Verhaltensregeln für Sportfahrzeuge im Nord-Ostsee-Kanal (NOK).`

Sportfahrzeuge dürfen in der Regel die Zufahrten und den NOK lediglich zur Durchfahrt und ohne Lotsen nur während der Tagfahrzeiten und nicht bei verminderter Sicht benutzen.

Sportfahrzeuge müssen ihre Kanalfahrt so einrichten, dass sie vor Ablauf der Tagfahrzeit eine für Sportfahrzeuge bestimmte Liegestelle erreichen können.

Bei plötzlich auftretender verminderter Sicht dürfen Sportfahrzeuge in den Weichengebieten hinter den Dalben oder an geeigneten Liegestellen festmachen.

# Nummer 65:

`Welche speziellen Fahrregeln haben Sportfahrzeuge im Nord-Ostsee-Kanal (NOK) einzuhalten?`

Das Segeln ist auf dem NOK verboten.

Sportfahrzeuge mit Maschinenantrieb dürfen zusätzlich die Segel setzen.

Ein motorbetriebenes Sportfahrzeug darf nur ein Sportfahrzeug schleppen.

# Nummer 66:

`Während der Durchfahrt durch den Nord-Ostsee-Kanal (NOK) wird man auf einem Sportboot von Nebel überrascht. Was ist zu unternehmen?`

Schnellstmöglich in einem Weichengebiet hinter den Dalben oder an geeigneten Liegestellen festmachen.

# Nummer 67:

`Sie sehen vor dem Einlaufen in den NOK in Brunsbüttel folgende Lichtsignale:`
1. ein unterbrochenes rotes Licht,
2. ein unterbrochenes weißes Licht über einem unterbrochenen roten Licht,
3. ein unterbrochenes weißes Licht.
Geben Sie die Bedeutung dieser Signale an.

Einfahren verboten.

Freigabe wird vorbereitet.

Sportfahrzeuge können einfahren.

# Nummer 68:

`Erläutern Sie den Begriff "Vorfahrt beachten".`

"Vorfahrt beachten" begründet eine Wartepflicht. Wer die Vorfahrt zu beachten hat, muss rechtzeitig durch sein Fahrverhalten erkennen lassen, dass er warten wird. Er darf nur weiterfahren, wenn er übersehen kann, dass die Schifffahrt im Fahrwasser nicht beeinträchtigt wird. Ggf. hat der Wartepflichtige seinen Kurs und/oder seine Geschwindigkeit zu ändern (gilt rechtlich nicht als Ausweichen!).

# Nummer 69:

`Erläutern Sie den Begriff "Vorfahrt haben".`

"Vorfahrt haben" gilt nur für ein im Fahrwasser fahrendes oder dem Fahrwasserverlauf folgendes Fahrzeug. Das bedeutet, dass andere Fahrzeuge, die in das Fahrwasser einlaufen wollen, dort drehen oder an- und ablegen wollen, mit diesem Vorhaben warten müssen, bis das vorfahrtberechtigte Fahrzeug vorüber ist. "Vorfahrt haben" bedeutet aber nicht: Vorfahrt erzwingen! Ggf. muss ein vorfahrtberechtigtes Fahrzeug Maßnahmen zur Verhinderung einer drohenden Kollision ergreifen.

# Nummer 70:

`Wie hat sich ein in das Fahrwasser einlaufendes Fahrzeug gegenüber im Fahrwasser fahrenden Fahrzeugen zu verhalten?`

Es muss die Vorfahrt der Fahrzeuge im Fahrwasser beachten, d. h., es muss warten, bis das Fahrwasser frei ist. Es muss rechtzeitig durch sein Fahrverhalten erkennen lassen, dass es warten wird.

# Nummer 71:

`Wie hat sich ein den Ankerplatz oder Liegeplatz verlassendes Fahrzeug gegenüber im Fahrwasser fahrenden Fahrzeugen zu verhalten?`

Es muss die Vorfahrt der Fahrzeuge im Fahrwasser beachten, d. h. es muss warten, bis das Fahrwasser frei ist. Es muss rechtzeitig durch sein Fahrverhalten erkennen lassen, dass es warten wird.

# Nummer 72:

`Welche Fahrregeln muss ein Sportfahrzeug beachten, wenn es der Richtung des Fahrwassers folgt?`

Beim Fahren im Fahrwasser muss das Sportfahrzeug sich so nahe am äußeren Rand des Fahrwassers an seiner Steuerbordseite halten, wie dieses ohne Gefahr möglich ist.

# Nummer 73:

`Was muss ein Sportfahrzeug in Bezug auf das Fahrwasser beachten, wenn es außerhalb des Fahrwassers fährt?`

Außerhalb des Fahrwassers ist so zu fahren, dass klar erkennbar ist, dass das Fahrwasser nicht benutzt wird.

# Nummer 74:

`Wie müssen sich Segelfahrzeuge verhalten, die, dem Fahrwasserverlauf folgend, sich auf (nahezu) entgegengesetzten Kursen begegnen?`

Jedes Fahrzeug muss nach Steuerbord ausweichen.

# Nummer 75:

`Was bedeutet "Queren eines Fahrwassers" im Sinne der SeeSchStrO?`

Queren bedeutet deutliches Abweichen vom Fahrwasserverlauf, nach allgemeiner Verkehrsmeinung mehr als 10° (z. B. Kreuzen eines Segelfahrzeuges über die gesamte oder auch nur teilweise Fahrwasserbreite).

# Nummer 76:

`Wie müssen Sie die Geschwindigkeit Ihres Sportbootes einrichten, wenn Sie außerhalb eines Fahrwassers an Stellen mit erkennbarem Badebetrieb vorbeifahren?`

Höchstgeschwindigkeit 8 km/h im Abstand von weniger als 500 m vom Ufer.

# Nummer 77 (Abbildung fehlt):

`Sie sehen auf der Elbe bei Nacht ein Fahrzeug mit der nachfolgenden Lichterführung. Um was für ein Fahrzeug handelt es sich? Was bedeuten die beiden roten und die beiden grünen Lichter senkrecht übereinander?`

Lichterführung Manövrierbehindertes Fahrzeug, Länge wahrscheinlich 50 m oder mehr, von vorn mit Fahrt durchs Wasser, das Unterwasserarbeiten ausführt

Manövrierbehindertes Fahrzeug, Länge wahrscheinlich 50 m oder mehr, von vorn mit Fahrt durchs Wasser, das Unterwasserarbeiten ausführt (z. B. baggert).

Passierseite an Steuerbord (zwei grüne Rundumlichter übereinander), Passierbehinderung an Backbord-Seite (zwei rote Rundumlichter übereinander).

# Nummer 78 (Abbildung fehlt):

`Sie sehen auf der Elbe bei Tage ein Fahrzeug mit den nachfolgenden schwarzen Signalkörpern, dessen Bugwelle man klar erkennen kann. Um was für ein Fahrzeug handelt es sich? Was bedeuten die beiden schwarzen Bälle und die beiden schwarzen Rhomben senkrecht übereinander?`

Lichterführung entgegenkommendes manövrierbehindertes Fahrzeug von vorn, mit Fahrt durchs Wasser, das Unterwasserarbeiten ausführt

Entgegenkommendes manövrierbehindertes Fahrzeug von vorn, mit Fahrt durchs Wasser, das Unterwasserarbeiten ausführt (z. B. baggert).

Passierseite an Backbord des Baggers (zwei schwarze Rhomben übereinander), Passierbehinderung an Steuerbord-Seite des Baggers (zwei schwarze Bälle übereinander).

# Nummer 79:

`Beim Passieren von Cuxhaven sichten Sie, elbabwärts segelnd, an Ihrer Steuerbord-Seite die Tonne 32a. Um was für eine Tonne handelt es sich, welche Bezeichnung hat die nächste Tonne an der gleichen Seite?`

Es handelt sich um eine Backbordfahrwassertonne; die nächste Tonne hat die Aufschrift 32.

# Nummer 80:

`Welche besondere Lichterführung/Kennzeichnung ist vorgeschrieben, wenn ein Motorsportfahrzeug ein anderes Sportfahrzeug schleppt?`

Motorsportfahrzeuge, die andere Sportfahrzeuge schleppen, gelten nicht als schleppende Maschinenfahrzeuge im Sinne der KVR. Daher keine besondere Lichterführung/Kennzeichnung.

# Nummer 81:

`Welche besonderen Bestimmungen gelten auf dem Nord-Ostsee-Kanal (NOK) für Sportfahrzeuge beim Schleppen?`

Ein motorbetriebenes Sportfahrzeug darf nur ein Sportfahrzeug schleppen.

Das geschleppte Sportfahrzeug darf nur eine Höchstlänge von weniger als 15 m haben.

Die Mindestgeschwindigkeit beim Schleppen muss 9 km/h betragen.

# Nummer 82 (Abbildung fehlt):

`Während einer Revierfahrt erkennen Sie ein Signal an Land, jeweils schwarze Signalkörper:`
Signal Außergewöhnliche Schifffahrtsbehinderung
1. Was bedeutet dieses Signal?
2. Welches Signal wird stattdessen nachts gezeigt?

Außergewöhnliche Schifffahrtsbehinderung.

Nachts: Rundumlichter rot-rot-grün senkrecht übereinander.

# Nummer 83:

`Wer darf laut SeeSchStrO ein Fahrzeug nicht führen oder als Mitglied der Crew eine andere Tätigkeit des Brücken- oder Decksdienstes nicht ausüben (allgemein ohne Zahlen zu beantworten)?`

Wer infolge körperlicher oder geistiger Mängel oder des Genusses alkoholischer Getränke oder anderer berauschender Mittel in der sicheren Führung eines Fahrzeugs oder in der sicheren Ausübung einer anderen Tätigkeit des Brücken- oder Decksdienstes behindert ist.

# Nummer 84:

`Welche Atem- bzw. Blutalkoholkonzentration darf laut SeeSchStrO nicht erreicht werden, damit kein Verbot für ein Führen eines Fahrzeugs oder als Mitglied der Crew für ein Ausüben des Brückendienstes besteht?`

0,25 mg/l oder mehr Alkohol in der Atemluft oder 0,5 Promille oder mehr Alkohol im Blut oder eine Alkoholmenge, die zu einer solchen Atem- oder Blutalkoholkonzentration führt.

# Nummer 85:

`Was müssen Sie beim ersten Anlaufen eines ausländischen Hafens beachten?`

Die Einreise-, Gesundheits- und Zollformalitäten sind zu erledigen.

# Nummer 86:

`Was ist ein Flaggenzertifikat? Für welche Fahrzeuge kann es ausgestellt werden?`

Vom BSH ausgestellter Ausweis, mit dem das Recht und die Pflicht zum Führen der Bundesflagge nachgewiesen wird. Für Fahrzeuge unter 15 m Lüa = "nicht registerpflichtige Fahrzeuge".

# Nummer 87:

`Was ist das Schiffszertifikat, wer stellt es aus, ab welcher Schiffslänge ist es vorgeschrieben?`

Das Schiffszertifikat ist der Nachweis, dass ein Schiff im Seeschiffsregister eingetragen ist. Ausgestellt wird es vom Registergericht, Vorgeschrieben ist es ab 15 m Rumpflänge.

# Nummer 88:

`Was versteht man unter dem Begriff "Küstenmeer"?`

Die seewärts der Küstenlinie bei mittlerem Hochwasser oder der Basislinie gelegenen Meeresgewässer bis zu einer Breite von 12 sm.

# Nummer 89:

`Was versteht man unter dem Begriff "innere Gewässer"?`

Als "innere Gewässer" bezeichnet man die Gewässer landwärts der Basislinien.

# Nummer 90:

`Was versteht man unter dem Begriff "Basislinie" und wo finden Sie diese?`

Als Basislinie bezeichnet man die Grenze zwischen den inneren Gewässern (eines Staates) und dem Küstenmeer. Basislinien sind in Seekarten eingezeichnet.

# Nummer 91:

`Welche Aufgaben hat die Bundesstelle für Seeunfalluntersuchung?`

Amtliche Untersuchung eines schaden- oder gefahrverursachenden Vorkommnisses (Seeunfall) im Zusammenhang mit dem Betrieb eines Schiffes (z. B. Kollision zwischen zwei Fahrzeugen) und Ermittlung der Umstände, durch die es zu dem Seeunfall gekommen ist.

Herausgabe von Untersuchungsberichten und insbesondere Sicherheitsempfehlungen zur Verhütung von Seeunfällen.

# Nummer 92:

`Wann liegt ein schaden- oder gefahrverursachendes Vorkommnis (Seeunfall) im Sinne des Seesicherheits-Untersuchungs-Gesetzes (SUG) vor? Nennen Sie mindestens 3 Merkmale.`

Schiffsverlust, Aufgrundlaufen, Kollision eines Schiffes,

Tod oder Verschollenheit oder schwere Verletzung einer Person,

maritimer Umweltschaden oder sonstiger Sachschaden,

Gefahr für einen Menschen oder ein Schiff,

Gefahr eines schweren Schadens an einem Schiff, einem meerestechnischen Bauwerk oder an der Meeresumwelt.

# Nummer 93:

`Was müssen Sie nach einem Seeunfall veranlassen? Wie kann es umgesetzt werden?`

Den Seeunfall unverzüglich der Bundesstelle für Seeunfalluntersuchung melden. Das kann in einem deutschen Einlaufhafen auch über die Wasserschutzpolizei bzw. im Ausland über die zuständigen Hafenbehörden veranlasst werden.

# Nummer 94:

`Welche Angaben müssen der Bundesstelle für Seeunfalluntersuchung gemeldet werden? Nennen Sie mindestens 5 dieser Angaben.`

Es sind folgende Angaben zu melden:
- Name und derzeitiger Aufenthalt des Meldenden,
- Ort (geographische Position) und Zeit des Unfalles,
- Name, Rufzeichen und Flagge des Schiffes sowie Rufnummer des zu diesem Schiff gehörenden mobilen Seefunkdienstes (MMSI),
- Typ, Verwendungszweck,
- Name des Betreibers des Schiffes,
- Name des verantwortlichen Schiffsführers,
- Herkunfts- und Zielhafen des Schiffes,
- Anzahl der Besatzungsmitglieder und weiterer Personen an Bord,
- Umfang des Personen- und Sachschadens,
- Darstellung des Verlaufs des Vorkommnisses,
- Angaben über andere Schiffe, die am Unfall beteiligt sind,
- Wetterbedingungen,
- Darstellung der Gefahr einer Meeresverschmutzung.

# Nummer 95:

`In welcher Vorschrift ist geregelt, welche Angaben der Bundesstelle für Seeunfalluntersuchung bei einem schaden- oder gefahrverursachenden Vorkommnis (Seeunfall) gemeldet werden müssen? Wer ist verantwortlich für die Meldung?`

Geregelt in der "Verordnung über die Sicherung der Seefahrt".

Verantwortlich für die Meldung sind der Schiffsführer oder bei dessen Verhinderung ein anderes Besatzungsmitglied bzw. ggf. auch der Betreiber des Schiffes, falls keine der vorgenannten Personen dazu in der Lage ist.

# Nummer 96:

`Was sind Seeämter und was sind ihre Aufgaben?`

Seeämter sind bei den Wasser- und Schifffahrtsdirektionen Nord und Nordwest gebildete Untersuchungsausschüsse zur Untersuchung der Frage, ob gegenüber einem Verfahrensbeteiligten ein Fahrverbot ausgesprochen oder ein Befähigungszeugnis bzw. ein amtlicher Führerschein der Sportschifffahrt entzogen werden muss.

# Nummer 97:

`Welche behördlichen Veröffentlichungen für Wassersportler geben Ihnen rechtliche Informationen und Hinweise über das Verhalten auf Seeschifffahrtsstraßen?`

"Sicherheit auf dem Wasser, Leitfaden für Wassersportler".
"Sicherheit im See- und Küstenbereich, Sorgfaltsregeln für Wassersportler".

# Nummer 98:

`Sie sehen von Ihrem Sportfahrzeug aus in der Nordsee nördlich von Helgoland eine noch unbekannte Gefahr, z. B. einen teibenden Container. Was haben Sie zu unternehmen?`

Man muss dies auf dem schnellsten Weg direkt oder über eine Verkehrszentrale bzw. Küstenfunkstelle dem Maritimen Lagezentrum (MLZ) in Cuxhaven als Meldestelle für Unfälle auf See mitteilen.

# Nummer 99:

`Wann und wo wird eine Flagge des Gastlandes gehisst?`

Beim Einlaufen in die Küstengewässer des Gastlandes unter der Steuerbordsaling.

# Nummer 100:

`Wann und wo wird die Bundesflagge geführt?`

Auf in Betrieb befindlichen Yachten während der Flaggzeit in den Küstengewässern, auf See und im Hafen am Flaggenstock am Heck, auf segelnden mehrmastigen Yachten auch im Topp des hintersten Mastes (nicht am Achterstag).

# Nummer 101:

`Welche Befahrensregelungen gelten für die Schutzzonen I in den Nationalparks im Deutschen Wattenmeer außerhalb der speziellen Schutzgebiete?`

Das Verlassen der Fahrwasser zwischen 3 h nach Hochwasser und 3 h vor dem folgenden Hochwasser ist untersagt. In der übrigen Zeit beträgt für Sportfahrzeuge die Höchstgeschwindigkeit außerhalb des Fahrwassers 8 kn und generell im Fahrwasser 12 kn.

# Nummer 102:

`Wo sind die Grenzen der Schutzzonen I und der speziellen Schutzgebiete in den Nationalparks aufgeführt?`

In den Seekarten.

# Nummer 103:

`Welchen Zweck soll das MARPOL-Übereinkommen erfüllen?`

Das MARPOL-Übereinkommen soll die Verschmutzung der Meere verhindern.

# Nummer 104:

`Was sind Sondergebiete im Sinne des MARPOL-Übereinkommens in Europa?`

Ostsee, Nordsee und Mittelmeer.

# Nummer 105:

`Was ist nach dem MARPOL-Übereinkommen für die Sportschifffahrt in Sondergebieten grundsätzlich verboten?`

Das Einleiten von Öl, Schiffsabwässern, Schiffsmüll und anderen Schadstoffen.

# Nummer 106:

`Gilt das MARPOL-Übereinkommen grundsätzlich auch für Sportfahrzeuge?`

Das MARPOL-Übereinkommen gilt grundsätzlich für alle Schiffe, somit auch für Sportfahrzeuge.

# Nummer 107:

`Woraus können Sie Informationen über Entsorgungsmöglichkeiten in deutschen Sportboothäfen entnehmen?`

Aus der Broschüre "Entsorgungsmöglichkeiten für Öl, Schiffsmüll und Schiffsabwässer - eine Übersicht für Sport- und Kleinschifffahrt" des BSH.

# Nummer 108:

`Wie ist auf Sportfahrzeugen mit ölhaltigem Bilgenwasser zu verfahren, wenn die Bedingungen, unter denen nach MARPOL das Lenzen zulässig ist, nicht eingehalten werden können?`

Es muss im Hafen entsorgt werden.

# Nummer 109:

`Was ist hinsichtlich der Entsorgung von Müll in Nord- und Ostsee und im Mittelmeer zu beachten? (Begründung!)`

Da Nord-, Ostsee und Mittelmeer Sondergebiete nach MARPOL sind, darf dort kein Müll in die See entsorgt werden.

# Nummer 110:

`Welche Müllanteile dürfen in Sondergebieten nicht auf See entsorgt werden?`

Synthetische Seile, Netze, Segel, Kunststofftüten u. Ä., Papiererzeugnisse, Lumpen, Glas, Metall, Steingut, Schalungs- oder Verpackungsmaterial.