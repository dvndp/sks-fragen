# Nummer 1:

`Was ist Wind und wie entsteht er?`

Wind ist bewegte Luft. Die Bewegung entsteht durch die Druckunterschiede zwischen Hoch- und Tiefdruckgebieten.

# Nummer 2:

`Was ist der Taupunkt?`

Der Taupunkt ist die Temperatur, auf die Luft abgekühlt werden muss, damit sie mit Feuchtigkeit gesättigt ist. Es setzt Kondensation (Taubildung) ein.

# Nummer 3:

`In welcher Größe wird in der Schifffahrt die Luftfeuchtigkeit allgemein angegeben?`

Relative Feuchtigkeit in Prozent.

# Nummer 4:

`Nennen Sie mindestens 6 Parameter, aus denen sich eine Wetterbeobachtung an Bord zusammensetzt.`

Windrichtung, Windstärke, Luftdruck, aktuelles Wetter, Bedeckungsgrad, Wolken, Seegang, Strom, Temperatur und ggf. Luftfeuchte.

# Nummer 5:

`1. In welcher Maßeinheit wird die Windstärke angegeben?`
2. In welchen Maßeinheiten wird die Windgeschwindigkeit angegeben?

Nach der Beaufortskala (Bft).

In kn, m/s und km/h.

# Nummer 6:

`1. Wie heißen die Linien gleichen Luftdrucks?`
2. In welcher Maßeinheit wird der Luftdruck angegeben?

Isobaren.

Hektopascal (hPa) oder vereinzelt auch noch Millibar (mb, teilweise auch mbar).

# Nummer 7:

`Welche Gefahren kann ein Gewitter mit sich bringen?`

Böen bis Orkanstärke,

plötzliche Winddrehungen,

Regen- oder Hagelschauer mit zum Teil starker Sichtminderung,

Blitzschlag.

# Nummer 8:

`Wann entstehen besonders starke Gewitter?`

Besonders zum Ende einer hochsommerlichen Schönwetterperiode im Zusammenhang mit Kaltfronten.

# Nummer 9:

`Welche Skala wird verwendet für die Angabe der Windrichtung in Seewetterberichten bei`
1. den Vorhersagen und Aussichten,
2. den Stationsmeldungen?

Die 8-teilige mit Auflösung in 45°-Stufen.

Die 16-teilige mit Auflösung in 22,5°-Stufen.

# Nummer 10:

`Ab welcher Windstärke werden Orkanwarnungen ausgegeben?`

Ab Windstärke 10 Bft, erfahrungsgemäß mit Böen über Bft 12.

# Nummer 11:

`1. Welche Skala wird für die Schätzung der Windstärke verwendet?`
2. Was verstehen Sie unter mäßigem Wind, was unter Starkwind?

Die 12-teilige Beaufortskala.

Mäßiger Wind bedeutet Stärke 4 der Beaufortskala, Starkwind 6 und 7 Beaufort.

# Nummer 12:

`Welche amtlichen Veröffentlichungen enthalten Sendezeiten und Frequenzen für Seewetterberichte`
1. für Europa,
2. Europa und weltweit?

Das "Handbuch Nautischer Funkdienst" und der "Jachtfunkdienst".

Die "Admiralty List of Radio Signals".

# Nummer 13:

`Nennen Sie 6 Möglichkeiten, um Wetterinformationen an Bord zu erhalten.`

Hörfunksender (UKW, KW, MW, LW), Küstenfunkstellen, Verkehrszentralen, NAVTEX, SafetyNet (Satcom), Online-Dienste (z. B. SEEWIS-Online des Deutschen Wetterdienstes, T-Online), RTTY (Funkfernschreiben), Faskimile (Wetterfax), Faxpolling (z. B. SEEWIS-Fax des Deutschen Wetterdienstes), Telefonabruf, Törnberatung.

# Nummer 14:

`Welche Bedeutung für die Wetterentwicklung hat ein Halo um die Sonne und ein Hof um den Mond?`

Wolkenaufzug, meist Cirrostratus.
Ggf. Niederschlag und Wetterverschlechterung.

# Nummer 15:

`Bei welchen Wolkenformen müssen Sie mit erhöhter Böigkeit rechnen?`

Bei Haufenwolken, besonders beim Cumulonimbus (Schauer- und Gewitterwolke).

# Nummer 16:

`1. Welche Formen von Wolken gibt es?`
2. Nennen Sie 6 der 10 Haupttypen!

Es gibt Haufenwolken und Schichtwolken.

Cirrus, Cirrostratus, Cirrocumulus, Altostratus, Altocumulus, Nimbostratus, Stratocumulus, Stratus, Cumulus, Cumulonimbus.

# Nummer 17:

`1. Welche Höhen unterscheidet man bei Wolken?`
2. Welche Höhen haben sie etwa in den gemäßigten Breiten?

Tiefe, mittelhohe und hohe Wolken.

Tiefe Wolken zwischen 0 und 2 km, mittelhohe Wolken zwischen 2 und 7 km und hohe Wolken zwischen 7 und 13 km.

# Nummer 18:

`Woraus bestehen hohe Wolken?`

Aus kleinen Eiskristallen.

# Nummer 19:

`Woran erkennt man bei Wolkenbildung eine kräftige Gewitterentwicklung?`

Am Cumulonimbus, wenn er in großer Höhe einen ambossförmigen Schirm hat.

# Nummer 20:

`Welche Wolken kündigen oft schon vormittags kräftige Wärmegewitter an?`

Altocumulus castellanus (mittelhohe türmchenartige Haufenwolken).

# Nummer 21:

`Wie verhält sich der Wind in Bodennähe auf der Nordhalbkugel zwischen Hoch- und Tiefdruckgebieten?`

Er weht rechtsherum aus dem Hochdruckzentrum heraus und linksherum in den Tiefdruckkern hinein.

# Nummer 22:

`1. Was ist eine Front?`
2. Welche Fronten unterscheidet man im Allgemeinen?

Front ist die vordere Grenze einer Luftmasse in Bewegungsrichtung.

Warm-, Kalt- und Okklusionsfronten.

# Nummer 23:

`Wie verhält sich typischerweise der Luftdruck`
1. vor,
2. während und
3. nach dem Durchzuug einer Kaltfront?

Der Luftdruck ist gleichbleibend oder fälllt nur wenig.

Während des Durchgangs der Front erreicht der Luftdruck seinen tiefsten Wert.

Der Luftdruck steigt wieder deutlich an.

# Nummer 24:

`Was lässt sich aus der Darstellung der Isobaren in einer Wetterkarte erkennen?`

Windrichtung und Druckgefälle; je enger sie liegen, desto größer ist das Druckgefälle und desto stärker ist der Wind.

# Nummer 25:

`Warum weht der Wind nicht parallel zu den Isobaren? (Begründung)`

Durch die Bodenreibung ist der Wind rückgedreht (gegen den Uhrzeigersinn).

# Nummer 26:

`1. Wie weht der Wind über See in Bodennähe um ein Tiefdruckgebiet?`
2. Mit wie viel Grad Änderung in der Windrichtung müssen Sie etwa rechnen?

Der Wind weht nicht parallel zu den Isobaren, er ist rückgedreht und weht in das Tief hinein.

Ein bis zwei Strich bzw. ca. 10° bis 20°.

# Nummer 27:

`1. Wie weht der Wind über See in Bodennähe um ein Hochdruckgebiet?`
2. Mit wie viel Grad Änderung in der Windrichtung müssen Sie etwa rechnen?

Der Wind weht nicht parallel zu den Isobaren, er ist rückgedreht und weht aus dem Hoch hinaus.

Ein bis zwei Strich bzw. 10° bis 20°.

# Nummer 28:

`Welche Verlagerungsgeschwindigkeiten haben Tiefdruckgebiete:`
1. schnelle,
2. mittlere,
3. langsame?

Schnelle: 30 bis 50 kn.

Mittlere: 15 bis 30 kn.

Langsame: bis 15 kn.

# Nummer 29:

`Wie entstehen Tiefdruckgebiete?`

Durch das Aufeinandertreffen von kalten Luftmassen aus hohen Breiten und subtropischen warmen Luftmassen.

# Nummer 30:

`Welche Windverhältnisse herrschen in der Nähe des Zentrums eines Hochdruckgebiets?`

Meist schwache umlaufende Winde.

# Nummer 31:

`In welchem Abstand werden Isobaren international dargestellt oder gezeichnet?`

Im Abstand von 5 hPa oder im Abstand von 5 mbar.

# Nummer 32:

`Welche Sicht- und Wetterverhältnisse erwarten Sie typischerweise`
1. vor oder nahe der Warmfront,
2. im Warmsektor,
3. hinter der Kaltfront?

Sichtverschlechterung durch Niederschlag, bedeckt, länger andauernder Regen.

Diesig oder mäßige Sicht, Wolkenauflockerung, zeitweise Regen

Sichtbesserung, meist gute Sicht. Schauer mit zum Teil kräftigen Böen.

# Nummer 33 (Abbildung fehlt):

`Welche Windrichtungen erwarten Sie an den Punkten 1, 2, 3, 4, 5 eines Tiefdruckgebiets auf der Nordhalbkugel?`

Abbildung Windrichtungen Nordost, Süd, Südwest, Nordwest und umlaufender Wind

Nordost.

Süd.

Südwest.

Nordwest.

Umlaufender Wind.

# Nummer 34 (Abbildung fehlt):

`Um welche Arten von Fronten handelt es sich in der Abbildung, die mit 1, 2 und 3 bezeichnet sind?`

Abbildung Okklusionsfront (Tiefausläufer), Warmfront und Kaltfront

Okklusionsfront (Tiefausläufer).

Warmfront.

Kaltfront.

# Nummer 35:

`1. Was sind Luftmassengrenzen?`
2. Welche Luftmassengrenzen kennen Sie? Nennen Sie mindestens 2 Beispiele.

Luftmassengrenzen sind Fronten. Sie trennen Luftmassen mit unterschiedlicher Temperatur und Luftfeuchtigkeit.

Kaltfront, Warmfront, Okklusion.

# Nummer 36:

`Mit welchen lokalen Windsystemen müssen Sie insbesondere im Mittelmeer rechnen?`

Mit der Land-/Seewind-Zirkulation.

# Nummer 37:

`Nennen Sie mindestens 3 regionale Windsysteme im Mittelmeer, die beim küstennahen Segeln im Mittelmeer besonders beachtet werden müssen.`

Mistral,
Scirocco,
Bora und
Etesien/Meltemi.

# Nummer 38:

`Mit welchem regionalen Windsystem muss in der Adria gerechnet werden?`

Mit Bora.

# Nummer 39:

`Mit welchem regionalen Windsystem muss in der Ägäis gerechnet werden?`

Mit den Etesien/dem Meltemi.

# Nummer 40:

`Wo bilden sich Tröge um ein Tiefdruckgebiet?`

Auf der Rückseite von Tiefdruckgebieten in hochreichender Kaltluft. Ein Trog folgt typischerweise einer Kaltfront.

# Nummer 41:

`Welche Front wird auch als "Ausläufer" bezeichnet?`

Die Okklusion.

# Nummer 42:

`Wodurch und wie entsteht am Tage Seewind?`

Das Land erwärmt sich bei Sonneneinstrahlung tagsüber stärker als das Wasser. Über Land steigt die erwärmte Luft auf. Das dabei entstehende Bodentief wird durch Seewind (Wind von See) aufgefüllt.

# Nummer 43:

`Welche Wolkenform zeigt sich am späten Vormittag über Land am Himmel und kündigt Seewind an?`

Haufenwolke (Cumulus).

# Nummer 44:

`Welche Windgeschwindigkeiten in Knoten oder Beaufort erreicht der Seewind etwa`
1. im Mittelmeer,
2. in Nord- und Ostsee?

Bis zu 25 kn oder Bft 6.

Bis 15 kn, in Einzelfällen bis 20 kn oder Bft 4/5, in Einzelfällen Bft 5/6.

# Nummer 45:

`Zu welcher Tageszeit müssen Sie mit Seewind rechnen?`

Von Mittag bis zum frühen Abend.

# Nummer 46:

`Welche Windänderung kann der einsetzende Seewind bewirken?`

Er verändert den vorher wehenden Wind zum Teil erheblich in Richtung und Stärke.

# Nummer 47:

`Wodurch und wie entsteht nachts Landwind?`

Das Land kühlt sich bei geringer Bewölkung stark ab. Das Wasser ändert seine Temperatur an der Oberfläche dagegen nur geringfügig. Über dem Wasser steigt daher erwärmte Luft auf. Das dabei entstehende Bodentief wird durch Landwind (Wind von Land) aufgefüllt.

# Nummer 48:

`Welche Windgeschwindigkeiten erreicht nachts der Landwind?`

Er weht allgemein schwächer als der Seewind, etwa 1 bis 10 kn oder Bft 1-3.

# Nummer 49:

`Wann müssen Sie im Laufe eines Tages mit Landwind rechnen?`

Von Mitternacht bis zum frühen Morgen.

# Nummer 50:

`Im Internet finden Sie auf einer "Wetterseite" eine Vorhersagekarte mit Windpfeilen. In welcher Höhe über dem Erdboden/der Wasseroberfläche gelten die vorhergesagten Windgeschwindigkeiten?`

Meistens etwa 10 Meter über dem Erdboden/der Wasseroberfläche.

# Nummer 51:

`Sie segeln mit Ihrer Yacht "raumschots". Nach der nächsten Tonne müssen Sie anluven. Wie wird sich die wahre Windgeschwindigkeit auf Ihrem Windmesser/Anemometer entwickeln?`

Sie bleib unverändert.

# Nummer 52:

`Welche Windsituation ist mit der Formulierung "Nordwest 6" bezüglich`
1. der Schwankungsbreite in Windrichtung und
2. der Schwankungsbreite in der Windstärke (Böen) verbunden?

Die Schwankung in der Windrichtung kann bis zu 45° um die Hauptwindrichtung betragen, also von Westnordwest (WNW) bis Nordnordwest (NNW).

Es können Böen auftreten, die etwa 1 bis 2 Bft über dem Mittelwind liegen.

# Nummer 53:

`Was ist mit dem Zusatz "Schauerböen" bei der Windvorhersage verbunden?`

Besonders während der Passage und auf der Rückseite von Kaltfronten treten in der näheren Umgebung von Schauern Böen auf, die den Mittelwind um 2 Bft überschreiten können.

# Nummer 54:

`Warum werden Gewitterböen in der Windvorhersage zusätzlich angegeben?`

Besonders im Sommer können bei Schwachwindlagen Gewitter mit Böen auftreten, die Sturm- oder Orkanstärke erreichen können.

# Nummer 55:

`Wie ist der Aufbau von Seewetterberichten?`

Hinweise auf Starkwind oder Sturm, Wetterlage, Vorhersagen, Aussichten und Stationsmeldungen.

# Nummer 56:

`Welche lokalen Effekte, die das vorherrschende Windfeld stark verändern, können in Seewetterberichten nur eingeschränkt berücksichtigt werden?`

U. a. Land-/Seewind-Zirkulation, Düsen- und Kapeffekte.

# Nummer 57:

`1. Wann werden Starkwindwarnungen verbreitet?`
2. Welche Bezeichnung hat die Starkwindwarnung im internationalen Sprachgebrauch?

Bei erwarteten oder noch andauernden Windstärken zwischen 6 und 7 Bft.

Near-gale warning.

# Nummer 58:

`1. Wann werden Sturmwarnungen verbreitet?`
2. Welche Bezeichnung hat die Sturmwarnung im internationalen Sprachgebrauch?

Bei zu erwartenden oder noch andauernden Windstärken von mindestens 8 Bft.

Gale warning.

# Nummer 59:

`Welche Wellenhöhe wird bei der Angabe des Seegangs in Seewetterberichten verwendet?`

Die kennzeichnende (charakteristische) Wellenhöhe.

# Nummer 60:

`1. Wie ist die kennzeichnende (charakteristische) Wellenhöhe definiert?`
2. Womit müssen Sie rechnen?

Mittlere Höhe der gut ausgeprägten (Mittel des oberen Drittels) - nicht extremen - Wellen.

Einzelne Wellen können das 1,5fache der kennzeichnenden Wellenhöhe erreichen.

# Nummer 61:

`Was bedeutet rechtdrehender bzw. rückdrehender Wind?`

Rechtdrehend bedeutet Änderung der Windrichtung im Uhrzeigersinn. Rückdrehend bedeutet Änderung der Windrichtung gegen den Uhrzeigersinn um mindestens 45°.

# Nummer 62:

`Sie hören am Ende eines Seewetterberichts die Stationsmeldungen. Was sagen Windrichtung und Windgeschwindigkeit gegenüber den Verhältnissen auf See aus?`

Durch die Umgebung der Wetterstation kann die Windrichtung verfälscht werden. Die Windgeschwindigkeit ist meist reduziert, in Einzelfällen auch erhöht.

# Nummer 63:

`Welche Sichtweiten umfasst der Begriff "diesig"?`

Sichtweiten über 1 km bis 10 km (bzw. ca. 0,5 bis 6 Seemeilen).

# Nummer 64:

`Seegebiete sind international festgelegt. In welchen amtlichen Veröffentlichungen können Sie nachlesen, wo sich das Seegebiet "Fischer" befindet?`

Im "Handbuch Nautischer Funkdienst", im "Jachtfunkdienst für Nord- und Ostsee" oder in der "Admiralty List of Radio Signals".

# Nummer 65:

`Sie wollen einen Törn in einem für Sie fremden Küstenrevier fahren. Wie können Sie sich über mittlere Windverhältnisse für bestimmte Jahreszeiten oder Monate informieren?`

In den entsprechenden Hafen-, Revierführern. Außerdem z. B. in Monatskarten.

# Nummer 66:

`1. Was für Wetter muss meistens erwartet werden, wenn der Luftdruck über einen Zeitraum von 3 Stunden um 10 hPa fällt?`
2. Was muss bei einem an Bord beobachteten starken Luftdruckfall beachtet werden?

Schwerer Sturm.

Der Kurs und die Fahrt des Schiffes in Bezug auf das Tiefdruckgebiet.

# Nummer 67:

`Wie verändert sich der an Bord beobachtete Luftdruckfall, wenn sich ein Fahrzeug mit Westkurs dem Zentrum eines ostwärts ziehenden Tiefdruckgebiets nähert?`

Der Luftdruckfall wird verstärkt.

# Nummer 68:

`Mit welchen Windverhältnissen müssen Sie rechnen, wenn Sie im Hafen liegen und der Wind ablandig weht?`

Die im Hafen vorherrschenden Windgeschwindigkeiten entsprechen nicht den Verhältnissen auf der freien See.

# Nummer 69:

`Mit welchen Windverhältnissen müssen Sie rechnen, wenn Sie in einem relativ ungeschützten Hafen liegen und der Wind auflandig weht?`

Die im Hafen vorherrschenden Windgeschwindigkeiten entsprechen etwa den Verhältnissen auf der freien See.

# Nummer 70:

`Warum verstärkt sich der Wind in engen Durchfahrten?`

Durch den Düseneffekt (Trichtereffekt) in Durchfahrten. Dabei wird die Luftströmung "zusammengepresst" und beschleunigt.

# Nummer 71:

`Mit welcher Windentwicklung ist zu rechnen`
1. in Luv und
2. in Lee von Kaps oder Inseln?

Die Windrichtung ändert sich in Luv des Kaps zum Teil stark und verläuft oft parallel zum Kap. Die Windgeschwindigkeit nimmt zu.

Die Windrichtung kann bei besonders hohen Gebirgen auch umlaufend werden. Die Windgeschwindigkeit ist meist schwach, kann dafür örtlich aber sehr böig sein (Fallwinde).

# Nummer 72:

`Welche Windverhältnisse erwarten Sie in der Nähe von Steilküsten`
1. bei auflandigem und
2. bei ablandigem Wind?

Der Wind wird durch Küstenführung zum Teil beschleunigt, wenn er nahezu auflandig oder parallel zur Küste weht.

Weht der Wind ablandig, muss örtlich mit umlaufenden Winden und erhöhter Böigkeit (Fallwinden) gerechnet werden.

# Nummer 73:

`Wie wird sich das Wetter wahrscheinlich entwickeln, wenn der Wind am Abend`
1. abflaut oder
2. zunimmt?

Langsames Abflauen des Windes ist oft ein Zeichen für gutes Wetter.

Windzunahme am Abend kündigt häufig Starkwind, Sturm und Regen an.

# Nummer 74:

`1. Womit müssen Sie auf der Nordhalbkugel rechnen, wenn nach Durchzug einer Kaltfront der Wind rückdreht und der Luftdruck wieder fällt?`
2. Wie nennt man die Wetterlage?

Meist deutliche Wetterverschlechterung mit erneut auffrischendem Wind bis Sturmstärke.

Troglage.

# Nummer 75:

`Welche Windverhältnisse erwarten Sie auf der Nordhalbkugel während der unmittelbaren Passage eines markanten Troges?`

Der Wind dreht recht, meist über 60 bis 90°. Winde bis Orkanstärke besonders auf der Rückseite eines Troges.

# Nummer 76:

`Wie entsteht Nebel?`

Zufuhr von Feuchte, Mischung von Luftmassen mit hoher Feuchtigkeit und verschiedener Temperatur, Abkühlung der Luftmasse.

# Nummer 77:

`Wie ist Nebel definiert?`

Sichtweite unter 1 000 Meter.

# Nummer 78:

`1. Wie entsteht Kaltwassernebel?`
2. Zu welcher Jahreszeit tritt diese Nebelart in europäischen Gewässern bevorzugt auf?

Warme und feuchte Luftmassen werden durch den kalten Untergrund (Meer) unter den Taupunkt abgekühlt.

Überwiegend im Frühjahr.

# Nummer 79:

`1. Wie entsteht Warmwassernebel?`
2. Zu welcher Jahreszeit tritt diese Nebelart in europäischen Gewässern bevorzugt auf?

Kalte Luft strömt über warmes Wasser. Durch Verdunstung an der Wasseroberfläche kommt es bei hoher Differenz zwischen der Luft- und Wassertemperatur zur Feuchtesättigung.

Überwiegend im Herbst.

# Nummer 80:

`1. Wie entsteht Strahlungsnebel?`
2. Wo ist diese Nebelart anzutreffen?

Nach Sonnenuntergang kann sich bei klarem Himmel die bodennahe Luftschicht über Land unter den Taupunkt abkühlen.

Besonders auf Flüssen und engen Durchfahrten, außerdem durch seewärtige Windverdriftung in Küstennähe.

# Nummer 81:

`Wodurch kann es im Mittelmeerraum in besonderen Fällen zur Sichtreduktion kommen?`

Bei bestimmten Wetterlagen kann mit der Luftmasse transportierter Saharastaub die Sicht stark vermindern.

# Nummer 82:

`Woraus besteht Seegang?`

Aus Windsee und Dünung.

# Nummer 83:

`Was verstehen Sie unter Windsee?`

Seegang, der durch den Wind am Ort oder in der näheren Umgebung angefacht wird.

# Nummer 84:

`Wovon hängt die Höhe der Windsee ab?`

Windgeschwindigkeit, Fetch (Windwirklänge) und Wirkdauer des Windes.

# Nummer 85:

`1. Was verstehen Sie unter Dünung?`
2. Was kann einsetzende hohe Dünung andeuten?

Seegang, der dem erzeugenden Windfeld vorausläuft, sowie abklingender (alter) Seegang.

Einen eventuell aufziehenden Sturm.

# Nummer 86:

`Was verstehen Sie unter der Wellenhöhe?`

Der senkrechte Abstand zwischen Wellenberg und Wellental.

# Nummer 87:

`Was verstehen Sie unter der Wellenlänge?`

Der horizontale Abstand zwischen zwei Wellenbergen.

# Nummer 88:

`Welchen Seegang müssen Sie erwarten, wenn Sie küstennah bei ablandigem Wind fahren?`

Der Seegang wird nicht so hoch sein wie auf der freien See, da der Fetch (Windwirklänge) nur sehr kurz ist.

# Nummer 89:

`1. Welchen Seegang müssen Sie erwarten, wenn Sie küstennah bei auflandigem Wind fahren?`
2. Welche Gefahr besteht bezüglich der Entwicklung des Seegangs außerdem?

Der Seegang wird ähnlich ausgeprägt sein wie auf der freien See, da genügend Fetch (Windwirklänge) vorhanden ist.

Dort, wo das Wasser flacher wird, oder im Bereich von Untiefen muss mit Brechern und Grundseen gerechnet werden.

# Nummer 90:

`1. Was verstehen Sie unter einer Grundsee?`
2. Welche Höhen kann sie erreichen?

Meereswellen mit besonders hohen Brechern, die durch Untiefen oder Küstennähe bzw. durch ansteigenden Meeresboden entstehen.

Etwa das 2,5fache der kennzeichnenden (charakteristischen) Wellenhöhe.

# Nummer 91:

`Wie verändert sich Seegang, wenn Wind und Meeresströmungen (z. B. Gezeitenstrom) entgegengesetzte Richtungen haben?`

Die Wellen werden kürzer und steiler.

# Nummer 92:

`Wie verändert sich Seegang, wenn Wind und Meeresströmungen (z. B. Gezeitenstrom) die gleiche Richtung haben?`

Die Wellen werden länger und flacher.

# Nummer 93:

`1. Was verstehen Sie unter einer Kreuzsee?`
2. Geben Sie 3 Beispiele an, wo mit Kreuzsee zu rechnen ist.

Windsee und Dünung laufen aus unterschiedlichen Richtungen heran.

Kurz vor und bei dem Durchzug einer Kaltfront oder eines Troges sowie in der Nähe des Tiefkerns.

# Nummer 94:

`Welcher Seegang ist in Lee kleiner Inseln zu erwarten?`

Kreuzlaufende See, die meist kurz und kabbelig ist.

# Nummer 95:

`Welche Faktoren können die Länge und Höhe des Seegangs erheblich verändern?`

Wassertiefe sowie Meeres- und Gezeitenströmungen.

# Nummer 96:

`Im Internet finden Sie auf einer "Wetterseite" eine Vorhersagekarte für die Dünung. Können Sie daraus ungefähr den vorherrschenden Wind über See ableiten?`

Nein. Dünung kann vorhanden sein, auch wenn kein Windfeld unmittelbar vorhanden ist.

# Nummer 97:

`Mit welchem Messinstrument wird an Bord die Windgeschwindigkeit gemessen?`

Mit einem Anemometer.

# Nummer 98:

`Welche Windgeschwindigkeit zeigt das Anemometer an, wenn das Fahrzeug Fahrt durchs Wasser macht?`

Die scheinbare Windgeschwindigkeit.

# Nummer 99:

`1. Warum sollten Luftdrucktendenzen an Bord beobachtet und aufgezeichnet werden?`
2. In welchem zeitlichen Abstand sollte man den Luftdruck aufzeichnen?

Eventuelle Wetterveränderungen (z. B. Trog, Annäherung eines Tiefdruckgebiets) können registriert werden.

Mindestens alle 4 Stunden.

# Nummer 100:

`Mit welchem Messinstrument wird an Bord der Luftdruck gemessen?`

Mit dem Barometer oder Barographen.

# Nummer 101:

`1. Wie bestimmen Sie an Bord die Windstärke, wenn keine Windmessanlage vorhanden ist?`
2. Wie bestimmen Sie an Bord die Windrichtung, wenn keine Windmessanlage vorhanden ist?

Die Windstärke wird geschätzt mit Hilfe der Beaufortskala in Anlehnung an das Seegangsbild.

Die Windrichtung wird anhand der Verlagerung der Wellenkämme geschätzt.