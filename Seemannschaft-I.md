# Nummer 1:

`Was versteht man im Bootsbau unter Gelcoat?`

Die äußere Schutzschicht eines Bauteils aus glasfaserverstärktem Kunststoff.

# Nummer 2:

`Was versteht man beim GFK-Bootsbau unter Sandwichverfahren (GFK = glasvaserverstärkter Kunststoff)?`

Zwischen zwei GFK-Schichten wird eine Zwischenlage zur Versteifung einlaminiert, z. B. aus Balsaholz.

# Nummer 3:

`Welchen Vorteil hat die Sandwichbauweise gegenüber der Massivbauweise bei GFK-Yachten (GFK = glasfaserverstärkter Kunststoff)?`

Große Steifheit, Verwindungsfestigkeit, geringes Gewicht, gute Isolierung.

# Nummer 4:

`Beschreiben Sie die Vor- und Nachteile von Stahl als Baumaterial für Yachten.`

Vorteile:
zuverlässiges, problemloses Baumaterial mit sehr hoher Festigkeit und langer Lebensdauer.

Nachteile:
hohes Gewicht, Rostanfälligkeit.

# Nummer 5:

`Was bedeutet der Begriff "Kategorie" im Zusammenhang mit dem CE-Zeichen für Wassersportfahrzeuge?`

Mit der Kategorie legt der Hersteller fest, in welchem Fahrgebiet, bis zu welcher Windstärke und bis zu welcher charakteristischen Wellenhöhe das Fahrzeug sicher betrieben werden kann.

# Nummer 6:

`Welche Kategorien können im Zusammenhang mit dem CE-Zeichen für Wassersportfahrzeuge vergeben werden?`

A Hochsee,

B Außerhalb von Küstengewässern,

C Küstennahe Gewässer,

D Geschützte Gewässer.

# Nummer 7:

`Was bedeutet die im Zusammenhang mit dem CE-Zeichen für Wassersportfahrzeuge angegebene Kategorie "B Außerhalb von Küstengewässern"?`

Das Fahrzeug ist ausgelegt für Fahrten außerhalb von Küstengewässern, in denen Windstärken bis einschließlich 8 Bft und signifikante Wellenhöhen bis einschließlich 4 m auftreten können.

# Nummer 8:

`Beschreiben Sie den Aufbau einer Radsteuerung mit Seilzügen.`

Das Rad dreht ein Zahnrad, über das eine Kette in der Steuersäule nach unten verläuft. Die Kette ist mit den Steuerseilen verbunden, welche über Umlenkrollen zum Ruderquadranten führen.

# Nummer 9:

`Wie nennt man die Teile des "stehenden Gutes", die den Mast nach vorn, achtern und seitlich verankern?`

Stagen und Wanten.

# Nummer 10:

`1. Wozu dienen Backstagen? 2. Bei welchen Takelungen werden sie vor allem gefahren?`

Zum zusätzlichen Abstagen des Mastes nach achtern.

Bei 7/8-Takelung und anderen nicht toppgetakelten Yachten.

# Nummer 11:

`Was ist das "laufende Gut"?`

Tauwerk, das zum Setzen, Bergen oder Bedienen der Segel oder anderer Teile der Takelage dient.

# Nummer 12:

`Was gehört zur regelmäßigen Pflege der Segel?`

Nasse Segel trocknen,

Segel vor Sonnenlicht schützen,

Salzwasserreste abspülen und

Beschädigungen umgehend beseitigen.

# Nummer 13:

`Warum sollten Vorsegel für schweres Wetter im Unterliek hoch geschnitten sein?`

Damit überkommende Seen nicht ins Segel schlagen und so Rigg und Segel belasten.

# Nummer 14:

`Was ist ein Trysegel und wie wird es gefahren?`

Ein Schwerwettersegel, das anstelle des Großsegels mit losem Unterliek gefahren wird.

# Nummer 15:

`Warum soll man das Schlagen eines Segels vermeiden, vor allem bei stärkerem Wind?`

Um Beschädigungen zu vermeiden.

# Nummer 16:

`Wozu dienen Segellatten?`

Zur Profilierung des Segels, damit das Achterliek nicht einklappt.

# Nummer 17:

`Wozu dient ein "Cunningham-Stropp"?`

Zur Regulierung der Vorliekspannung des Großsegels, um es so zu trimmen.

# Nummer 18:

`Welche Segel sollte eine Segelyacht in der Küstenfahrt mindestens an Bord haben?`

Reffbares Großsegel,

reffbare Rollfock oder Vorsegel verschiedener Größen,

Sturmfock.

# Nummer 19:

`Wozu dient eine Vorsegel-Rollreffeinrichtung?`

Mit ihr wird das Vorsegel um das Vorstag gerollt und kann so stufenlos verkleinert werden.

# Nummer 20:

`Wozu dient eine Großsegel-Rollreffeinrichtung?`

Mit ihr wird das Großsegel entweder im Mast oder im Baum aufgerollt und kann stufenlos verkleinert werden.

# Nummer 21:

`Wozu dient der Lenzkorb am Ansaugstutzen einer Lenzpumpe und wie erhalten Sie damit ihre Funktionsfähigkeit?`

Der Lenzkorb verhindert Verunreinigungen und Verstopfungen der Lenzpumpe. Er muss regelmäßig überprüft und gereinigt werden.

# Nummer 22:

`Welche Lenzvorrichtungen und -möglichkeiten sollten auf jeder seegehenden Yacht vorhanden sein?`

Zwei voneinander unabhängige Bilgenpumpen, von denen eine über Deck und eine unter Deck bedienbar ist, sowie 2 Pützen mit Leinen.

# Nummer 23:

`Warum sollten Sie mehr als einen Anker an Bord haben, möglichst unterschiedlicher Art?`

Als Ersatz bei Verlust,

zum Verwarpen oder Verkatten,

um unterschiedliche Ankergründe berücksichtigen zu können,

um bei schwerem Wetter oder in Tidengewässern vor 2 Ankern liegen zu können.

# Nummer 24:

`Wie viele Fender und Festmacherleinen sollten Sie mindestens an Bord haben?`

4 Festmacherleinen und 4 Fender.

# Nummer 25:

`Was sollte auf jeder Yacht außer Festmacherleinen, Fallen und Schoten an Tauwerk vorhanden sein?`

Reservetauwerk, Wurfleine, Schlepptrosse und Ankerleine.

# Nummer 26:

`Für welche unterschiedlichen Reparaturbereiche sollten Sie Ersatzteile und Werkzeug an Bord haben?`

Segelreparaturen,

Reparaturen an Rumpf und Rigg,

Motorreparaturen,

Elektroreparaturen und

Reparaturen an Schlauchleitungen.

# Nummer 27:

`Welches Werkzeug sollten Sie zur Segelreparatur an Bord haben?`

Segelhandschuh, Segelnadeln, Segelgarn, Wachs, Zange und selbstklebendes Segeltuch.

# Nummer 28:

`Ihr Mast ist gebrochen, eine Bergung ist nicht möglich. Welche Werkzeuge benötigen Sie, um die Takelage zu kappen?`

Bolzenschneider, Metallsäge mit Ersatzblättern, Schraubenschlüssel und verschiedene Zangen.

# Nummer 29:

`Welches Kleinmaterial und Kleinwerkzeug muss an Bord jederzeit greifbar sein?`

Zeisinge, Bändselwerk, Tape, Reserveschäkel, Schäkelöffner, Bordmesser und Kombizange.

# Nummer 30:

`Womit muss insbesondere eine Yacht mit Radsteuerung zusätzlich ausgerüstet sein, und warum sollten alle Mitsegler mit dieser Einrichtung vertraut sein?`

Mit einer Notpinne. Sie muss ggf. in kürzester Zeit einsatzbereit sein.

# Nummer 31:

`Wo finden Sie amtliche Informationen über die Ausrüstung und Sicherheit von Sportbooten, die auch bei der Beurteilung von Sportbootunfällen herangezogen werden?`

"Sicherheit im See- und Küstenbereich, Sorgfaltsregeln für Wassersportler", herausgegeben vom BSH,

"Sicherheit auf dem Wasser, Leitfaden für Wassersportler", herausgegeben vom Bundesministerium für Verkehr, Bau und Stadtentwicklung.

# Nummer 32:

`Warum müssen auf Yachten zusätzlich zu elektrisch oder motorgetriebenen Lenzpumpen auch Handlenzpumpen vorhanden sein?`

Weil sie auch bei Strom- oder Motorausfall betätigt werden können.

# Nummer 33:

`Warum ist Flüssiggas (Propan, Butan) an Bord einer Yacht besonders gefährlich?`

Es ist schwerer als Luft, sinkt nach unten und bildet mit Luft ein explosives Gemisch; es kann sich im Schiffsinneren (z. B. in der Bilge) sammeln.

# Nummer 34:

`Welche 4 Bedienelemente besitzt ein mit Handpumpe betriebenes Bord-WC auf einer Yacht?`

Seeventil und Spülwasserschlauch (Seewasser),

Handpumpe für Toilettenspülung,

Hebel zur Unterbrechung der Seewasserzufuhr (Handpumpe dient dann nur noch zum Abpumpen),

Abwasserschlauch (via Fäkalientank) zum Seeventil.

# Nummer 35:

`Beschreiben Sie in 5 Schritten die Bedienung eines Bord-WC auf einer Yacht.`

Seeventil für Seewasserspülung öffnen,

Handpumpe betätigen, sodass das Becken gespült wird und gleichzeitig die Fäkalien abfließen - ausgiebig spülen,

Seewasserzufuhr unterbrechen (Hebel umlegen),

Becken mit Handpumpe leer pumpen,

Seeventile für Zu- und Abfluss schließen.

# Nummer 36:

`Was versteht man unter der "Stabilität" eines Schiffes?`

Unter Stabilität eines Schiffes versteht man seine Eigenschaft, in aufrechter Lage zu schwimmen und sich aus einer Krängung wieder aufzurichten.

# Nummer 37:

`Wovon hängt die Stabilität eines Schiffes in ruhigem Wasser ab? Nennen Sie Beispiele für äußere Momente, welche die Stabilität beanspruchen.`

Die Stabilität eines Schiffes hängt ab von:
seiner Geometrie (Form),

der Gewichtsverteilung im Schiff (Ausrüstung, Crew, Ballast).
Beispiele für eine Beanspruchung der Stabilität sind krängende Momente durch Seitenwind, Trossenzug oder Drehkreisfahrt bei schnellen Motoryachten.

# Nummer 38:

`Wovon hängt eine in ruhigem Wasser vorhandene Stabilität zusätzlich in schwerem Wetter ab?`

Die Stabilität in schwerem Wetter hängt zusätzlich von Wind und Seegang, besonders von brechenden Wellen ab.

# Nummer 39:

`Was versteht man unter 1. Formschwerpunkt (F)? 2. Massenschwerpunkt (Gewichtsschwerpunkt, G)? Welche Kräfte wirken in den beiden Punkten?`

Im Formschwerpunkt F kann man sich die Masse des vom Schiff verdrängten Wassers vereinigt denken. In F wirkt die Auftriebskraft senkrecht zur Wasseroberfläche nach oben.

Im Massenschwerpunkt G kann man sich die Masse des Schiffes einschließlich Ausrüstung und Besatzung vereinigt denken. In G wirkt die Gewichtskraft senkrecht zur Wasseroberfläche nach unten.

# Nummer 40:

`Was geschieht bei einer Neigung des Schiffes, z. B. durch seitlichen Winddruck, solange sich die Lage des Massenschwerpunktes (Gewichtsschwerpunktes) nicht verändert? (Begründung!)`

Der Formschwerpunkt F wandert zur geneigten Seite aus, weil dort ein größerer Teil des Bootskörpers unter Wasser gelangt. Die Wirklinie der Auftriebskraft bekommt dadurch einen seitlichen Abstand zur Wirklinie der Gewichtskraft. Es entsteht ein Kräftepaar, der seitliche Abstand zwischen den Wirklinien ist der Hebelarm. Es entsteht ein aufrichtendes Moment, welches gleich dem Produkt aus Gewichtskraft und Hebelarm ist.

# Nummer 41 (Abbildung fehlt):

`Erklären Sie mit Hilfe eines Vektorparallelogramms aus "wahrem Wind (wW)", "Fahrtwind (Fw)" und "scheinbarem Wind (sW)", warum beim Einfallen einer Bö (Windzunahme) der "scheinbare Wind" raumt. Welcher Vorteil ergibt sich dadurch beim Kreuzen?(Zeichnung!)`

![](https://www.elwis.de/DE/Sportschifffahrt/Sportbootfuehrerscheine/Fragenkatalog-SKS/Seemannschaft-I/Zeichnung-41-vor-Einfall-der-Boe-gif.gif?__blob=normal&v=1)

Verhältnisse vor Einfall der Bö

![](https://www.elwis.de/DE/Sportschifffahrt/Sportbootfuehrerscheine/Fragenkatalog-SKS/Seemannschaft-I/Zeichnung-41-bei-Einfall-der-Boe-gif.gif?__blob=normal&v=1)

Verhältnisse bei Einfall der Bö

Der "scheinbare Wind" fällt etwas achterlicher ein, er "raumt", und das Segelboot kann etwas höher an den "wahren Wind" gehen.

# Nummer 42:

`Auf Amwindkurs mussten Sie reffen. Ein entgegenkommendes Boot gleicher Größe segelt ungerefft. Wie erklären Sie das?`

Der "scheinbare Wind" ist unterschiedlich stark. Er ist auf Amwindkursen stärker und auf Raumschot- oder Vorwindkursen schwächer als der "wahre Wind".

# Nummer 43:

`Warum wird die Versetzung einer Yacht durch den Wind mit zunehmender Krängung größer?`

Der Lateralplan wird kleiner, dadurch verringert sich der seitliche Widerstand des Schiffes im Wasser.

# Nummer 44:

`Welchen Einfluss hat zunehmende Krängung auf den Trimm eines Segelbootes? (Begründung!)`

Die Luvgierigkeit nimmt zu, weil der Segeldruckpunkt nach Lee auswandert.

# Nummer 45:

`Sie segeln am Wind, Krängung und Ruderdruck nehmen stark zu. 1. Wie bezeichnet man das Verhalten des Bootes? 2. Mit welchen Mitteln können Sie die Krängung und den Ruderdruck vermindern?`

Luvgierigkeit.

Traveller nach Lee setzen und/oder Großschot (etwas) fieren, Großsegel reffen.

# Nummer 46:

`Welche Funktion hat ein Unterliekstrecker und wie kann mit dem Unterliekstrecker der Trimm des Großsegels beeinflusst werden?`

Er reguliert die Spannung der Unterlieks. Je nach Zugkraft wird der untere Teil des Großsegels flacher oder bauchiger.

# Nummer 47:

`Das Vorliek der Fock wirft Falten. Welches ist die häufigste Ursache?`

Das Fall ist nicht genügend durchgesetzt.

# Nummer 48:

`Das Unterliek Ihres Vorsegels killt. Was ist die Ursache?`

Der Holepunkt der Schot liegt zu weit vorne.

# Nummer 49:

`Das Achterliek Ihres Vorsegels killt. Was ist die Ursache?`

Der Holepunkt der Schot liegt zu weit achtern.

# Nummer 50:

`Wozu dienen Windfäden am Segel?`

Sie machen den Strömungsverlauf am Segel sichtbar, um das Segel optimal trimmen zu können.

# Nummer 51:

`1. Wann sollte ein Großsegel flach getrimmt sein? 2. Wie kann ein Großsegel flach getrimmt werden?`

Bei Starkwind.

Dichtholen von Unterliek- und Vorliekstrecker, Dichtholen der Großschot, Durchsetzen des Großfalls, Spannung des Achterstags erhöhen, Traveller nach Lee.

# Nummer 52:

`Mit welchen 6 Teilen des laufenden und stehenden Guts können Sie ein Großsegel trimmen?`

Großschot, Traveller, Unterliekstrecker, Cunninghamstrecker, Großfall, Achterstagspanner.

# Nummer 53:

`Wozu dienen "Opferanoden" und wann sollten sie ausgewechselt werden?`

Sie dienen dem Schutz gegen Schäden durch Elektrolyse. Verbrauchte Anoden müssen nach einer Saison ersetzt werden, nicht erst wenn sie verbraucht sind.

# Nummer 54:

`Wie schützt man auf Kunststoffyachten den Propeller gegen Elektrolyse?`

Durch eine Zinkanode auf der Propellerwelle.

# Nummer 55:

`Welche Sicherheitsmaßnahmen sind vor und beim Tanken von Diesel zu treffen?`

Maschine abstellen,

offenes Feuer löschen (Rauchen einstellen),

Maßnahmen gegen Überlaufen treffen.

# Nummer 56:

`Wie können Sie beim Tanken Umweltverschmutzungen vermeiden?`

Tanköffnung mit Ölbindetüchern umlegen,

möglichst an Zapfsäulen mit Zapfhahn tanken,

beim Tanken aus Kanistern großen Trichter mit Schlauch benutzen,

Nachfüllen aus Kanistern bei Wind und bewegter See möglichst vermeiden.

# Nummer 57:

`Wozu dient das Wendegetriebe eines Motors?`

Zum Ein- und Auskuppeln des Propellers,

zum Umsteuern des Propellers auf Rückwärtsfahrt,

zur Drehzahluntersetzung.

# Nummer 58:

`Welche Maßnahmen sind vor dem Anlassen eines eingebauten Motors zu treffen?`

Hauptstromschalter einschalten,

Kraftstoff- und Kühlwasserventile öffnen,

Getriebe auf "neutral" stellen.

# Nummer 59:

`Was sollte nach dem Anlassen der Maschine kontrolliert werden?`

Kühlwasserdurchlauf,

Öldruck und Ladung,

Motorengeräusche und

Auspuffgase.

# Nummer 60:

`Was können erste Störungsanzeichen im Motorbetrieb sein?`

Ungewöhnliche und fremde Motorengeräusche, Vibrationen, Verfärbung der Abgase, Aufleuchten der Ladekontrolle bzw. Öldruckkontrolle und die entsprechenden akustischen Warnungen.

# Nummer 61:

`Wie können Sie einen Dieselmotor abstellen, wenn die vorgesehene Abstellvorrichtung defekt ist?`

Kraftstoffzufuhr unterbrechen.

Verschließen des Luftansaugrohres/der Luftansaugrohre.

# Nummer 62:

`Der Dieselmotor Ihres Bootes startet nicht. Welche Fehler, die Sie selber überprüfen können, könnten die Ursache sein?`

Anlasserdrehzahl zu gering (Batterie zu schwach),

kein Kraftstoff im Tank,

Luft in der Kraftstoffleitung,

falsche Bedienung der Kaltstarthilfe (eventuell Vorglühen zu kurz),

Anlasser defekt.

# Nummer 63:

`Der Motor Ihres Bootes bleibt beim Einkuppeln stehen. Nennen Sie mögliche Ursachen.`

Propellerwelle durch Tauwerk o. Ä. blockiert,

Schwerlauf des Getriebes wegen defekter Zahnräder, Lagerschaden, dicken Öls oder

verbogene Propellerwelle.

# Nummer 64:

`Während Sie unter Maschine laufen, steigt plötzlich die Kühlwassertemperatur stark an. Ihre Yacht ist mit einem Saildrive-Antrieb ausgestattet. 1. Welche typische Ursache hat der Temperaturanstieg, wenn eine technische Störung unwahrscheinlich ist? 2. Wie können Sie die Störung einfach beheben?`

Fremdkörper (Folienstücke, Plastiktüten, Pflanzenteile o. Ä.) haben den Kühlwassereinlass verstopft.

Mehrmals abwechselnd vor- und zurückfahren, sodass sich die Fremdkörper vom Kühlwassereinlass lösen.

# Nummer 65:

`Welche Propeller werden auf Yachten mit Einbaumotor eingesetzt?`

Festflügelpropeller, Faltpropeller, Drehflügelpropeller und Verstellpropeller.

# Nummer 66:

`Was müssen Sie beim Aufstoppen unter Maschine mit einem Faltpropeller beachten?`

Der Propeller entfaltet sich eventuell erst bei relativ hoher Drehzahl und der Wirkungsgrad ist geringer als beim Festflügelpropeller.

# Nummer 67:

`Mit welchen 4 Angaben werden Propeller auf Yachten beschrieben?`

Anzahl der Flügel, Größe ihrer Fläche, Durchmesser und Steigung.

# Nummer 68:

`Was sollten Sie beachten, wenn Sie den kleinen Außenborder mit eingebautem Tank Ihres Beibootes an Bord verstauen?`
(Begründung!)

Tank und Vergaser müssen leer sein.

Lagerung an Deck oder in einer Backskiste mit Außenentlüftung, niemals unter Deck.

Restbenzin und entweichende Benzingase bilden mit Luft ein leicht entzündliches Gemisch.

# Nummer 69:

`Wozu dient ein Wasserabscheider in der Kraftstoffleitung?`

In ihm sammelt sich das Kondenswasser aus dem Tank; dadurch werden Startschwierigkeiten vermieden.

# Nummer 70:

`Warum sollten Sie bei seltener Motorbenutzung den eingebauten Tank eines Dieselmotors möglichst voll getankt halten?`

Um Kondenswasserbildung zu verringern, was zu Startschwierigkeiten führen kann.

# Nummer 71:

`Welche Motor-Ersatzteile bzw. Schmierstoffe sollten Sie mindestens an Bord haben?`

Impeller für die Wasserpumpe,

Reservekeilriemen,

Motorenöl,

Dichtungsmaterial.

# Nummer 72:

`1. Was bedeutet die Angabe einer Batteriekapazität "2 x 60 Ah"? (Begründung!) 2. Welche Nettokapazität steht in dem Fall zur Verfügung? (Begründung!)`

Es handelt sich um 2 Batterien (Akkus) mit jeweils 60 Amperestunden, insgesamt also 120 Ah Nennkapazität.

Dem entspricht eine Nettokapazität von etwa 72 Ah, da ein Akku kaum über 80 % seiner Nennkapazität geladen werden kann.

# Nummer 73:

`Geben Sie die benötigte Strommenge (in Amperestunden) an, um bei einer 12-Volt-Anlage zwei Verbraucher mit je 24 Watt 10 Stunden betreiben zu können (mit Angabe der Berechnung)!`

Benötigte Strommenge je Verbraucher:
24 : 12 = 2 Ampere mal Anzahl der Verbraucher mal Stunden ergibt:
2 x 2 A x 10 h = 40 Ah.

# Nummer 74:

`Wie muss Tauwerk beschaffen sein, das für Festmacherleinen, Anker- und Schlepptrossen verwendet wird?`

Es muss bruchfest und elastisch sein.

# Nummer 75:

`Wodurch können Sie verhindern, dass Festmacherleinen durch Schamfilen in Klüsen oder an Kanten an der Pier beschädigt werden?`

Durch einen gegen Verrutschen gesicherten Plastikschlauch, der über den Festmacher an der Scheuerstelle gezogen wird, hilfsweise mit Tuchstreifen.

# Nummer 76:

`Was müssen Sie hinsichtlich der Festigkeit bedenken, wenn Sie Leinen zusammenknoten?`

Beim Knoten können Festigkeitsverluste bis zu 50 % auftreten.

# Nummer 77:

`Wodurch können Sie verhindern, dass bei Tauwerk aus unterschiedlichem Innen- und Außenmaterial die Seele in den Mantel rutscht?`

Durch einen genähten Takling.

# Nummer 78 (Abbildung fehlt):

`Wie sind längsseits liegende Fahrzeuge festzumachen? Ergänzen Sie die Skizze und benennen Sie die Leinen.`

![](https://www.elwis.de/DE/Sportschifffahrt/Sportbootfuehrerscheine/Fragenkatalog-SKS/Seemannschaft-I/Zeichnung-78-01-gif.gif?__blob=normal&v=1)

Achterleine,
Achterspring,
Vorspring,
Vorleine.

![](https://www.elwis.de/DE/Sportschifffahrt/Sportbootfuehrerscheine/Fragenkatalog-SKS/Seemannschaft-I/Zeichnung-78-02-gif.gif?__blob=normal&v=1)

# Nummer 79 (Abbildung fehlt):

`Wie können Sie mit Hilfe von zwei Fendern und einem Fenderbrett Ihr Boot festmachen, wenn die Pier mit vorspringenden Pfählen versehen ist? Ergänzen Sie die Skizze mit Leinen.`

![](https://www.elwis.de/DE/Sportschifffahrt/Sportbootfuehrerscheine/Fragenkatalog-SKS/Seemannschaft-I/Zeichnung-79-01-gif.gif?__blob=normal&v=1)

![](https://www.elwis.de/DE/Sportschifffahrt/Sportbootfuehrerscheine/Fragenkatalog-SKS/Seemannschaft-I/Zeichnung-79-02-gif.gif?__blob=normal&v=1)

# Nummer 80:

`Was ist an Land beim Reinigen eines mit Antifouling behandelten Unterwasserschiffes zu beachten?`

Umweltschutzbestimmungen beachten, d. h., das Schiff nur auf einem entsprechend ausgerüsteten Reinigungsplatz abspritzen und Wasser und Schmutz auffangen, also nicht in die Kanalisation leiten.

# Nummer 81:

`Ein funktionsfähiges elektrisches Gerät arbeitet an Bord nicht. Nennen Sie häufige Ursachen und was kann zur Behebung getan werden?`

Schlechte Kontakte und Korrosion.

Kontakte fest anziehen, korrodierte Stellen mit feinstem Schleifpapier säubern, Kontaktspray verwenden.

# Nummer 82:

`Warum müssen Schäden im Gelcoat unverzüglich beseitigt werden?`

Das Laminat unter der Gelcoatschicht nimmt sonst Wasser auf und wird dadurch geschädigt.

# Nummer 83:

`Welche Daten sollten mindestens an Bord im Logbuch dokumentiert werden?`

Namen und Funktionen der Crewmitglieder,

Beginn und Ende einer Fahrt und

in angemessenen Zeitabständen: Position, Kurs, Geschwindigkeit, Strömung, Wetter, Luftdruck.

# Nummer 84:

`Worauf müssen Sie vor dem Setzen des Großsegels achten? Welche Gefahr besteht nach dem Setzen des Großsegels, solange der Wind von vorne kommt?`

Großschot und Baumniederholer müssen ausreichend Lose haben.

Das Großfall muss frei laufen und darf nicht vertörnt sein.

Verletzungsgefahr durch schlagenden Großbaum.

# Nummer 85:

`Was tun Sie, wenn Ihr Großsegel unter der untersten Lattentasche einreißt?`

Untere Latte entfernen.

Segel bis über den Riss reffen.

# Nummer 86:

`Beschreiben Sie die Schritte für den Reffvorgang mit dem Bindereff.`

Sicherstellen, dass Dirk angeschlagen oder Baum durch stützenden Baumniederholer in der Höhe gehalten wird,

Fall fieren und Segel etwas herunterholen,

Segelhals in Reffhaken am Lümmelbeschlag einhaken und festsetzen,

Fall wieder dichtholen,

Segelschothorn (hintere Reffkausch) mit Schmeerreep oder Reffleine nach achtern auf den Baum holen und

eventuell loses Segeltuch auftuchen und mit Reffbändseln/Reffleine einbinden.

# Nummer 87:

`1. Wozu dient ein "Bullenstander"? 2. Wie wird er gefahren?`

Er soll das ungewollte Überkommen des Großbaums bei achterlichen Winden verhindern.

Von der Baumnock zum Vorschiff.

# Nummer 88:

`Warum muss beim Segeln vor dem Wind oder mit raumem Wind der Baumniederholer entsprechend der Windstärke durchgesetzt werden?`

Um das Steigen des Baumes zu verhindern.

# Nummer 89:

`Wenn gleich große Boote im Päckchen oder in der Box zusammenliegen, kann es zu Berührungen und Schäden in der Takelage kommen. Wie ist das zu verhindern?`

Boote versetzt legen, damit Masten nicht auf gleicher Höhe sind oder im Wechsel Heck-Bug zur Pier legen.

# Nummer 90:

`Worauf ist beim Liegen in einer Box in Bezug auf benachbarte Boote zu achten, wenn Schwell in den Hafen läuft?`

Dass benachbarte Boote mit ihren Masten versetzt liegen und nicht gegeneinander schlagen.

# Nummer 91:

`Sie sind mit Ihrer Segelyacht auf See. Was veranlassen Sie bei einem Gewitteraufzug?`

Vorsegel rechtzeitig verkleinern,

Großsegel klar zum Reffen oder Bergen,

Schlechtwetterkleidung, Sicherheitsgurte und Rettungswesten anlegen,

Position in Karte eintragen.

# Nummer 92:

`Sie übernehmen in einem Hafen eine Ihnen unbekannte Yacht. Wie machen Sie sich zu Reisebeginn mit den Segeleigenschaften vertraut?`

Ich fahre diverse Manöver - Wende, Halse, Q-Wende, verschiedene Rettungsmanöver - mit unterschiedlicher Geschwindigkeit und Besegelung.

# Nummer 93:

`Wie verhalten Sie sich nach einem Mastbruch, was müssen Sie veranlassen?`

Nach Möglichkeit den Mast an Bord nehmen und sichern.

Falls nicht möglich, Mast und Wanten kappen, um Rumpfschäden zu vermeiden.

# Nummer 94:

`Von welchen Faktoren ist die Länge eines Nahezu-Aufschießers zu einer im Wasser treibenden Person abhängig?`

Geschwindigkeit, Wind, Seegang, Strömung und Form und Gewicht des Bootes.

# Nummer 95:

`Sie wollen in eine Box einlaufen. Wie bereiten Sie die Achterleinen vor und machen sie fest?`

Achterleinen mit Auge versehen (z. B. Palstek), möglichst früh über die Pfähle legen, bei seitlichem Wind zuerst über den Luvpfahl.

# Nummer 96:

`Welche Vorbereitung haben Sie für ein Anlegemanöver zu treffen?`

Crew für Manöver einteilen.

Leinen und Fender bereitlegen.

# Nummer 97:

`Welchen Nachteil hat ein "Saildrive-Antrieb" insbesondere bei Hafenmanövern?`

Durch den großen Abstand zwischen Propeller und Ruder wird dieses nicht direkt angeströmt. Das kann die Manövrierfähigkeit beim Anfahren etwas verschlechtern.

# Nummer 98:

`Was ist ein Bugstrahlruder und wozu dient es?`

Eine im Bug einer Yacht befindliche Röhre mit einem Propeller, mit dem ein Querschub und damit ein Drehen des Buges bei geringen Vorausgeschwindigkeiten erreicht werden kann.

# Nummer 99:

`Bei welchen Manövern können Sie ein Bugstrahlruder sinnvoll einsetzen?`

Beim An- und Ablegen.

Beim Drehen auf engem Raum.

# Nummer 100:

`Sie liegen längsseits mit der Steuerbordseite an einer Pier. Beschreiben Sie ein Ablegemanöver unter gleichzeitigem Einsatz von Bugstrahlruder und Maschine.`

Hebel für Bugstrahlruder nach Backbord legen, sodass der Bug von der Pier weggedrückt wird (nach Backbord schwenkt) und gleichzeitig

Ruderlage deutlich nach Steuerbord und langsame Fahrt voraus, sodass das Heck nach Backbord ausschwenkt.
So wird das Schiff fast parallel von der Pier abgedrückt.

# Nummer 101:

`Wie können Sie im freien Seeraum auf einer Segelyacht einen Sturm abwettern?`

Durch Beiliegen

Lenzen vor Topp und Takel, dabei Leinen achteraus schleppen

Liegen vor Treibanker oder

unter Sturmbesegelung aktiv segelnd und nach Möglichkeit brechende Seen aussteuernd.

# Nummer 102:

`Warum kann das Anlaufen eines Hafens bei auflandigem Starkwind bzw. schwerem Wetter gefährlich werden?`

Gefahr durch Grundseen bzw. Kreuzseen. Möglichkeit von Querstrom.

# Nummer 103:

`Warum kann eine Leeküste bei schwerem Wetter einer Segelyacht gefährlich werden?`

Wenn die Yacht sich nicht freikreuzen kann, droht Strandung.

# Nummer 104 (Abbildung fehlt):

`Mit welchem Manöver können Sie bei Starkwind das Halsen vermeiden (Name)? Vervollständigen Sie die Skizze durch Einzeichnen der Kurslinie und geben Sie die erforderlichen Manöver an.`

![](https://www.elwis.de/DE/Sportschifffahrt/Sportbootfuehrerscheine/Fragenkatalog-SKS/Seemannschaft-I/Zeichnung-104-01-gif.gif?__blob=normal&v=1)

![](https://www.elwis.de/DE/Sportschifffahrt/Sportbootfuehrerscheine/Fragenkatalog-SKS/Seemannschaft-I/Zeichnung-104-02-gif.gif?__blob=normal&v=1)

# Nummer 105:

`Was erreicht man mit dem Ausbringen eines Treibankers bei schwerer See?`

Man hält bei richtiger Leinenlänge den Bug im Wind und verringert die Driftgeschwindigkeit.

# Nummer 106:

`Worauf müssen Sie achten, wenn Sie in Tidengewässern längsseits einer Pier festgemacht haben?`

Die Wassertiefe muss auch bei Niedrigwasser ausreichen oder sicheres Aufsetzen muss gewährleistet sein.

Die Leinen müssen für den Tidenstieg oder -fall ausreichend lang sein. Bei größerem Tidenhub darf das Fahrzeug keinesfalls unbeaufsichtigt bleiben.

# Nummer 107:

`Sie kreuzen bei frischem Wind und mitlaufendem Strom (Wind gegen Strom) nach Luv auf. Welche Auswirkungen hat ein gegen den Wind setzender Strom auf den Seegang?`

Durch den Strom entsteht eine kurze, steile und kabbelige See.

# Nummer 108:

`Wie wirkt sich mitlaufender Strom auf die Fahrt eines Fahrzeugs und die Loganzeige aus?`

Der Strom erhöht die Fahrt über Grund.

Das Log zeigt dies nicht an.

# Nummer 109:

`Welche Hilfsmittel können Sie einsetzen, um einen Überbordgefallenen an Deck zu bekommen?`

Bewegliche (und gesicherte) Badeleiter, eventuell Großschot, beschwerte Trittschlinge, Rettungstalje, Unterfangen mit kleinem Segel, Bergegurt, Dingi.

# Nummer 110:

`Welche Sofortmaßnahmen sind einzuleiten, wenn jemand über Bord gefallen ist?`

Ruf "Mensch über Bord!"

Rettungsmittel zuwerfen,

Ausguck halten, Mensch im Auge behalten,

Maschine starten,

"Mensch-über-Bord-Manöver" einleiten,

Notmeldung abgeben,

ggf. Markierungsblitzboje werfen,

ggf. MOB-Taste eines satellitengestützten Navigationsgerätes drücken,

Bergung durchführen.

# Nummer 111:

`Welche Maßnahmen können gegen das Überbordfallen getroffen werden?`

Sicherheitsgurte anlegen und einpicken.

Anbringen von "Streck-" oder Laufleinen vom Bug zum Heck.

Crew auf Befestigungspunkte (Einpickpunkte für Karabinerhaken) hinweisen.

# Nummer 112:

`Nennen Sie die grundsätzlichen Schritte und ihre Ziele zur Rettung einer über Bord gegangenen Person.`

Maschine starten,

Suche, Herstellung eines Sichtkontaktes zur über Bord gegangenen Person,

"Mensch-über-Bord-Manöver", Annäherung an die im Wasser treibende Person und Herstellung einer ersten Leinenverbindung,

Bergung, sicheres und schnelles Anbordnehmen der Person,

Erste Hilfe, Betreuung,

ggf. Notalarm abgeben.

# Nummer 113:

`Mit welchen Hilfsmitteln können Sie den Bezugspunkt (internationaler Begriff: Datum) für die Suche nach einem Überbordgefallenen sichern?`

Markierungsblitzboje,

MOB-Taste eines satellitengestützten Navigationsgerätes (z. B. GPS).

# Nummer 114:

`Was gehört u. a. zur Sicherheitsausrüstung z. B. einer 10-m-Yacht? Nennen Sie mindestens 6 Ausrüstungsgegenstände.`

Lenzpumpen und Pützen,

Lecksicherungsmaterial,

Feuerlöscher,

Werkzeuge und Ersatzteile,

Seenotsignalmittel,

Handlampen,

Funkeinrichtung,

Anker,

Erste-Hilfe-Ausrüstung,

Radarreflektor und

Rettungsmittel.

# Nummer 115:

`Was gehört zur Sicherheitsausrüstung der Besatzung in der Küstenfahrt?`

Rettungsweste und Sicherheitsgurt (Lifebelt) für jedes Besatzungsmitglied,

Rettungsfloß (Rettungsinsel),

Rettungskragen mit Tag- und Nachtsignal und

Erste-Hilfe-Ausrüstung mit Anleitung.

# Nummer 116:

`Wie erhalten Sie Kenntnis über das nächste Wartungsdatum eines Rettungsfloßes?`

Die runde, auf der Insel klebende farbige Serviceplakette oder das bei der letzten Wartung mitgelieferte Zertifikat geben Auskunft über den nächsten Wartungstermin.

# Nummer 117:

`Worauf müssen Sie bei Ihren Automatikrettungswesten hinsichtlich der Funktionssicherheit achten?`

Auf regelmäßige Wartung. Wartungsfälligkeit erkennbar an der farbigen Serviceplakette.

# Nummer 118:

`Was ist auf Deck einer Yacht ein Strecktau (auch Laufleine genannt) und wozu dient es?`

Ein neben der Fußreling verlaufender Draht, Gurt oder eine starke Leine zwischen Cockpit und Vorschiff straff gespannt zum Einpicken der Sicherheitsleine (Lifebelt).

# Nummer 119:

`Welche Seenotsignalmittel sollten Sie an Bord haben? Nennen Sie mindestens 6 Beispiele.`

Handfackeln, rot,

Handraketen, rot,

Rauchfackeln oder Rauchtopf, orange,

Signalpistole mit Munition,

Seewasserfärber,

Signalflaggen N und C,

Signallampe,

Seenotfunkboje.

# Nummer 120:

`Welche Feuerlöscheinrichtungen sollten an Bord vorhanden sein?`

Feuerlöscher (ABC-Pulverlöscher und eventuell CO2-Löscher),

Pütz zum Löschen von Bränden fester Stoffe,

Feuerlöschdecke,

Löschdurchlass für geschlossene Motorräume, der das Löschen von Bränden mit CO2-Löschern ohne Sauerstoffzutritt ermöglicht.

# Nummer 121:

`Welche Feuerlöscharten sind für Sportboote geeignet? Wie und wo sind sie an Bord unterzubringen?`

Der ABC-Pulverlöscher, für geschlossene Motorräume der CO2-Löscher,

Der Feuerlöscher muss gebrauchsfertig und leicht erreichbar sein, CO2-Löscher nicht im Schiffsinneren unterbringen (Erstickungsgefahr bei Leckage).

Er soll in der Nähe der Maschinenräume, der Kombüse bzw. der Koch- oder Heizstelle montiert sein.

# Nummer 122:

`Wie wird die ständige Funktionssicherheit eines Feuerlöschers sichergestellt?`

Durch Einhaltung des vorgeschriebenen Prüftermins, ersichtlich aus der Prüfplakette.

Der Feuerlöscher muss vor Feuchtigkeit und Korrosion geschützt werden.

# Nummer 123:

`Wie wird ein Brand an Bord wirksam bekämpft?`

Alle Öffnungen schließen,

Brennstoffzufuhr (Hauptschalter) unterbrechen,

Feuerlöscher erst am Brandherd betätigen,

Feuer von unten und von vorn bekämpfen,

Löschdecke einsetzen,

Flüssigkeitsbrände nicht mit Wasser bekämpfen.

# Nummer 124:

`Was ist vor Reisebeginn beim Seeklarmachen zu überprüfen und zu beachten? Nennen Sie mindestens 6 Beispiele.`

Seetüchtigkeit der Yacht,

Zahl und Zustand der Segel,

Treibstoffvorrat,

Navigationsunterlagen,

Sicherheitseinweisung der Besatzung,

Rettungsmittel,

Seenotsignale,

Trinkwasser- und Proviantvorräte,

Funktionsfähigkeit des Motors,

Funktionsfähigkeit der elektronischen Navigationsgeräte,

Lenzeinrichtungen,

Feuerlöscher,

Boots- und Personalpapiere,

Betriebsfähigkeit der UKW-Seefunkanlage.

# Nummer 125:

`Was gehört zur Sicherheitseinweisung der gesamten Besatzung vor Reisebeginn? Nennen Sie mindestens 6 Beispiele.`

Einweisung in Gebrauch und Bedienung

der Rettungswesten und Sicherheitsgurte,

des Rettungsfloßes,

der Signalmittel,

der Lenzpumpen,

der Seeventile und des Bord-WC,

der Kocheinrichtung,

der Feuerlöscher,

der Motoranlage,

der Elektroanlage,

des Rundfunkgerätes und der UKW-Seefunkanlage,

Verhalten bei "Mensch-über-Bord",

Erkennen und Verhalten bei Seekrankheit.

# Nummer 126:

`In welche technischen Einrichtungen/Ausrüstungen muss der Schiffsführer die Besatzung vor Reiseantritt unbedingt einweisen? Nennen Sie mindestens 6 Beispiele.`

Ankergeschirr,

Lenzeinrichtung,

Feuerlöscheinrichtungen,

Motoranlage,

Seeventile,

UKW-Seefunkanlage,

MOB-Taste von satellitengestützten Navigationsgerät (z. B. GPS),

Seenotsignalmittel,

Notrudereinrichtung.

# Nummer 127:

`Welche Sicherheitsmaßnahmen sind vor jedem Auslaufen durchzuführen? Nennen Sie mindestens 6 Beispiele.`

Wetterbericht einholen,

Kontrolle der Sicherheitseinrichtung,

Kontrolle von Motor und Schaltung,

Kontrolle der nautischen Geräte,

Kontrolle der Bilge,

Überprüfen des Wasser- und Kraftstoffvorrats,

Kontrolle der Schall- und Lichtsignaleinrichtung,

Kontrolle der Navigationslichter,

Bereitlegen der aktuellen Seekarten und nautischen Veröffentlichungen.

# Nummer 128:

`Warum sollten alle Crewmitglieder Lage und Funktion sämtlicher Pumpen und Ventile kennen?`

Damit im Bedarfsfall sie jeder bedienen kann.

# Nummer 129:

`Warum sollte die Crew in die Funktion des Bord-WC eingewiesen werden?`

Weil durch unsachgemäße Bedienung Wasser ins Bootsinnere gelangen kann.

# Nummer 130:

`Warum sollte die Crew vor Reisebeginn in die Funktion des Ankergeschirrs und die Durchführung eines Ankermanövers eingewiesen werden?`

Damit jeder den Anker sicher ausbringen und einholen kann.

# Nummer 131:

`Wie verhalten Sie sich, wenn Ihr Schiff leckgeschlagen ist?`

Meldung abgeben.

Je nach Erfordernissen Fahrt aus dem Schiff nehmen.

Lenzpumpen betätigen, Lecksuche, Leck mit Bordmitteln abdichten.

Küste bzw. flaches Wasser ansteuern.

Fahrzeug so trimmen, dass Leckstelle aus dem Wasser kommt bzw. möglichst wenig unter Wasser ist.

# Nummer 132:

`Was tun Sie, wenn Ihr Schiff leckgeschlagen ist und das Wasser im Schiff trotz aller Maßnahmen weiter steigt?`

Notzeichen geben, Funkmeldung abgeben, ggf. Radartransponder einschalten.

Verlassen des Bootes vorbereiten, Rettungswesten anlegen, Rettungsfloß klarmachen.

Wenn möglich, ruhiges Flachwasser anlaufen und Schiff auf Grund setzen.

# Nummer 133:

`Welche Folgen können Grundberührungen und harte Stöße, z. B. bei Anlegemanövern oder Kollisionen mit treibenden Gegenständen haben?`

Eine Beschädigung der Bordwand kann eintreten.

Es kann Sinkgefahr entstehen.

# Nummer 134:

`Welche grundsätzliche Verhaltensweise sollte beachtet und welche Maßnahmen sollten ergriffen werden, wenn Ihr Schiff in Seenot kommt?`

Ruhe bewahren und überlegt handeln.

Notalarm abgeben, ggf. Radartransponder einschalten.

Rettungsfloß klarmachen.

Rettungsweste und Sicherheitsgurt anlegen.

So lange wie möglich an Bord bleiben.

Wärmende Kleidung anziehen.

# Nummer 135:

`Welche Maßnahmen treffen Sie, bevor Sie von Ihrem Fahrzeug in ein Rettungsfloß übersteigen?`

Rettungsweste und Sicherheitsgurt anlegen.

Wärmende Kleidung anziehen.

Nach Möglichkeit vorher reichlich warme Flüssigkeit trinken.

Soweit noch nicht geschehen, Proviant, Wasser, Seenotsignalmittel und ggf. Seenotfunkbake, Radartransponder und UKW-Handsprechfunkgeräte in das Rettungsfloß bringen.

# Nummer 136:

`Warum sollte ein sinkendes Schiff im Notfall so spät wie möglich verlassen werden?`

Die Überlebensmöglichkeiten sind auf dem Schiff größer.

Ein Schiff ist besser zu orten.

Einstieg in das Rettungsfloß und Aufenthalt können sehr schwierig sein.

# Nummer 137:

`Erklären Sie die Handhabung der Hubschrauberrettungsschlinge im Einsatz!`

Bei offener Rettungsschlinge: zuerst den Karabinerhaken einpicken.

Mit dem Kopf und beiden Armen in die Rettungsschlinge einsteigen.

Die Arme müssen nach unten gedrückt werden und die Hände sind zu schließen.

Das Windenseil muss frei hängen, es darf nicht an Bord befestigt werden.

# Nummer 138:

`Wann dürfen Notzeichen gegeben werden?`

Nach Feststellung des Notfalls auf Anordnung des Schiffsführers; bei unmittelbarer Gefahr für das Schiff oder die Besatzung, die ohne fremde Hilfe nicht überwunden werden kann.

# Nummer 139:

`Wann darf ein UKW-Sprechfunkgerät auch ohne entsprechenden Befähigungsnachweis benutzt werden?`

In Notfällen.

# Nummer 140:

`Worauf ist zu achten, wenn Crewmitglieder seekrank sind?`

Aufenthalt im Cockpit beaufsichtigen und Crewmitglieder gegen Überbordfallen sichern,

Flüssigkeitsverlust ausgleichen (Wasser),

Crewmitglied anhalten, zur Küste oder zum Horizont zu schauen,

mit Arbeiten beschäftigen.

# Nummer 141:

`Wozu dient ein Reitgewicht (Gleitgewicht, Ankergewicht) beim Ankern?`

Es soll die Ankertrosse auf den Grund ziehen, damit der Anker nicht durch einen zu steilen Winkel aus dem Grund gebrochen wird. Es wirkt ruckdämpfend.

# Nummer 142:

`Warum sollte beim Verwenden einer Ankertrosse ein Kettenvorlauf benutzt werden?`

Damit der Zug auf den Anker nicht zu steil wird.

# Nummer 143:

`Welcher Ankergrund ist für die üblichen Leichtgewichtsanker`
1. gut geeignet?
2. mäßig geeignet?
3. ungeeignet?

Sand, Schlick, weicher Ton und Lehm,

harter Ton und Lehm,

steinige, verkrautete und stark schlammige Böden.

# Nummer 144:

`Was müssen Sie bei der Auswahl eines Ankerplatzes beachten?`

Der Ankerplatz sollte Schutz vor Wind und Wellen bieten.

Auf ausreichenden Platz zum Schwojen achten.

Mögliche Winddrehungen einplanen.

# Nummer 145:

`Welchen Ankergrund sollten Sie nach Möglichkeit meiden?`

Steinige, verkrautete und stark schlammige Böden.

# Nummer 146:

`Wie können Sie die Haltekraft eines Ankers erhöhen, wenn Sie auf engem Raum (z. B. zwischen zwei Stegen) nicht die erforderliche Kettenlänge stecken können?`

Mit einem Reitgewicht, um so den Anker besser am Boden zu halten.

# Nummer 147:

`Sie ankern in einer Bucht. Wie können Sie bei zunehmendem Wind die Haltekraft Ihres Ankers verbessern?`

Mehr Trosse oder Kette stecken,

Reitgewicht verwenden.

# Nummer 148:

`Sie wollen auf verkrautetem Grund ankern. Ihnen steht ein Leichtgewichtsanker und ein Stockanker zur Verfügung. Welchen benutzen Sie und warum?`

Den Stockanker, weil er sich insbesondere auch aufgrund seines höheren Gewichtes besser eingräbt.

# Nummer 149:

`Wozu dient eine Ankerboje?`

Sie zeigt die Lage des Ankers an.

Mit der Trippleine kann das Bergen eines unklaren Ankers unterstützt werden.

# Nummer 150:

`Wie erkennen Sie, ob der Anker hält?`

Vibration von Kette oder Trosse prüfen,

Einrucken des Ankers prüfen,

durch wiederholte Peilungen und ggf. Schätzungen des Abstands zu anderen Schiffen oder zu Landmarken,

falls GPS vorhanden ist, die Ankeralarmfunktion einschalten.

# Nummer 151:

`Welche Ankerarten finden überwiegend auf Sportbooten Verwendung? Nennen Sie 3.`

Patentanker,

Stockanker (einklappbarer Stock),

Draggen (klappbare Flunken),

Pflugscharanker.

# Nummer 152:

`Nennen Sie 3 Ankertypen, die vom Germanischen Lloyd als Anker mit hoher Haltekraft anerkannt sind.`

Bruce-Anker, CQR-Anker, Danforth-Anker, D'Hone-Anker.

# Nummer 153:

`1. Welches sind die Vorteile einer Ankerkette gegenüber einer Ankerleine? 2. Wie kombiniert man auf Yachten häufig die Systeme?`

Die Kette unterstützt das Eingraben, verkleinert den Schwojeraum, wirkt ruckdämpfend, kann nicht an Steinen durchscheuern und erhöht die Haltekraft des Ankers.

Es wird zwischen Anker und Leine ein Kettenvorlauf von 3 bis 5 m gefahren.

# Nummer 154:

`1. Warum soll eine Ankerleine nicht an den Anker geknotet werden? 2. Warum muss die Ankerkette mit einem Taustropp am Schiff bzw. im Kettenkasten befestigt werden?`

Knoten reduzieren die Bruchlast einer Leine um bis zu 50 %.

Damit die Kette im Notfall schnell gekappt werden kann.

# Nummer 155:

`Sie wollen in einer Bucht ankern, in der das (ausreichend tiefe) Wasser unterschiedliche Färbungen zeigt. Wo wählen Sie den Akerplatz? (Begründung!)`

Ich ankere auf hellem Wasser.
Begründung: Der Grund ist hier sandig, der Anker hält gut. Dunkler Grund weist auf Bewuchs hin, wo der Anker schlecht hält.

# Nummer 156:

`Warum darf der Anker nicht zusammen mit seiner Leine am Ankerplatz über Bord geworfen werden?`

Die Leine könnte mit dem Anker vertörnen und dadurch das Eingraben des Ankers verhindern. Der Anker würde dann nicht halten.

# Nummer 157:

`Was müssen Sie bedenken, wenn ein großes Schiff auf Ihr Sportboot zukommt?`

Andere Manövrierfähigkeit (größere Drehkreise, längere Stoppstrecken),

u. U. eingeschränkte Sicht des anderen Fahrzeugs, insbesondere nach voraus,

Möglichkeit des Übersehenwerdens, weil man sich im Radarschatten befindet,

Beeinträchtigung durch Bugwellen des großen Schiffes,

mögliche Beeinträchtigung der Manövrierfähigkeit des eigenen Bootes durch Windabdeckung.

# Nummer 158:

`Warum sollten Sie nicht zu dicht hinter dem Heck eines vorbeifahrenden Schiffes durchfahren?`

Sog und Hecksee können das eigene Boot erheblich gefährden.

# Nummer 159:

`Was müssen Sie beim Passieren eines großen Schiffes bei dessen Kursänderungen, z. B. in einem kurvenreichen Fahrwasser, beachten?`

Bei einer Kursänderung schwenkt das Heck deutlich in die entgegengesetzte Richtung aus, also nach Backbord bei einer Kursänderung nach Steuerbord und umgekehrt.

# Nummer 160:

`Mit welchen Stoppstrecken und Stoppzeiten müssen Sie bei großen Schiffen in voller Fahrt rechnen und wovon hängen sie ab?`

Abhängig von Schiffstyp und -größe, Beladungszustand und Ausgangsgeschwindigkeit ist mit der 8- bis 12fachen Schiffslänge und bis zu 8 bis 12 Minuten Dauer (z. B. ein 300 m langes Containerschiff voll abgeladen mit 24 kn: Stoppstrecke ca. 2 sm, Stoppzeit ca. 12 Minuten) zu rechnen.

# Nummer 161:

`Wie reagiert ein großes Schiff, wenn bei ca. 20 kn Fahrt ein Ausweichmanöver durch Hartruderlage eingeleitet wird? Nach welcher Distanz verlässt es in etwa die alte Kurslinie?`

Der Steven bewegt sich in Richtung der Hartruderlage, das Heck schlägt relativ weit zur entgegengesetzten Richtung aus. Das Schiff verlässt mit seinem Heck erst nach mehreren Schiffslängen seine bisherige Kurslinie, bewegt sich also zunächst in der alten Kursrichtung fort.
Diese Strecke kann bei 300 m langen Containerschiffen 1,5 bis 2,5 Schiffslängen, d. h. ca. 500 bis 600 m betragen.

# Nummer 162:

`Auf vielen großen Schiffen ist die Sicht nach vorne eingeschränkt. Welchen Abstand vor einem Schiff müssen Sie als nicht einsehbar mindestens berücksichtigen?`

Sichtbeschränkung nach voraus maximal 2 Schiffslängen oder 500 m.

# Nummer 163:

`Wie können Sie die Wahrscheinlichkeit erhöhen, im Radar von anderen Fahrzeugen gesehen zu werden?`

Durch einen möglichst hoch und fest angebrachten passiven Radarreflektor bzw. besser noch durch einen "aktiven" Radarreflektor.