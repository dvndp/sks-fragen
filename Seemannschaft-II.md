# Nummer 1:

`Welche Bootsbau-Werkstoffe finden im Sportbootbau für den Rumpf überwiegend Verwendung?`

GFK = glasfaserverstärkter Kunststoff,

Stahl,

Aluminium,

wasserfest verleimtes Sperrholz,

Massivholz.

# Nummer 2:

`Was versteht man unter einem Gleiter (schnelle Schiffe) und welche Fahreigenschaften hat solch ein Motorboot?`

Schnelle Schiffe, deren Gewicht überwiegend von Antriebskomponenten getragen wird (flache und breite Konstruktion) und die auf glattem Wasser bei höherer Geschwindigkeit in Gleitfahrt kommen. Bei Seegang können die Boote hart aufschlagen, sodass die Konstruktionen stark beansprucht werden.

# Nummer 3:

`Was versteht man unter einem "Verdränger" und welche Fahreigenschaften hat solch ein Motorboot?`

Konventionelle Schiffe - unabhängig vom Tiefgang -, deren Gewicht ausschließlich vom hydrostatischen Auftrieb getragen wird und deren Geschwindigkeit dementsprechend beschränkt ist (Rumpfgeschwindigkeit).

# Nummer 4:

`Was versteht man unter einem "Halbgleiter" und welche Fahreigenschaften hat solch ein Motorboot?`

Halbgleiter sind Motorboote mit geringem Tiefgang, sehr hoher Formstabilität und schneller Fahrweise bei starker Motorisierung.

# Nummer 5:

`Was versteht man im Bootsbau unter Gelcoat?`

Die äußere Schutzschicht eines Bauteils aus glasfaserverstärktem Kunststoff.

# Nummer 6:

`Was versteht man beim GFK-Bootsbau unter Sandwichverfahren (GFK = glasfaserverstärkter Kunststoff)?`

Zwischen zwei GFK-Schichten wird eine Zwischenlage zur Versteifung einlaminiert, z. B. aus Balsaholz.

# Nummer 7:

`Welchen Vorteil hat die Sandwichbauweise gegenüber der Massivbauweise bei GFK-Yachten (GFK = glasfaserverstärkter Kunststoff)?`

Große Steifheit, Verwindungsfestigkeit, geringes Gewicht, gute Isolierung.

# Nummer 8:

`Beschreiben Sie die Vor- und Nachteile von Stahl als Baumaterial für Yachten.`

Vorteile:
zuverlässiges, problemloses Baumaterial mit sehr hoher Festigkeit und langer Lebensdauer.

Nachteile:
hohes Gewicht, Rostanfälligkeit.

# Nummer 9:

`Was bedeutet der Betriff "Kategorie" im Zusammenhang mit dem CE-Zeichen für Wassersportfahrzeuge?`

Mit der Kategorie legt der Hersteller fest, in welchem Fahrgebit, bis zu welcher Windstärke und bis zu welcher charakteristischen Wellenhöhe das Fahrzeug sicher betrieben werden kann.

# Nummer 10:

`Welche Kategorien können im Zusammenhang mit dem CE-Zeichen für Wassersportfahrzeuge vergeben werden?`

A Hochsee,

B Außerhalb von Küstengewässern,

C Küstennahe Gewässer,

D Geschützte Gewässer.

# Nummer 11:

`Was bedeutet die im Zusammenhang mit dem CE-Zeichen für Wassersportfahrzeuge angegebene Kategorie "B Außerhalb von Küstengewässern"?`

Das Fahrzeug ist ausgelegt für Fahrten außerhalb von Küstengewässern, in denen Windstärken bis einschließlich 8 Bft und signifikante Wellenhöhen bis einschließlich 4 m auftreten können.

# Nummer 12:

`Beschreiben Sie den Aufbau einer Radsteuerung mit Seilzügen.`

Das Rad dreht ein Zahnrad, über das eine Kette in der Steuersäule nach unten verläuft. Die Kette ist mit den Steuerseilen verbunden, welche über Umlenkrollen zum Ruderquadranten führen.

# Nummer 13:

`Wozu dient der Lenzkorb am Ansaugstutzen einer Lenzpumpe und wie erhalten Sie seine Funktionsfähigkeit?`

Der Lenzkorb verhindert Verunreinigungen und Verstopfungen der Lenzpumpe. Er muss regelmäßig überprüft und gereinigt werden.

# Nummer 14:

`Welche Lenzvorrichtungen und -möglichkeiten sollten auf jeder seegehenden Yacht vorhanden sein?`

Zwei voneinander unabhängige Bilgepumpen, von denen eine über Deck bedienbar ist, sowie 2 Pützen mit Leinen.

# Nummer 15:

`Warum sollten Sie mehr als einen Anker an Bord haben, möglichst unterschiedlicher Art?`

Als Ersatz bei Verlust,

zum Verwarpen oder Verkatten,

um unterschiedliche Ankergründe berücksichtigen zu können,

um bei schwerem Wetter oder in Tidengewässern vor 2 Ankern liegen zu können.

# Nummer 16:

`Wie viele Fender und Festmacherleinen sollten Sie mindestens an Bord haben?`

4 Festmacherleinen und 4 Fender.

# Nummer 17:

`Was sollte auf jeder Motoryacht außer Festmacherleinen an Tauwerk vorhanden sein?`

Reservetauwerk, Wurfleine, Schlepptrosse und Ankerleine.

# Nummer 18:

`In welchen Publikationen finden Sie amtliche Informationen über die Ausrüstung und Sicherheit von Sportbooten, die auch bei der Beurteilung von Sportbootunfällen herangezogen werden?`

"Sicherheit im See- und Küstenbereich, Sorgfaltsregeln für Wassersportler", herausgegeben vom BSH,

"Sicherheit auf dem Wasser, Leitfaden für Wassersportler", herausgegeben vom Bundesministerium für Verkehr, Bau und Stadtentwicklung.

# Nummer 19:

`Warum müssen auf Yachten zusätzlich zu elektrisch oder motorgetriebenen Lenzpumpen auch Handlenzpumpen vorhanden sein?`

Weil sie auch bei Strom- und Motorausfall betätigt werden können.

# Nummer 20:

`Warum ist Flüssiggas (Propan, Butan) auf einer Yacht besonders gefährlich?`

Es ist schwerer als Luft, sinkt nach unten und bildet mit Luft ein explosives Gemisch; es kann sich im Schiffsinneren (z. B. in der Bilge) sammeln.

# Nummer 21:

`Welche 4 Bedienelemente besitzt ein mit Handpumpe betriebenes Bord-WC auf einer Yacht?`

Seeventil und Spülwasserschlauch (Seewasser),

Handpumpe für Toilettenspülung,

Hebel zur Unterbrechung der Seewasserzufuhr (Handpumpe dient dann nur noch zum Abpumpen),

Abwasserschlauch (via Fäkalientank) zum Seeventil.

# Nummer 22:

`Beschreiben Sie in 5 Schritten die Bedienung eines Bord-WC auf einer Yacht.`

Seeventil für Seewasserspülung öffnen,

Handpumpe betätigen, sodass das Becken gespült wird und gleichzeitig die Fäkalien abfließen - ausgiebig spülen,

Seewasserzufuhr unterbrechen (Hebel umlegen),

Becken mit Handpumpe leer pumpen,

Seeventile für Zu- und Abfluss schließen.

# Nummer 23:

`Was versteht man unter der "Stabilität" eines Schiffes?`

Unter Stabilität eines Schiffes versteht man seine Eigenschaft, in aufrechter Lage zu schwimmen und sich aus einer Krängung wieder aufzurichten.

# Nummer 24:

`Wovon hängt die Stabilität eines Schiffes in ruhigem Wasser ab? Nennen Sie Beispiele für äußere Momente, welche die Stabilität beanspruchen.`

Die Stabilität eines Schiffes hängt ab von:
seiner Geometrie (Form),

der Gewichtsverteilung im Schiff (Ausrüstung, Crew, Ballast).
Beispiele für eine Beanspruchung der Stabilität sind krängende Momente durch Seitenwind, Trossenzug oder Drehkreisfahrt bei schnellen Motoryachten.

# Nummer 25:

`Wovon hängt eine in ruhigem Wasser vorhandene Stabilität zusätzlich in schwerem Wetter ab?`

Die Stabilität in schwerem Wetter hängt zusätzlich von Wind und Seegang, besonders von brechenden Wellen ab.

# Nummer 26:

`Was versteht man unter 1. Formschwerpunkt (F)? 2. Massenschwerpunkt (Gewichtsschwerpunkt, G)? Welche Kräfte wirken in den beiden Punkten?`

Im Formschwerpunkt F kann man sich die Masse des vom Schiff verdrängten Wassers vereinigt denken. In F wirkt die Auftriebskraft senkrecht zur Wasseroberfläche nach oben.

Im Massenschwerpunkt G kann man sich die Masse des Schiffes einschließlich Ausrüstung und Besatzung vereinigt denken. In G wirkt die Gewichtskraft senkrecht zur Wasseroberfläche nach unten.

# Nummer 27:

`Was geschieht bei einer Neigung des Schiffes, z. B. durch seitlichen Winddruck, solange sich die Lage des Massenschwerpunktes (Gewichtsschwerpunktes) nicht verändert? (Begründung!)`

Der Formschwerpunkt F wandert zur geneigten Seite aus, weil dort ein größerer Teil des Bootskörpers unter Wasser gelangt. Die Wirklinie der Auftriebskraft bekommt dadurch einen seitlichen Abstand zur Wirklinie der Gewichtskraft. Es entsteht ein Kräftepaar, der seitliche Abstand zwischen den Wirklinien ist der Hebelarm. Es entsteht ein aufrichtendes Moment, welches gleich dem Produkt aus Gewichtskraft und Hebelarm ist.

# Nummer 28:

`Was verstehen Sie unter "Trimm"?`

Der Trimm ist der Unterschied zwischen dem vorderen und dem achteren Tiefgang.

# Nummer 29:

`Nennen Sie mögliche Trimmlagen einer Motoryacht.`

Ist der vordere Tiefgang größer als der achtere, ergibt dies einen vorlichen Trimm. Ist der achtere Tiefgang größer als der vordere, ergibt dies einen achterlichen Trimm. Sind beide gleich, liegt eine Yacht auf ebenem Kiel.

# Nummer 30:

`Was verstehen Sie unter "Rumpfgeschwindigkeit" und wovon ist sie abhängig?`

Rumpfgeschwindigkeit ist die rechnerische Höchstfahrt eines Verdrängers. Sie ist abhängig von der Wasserlinienlänge.

# Nummer 31:

`Erklären Sie die wesentlichen Vorteile des Dieselmotors gegenüber dem Benzinmotor.`

Der zum Betrieb erforderliche Kraftstoff (Diesel) ist weniger feuergefährlich als der für einen Benzinmotor.

Er hat einen geringeren Kraftstoffverbrauch.

# Nummer 32:

`Erklären Sie die wesentlichen Nachteile des Benzinmotors gegenüber dem Dieselmotor.`

Das Benzin-Luft-Gemisch birgt Explosions- und Brandgefahr im Schiff.

Die Zündanlage kann störempflindlich gegen Feuchtigkeit und Nässe sein.

Der Motor hat einen höheren Kraftstoffverbrauch als der Dieselmotor.

# Nummer 33:

`Wozu dient das Wendegetriebe eines Motors?`

Zum Ein- und Auskuppeln des Propellers,

zum Umsteuern des Propellers auf Rückwärtsfahrt,

zur Drehzahluntersetzung.

# Nummer 34:

`Wodurch unterscheiden sich Einhebel- und Zweihebelschaltung?`

Bei Einhebelschaltung werden Gas und Getriebe gleichzeitig bedient.

Bei Zweihebelschaltung werden Gas und Getriebe mit 2 Hebeln getrennt bedient.

# Nummer 35:

`Erklären Sie die Grundstruktur des Zweikreis-Kühlsystems bei der Motorkühlung.`

Das Zweikreis-Kühlsystem besteht aus einem geschlossenen inneren Süßwasserkreislauf mit eigenem Kühlwassertank und einem offenen Seewasserkreislauf. Beide Kreisläufe sind in einem thermostatgeregelten Wärmetauscher wärmetechnisch miteinander verbunden. Der innere Süßwasserkreislauf durchfließt den Motor.

# Nummer 36:

`Welchen Vorteil hat die Zweikreis-Kühlung gegenüber der Einkreis-Kühlung?`

Im Zweikreis-Kühlsystem wird im inneren geschlossenen Kühlwasserkreislauf Süßwasser gefahren. Dem geschlossenen Kühlwassersystem können Zusätze (z. B. Frostschutzmittel) zugegeben werden. Ablagerungen durch Fremdwasser werden verhindert.
Durch eine thermostatische Regelung des Wärmeaustausches zwischen innerem und äußerem Kreislauf erreicht der Motor schneller seine Betriebstemperatur, diese wird auch konstant gehalten.

# Nummer 37:

`Was sollte nach dem Anlassen der Maschine kontrolliert werden?`

Kühlwasserdurchlauf,

Öldruck und Ladung,

Motorengeräusche und

Auspuffgase.

# Nummer 38:

`Was können erste Störungsanzeichen im Motorbetrieb sein?`

Ungewöhnliche und fremde Motorengeräusche, Vibrationen, Verfärbung der Abgase, Aufleuchten der Ladekontrolle bzw. Öldruckkontrolle und die entsprechenden akustischen Warnungen.

# Nummer 39:

`Der Dieselmotor Ihres Bootes startet nicht. Welche Teile der Kraftstoffanlage sollten überprüft werden?`

Kraftstofffüllung,

Kraftstoffabsperrhahn,

Kraftstoffschläuche,

Kraftstofffilter,

Kraftstoffpumpe.

# Nummer 40:

`Der Benzinmotor Ihres Bootes startet nicht. Welche Teile der Kraftstoffanlage sollten überprüft werden?`

Kraftstofffüllung,

Kraftstoffabsperrhahn,

Kraftstoffschläuche,

Kraftstofffilter,

Kraftstoffpumpe,

Vergaser.

# Nummer 41:

`Welche Ursachen können zu einer Anzeige eines zu geringen Öldrucks führen?`

Ein verstopftes Ölsieb (Ölwanne),

ein zu geringer Ölstand,

ein verstopfter Ölfilter,

eine defekte Ölpumpe,

ein defektes Öldruckventil,

ein defektes Anzeigegerät.

# Nummer 42:

`Welche Ursachen kann das Aufleuchten der Warnlampe der Batterie-Ladekontrolle während des Betriebes zur Folge haben?`

Die Kabelverbindungen sind unterbrochen (oxydiert, lose oder gebrochen).

Der Keilriemen zum Antrieb der Lichtmaschine ist defekt und es erfolgt keine Stromerzeugung.

Der Regler oder die Lichtmaschine können defekt sein.

# Nummer 43:

`Welche Fehlerursachen kann eine schwarze Färbung der Auspuffgase haben?`

Unvollständige Verbrennung durch:
kalten Motor,

verschmutzten Luftfilter,

schlechte Kraftstoffqualität,

verstellte Einspritzpumpe,

Überlastung des Motors.

# Nummer 44:

`Welche Fehlerursachen kann eine weiße Färbung der Auspuffgase haben?`

Verdampfung von Wasser durch z. B.

Kondensat im Auspuffsystem bei noch kaltem Motor,

gerissenen Zylinderkopf,

defekte Zylinderkopfdichtung.

# Nummer 45:

`Welche Vorsichtsmaßnahmen müssen beim Tanken und Umfüllen von Brennstoffen getroffen werden?`

Maschine abstellen.

Offenes Feuer löschen (Rauchen einstellen).

Keine elektrischen Schalter betätigen.

Alle Öffnungen schließen.

Tragbare Tanks möglichst außerhalb des Bootes befüllen.

# Nummer 46:

`Welche Maßnahmen sind vor dem Anlassen eines Dieselmotors zu treffen?`

Hauptstromschalter einschalten.

Kraftstoff- und Kühlwasserventile öffnen.

Getriebe auf "neutral" stellen.

Kühlwasser prüfen (Zweikreis-Kühlsystem).

# Nummer 47:

`Welche Maßnahmen sind vor dem Anlassen eines Benzinmotors zu treffen?`

Hauptstromschalter einschalten.

Motorraum mit Bilge entlüften.

Kraftstoff- und Kühlwasserventile öffnen.

Getriebe auf "neutral" stellen.

Kühlwasser prüfen (Zweikreis-Kühlsystem).

# Nummer 48:

`Wie kontrollieren Sie den ordnungsgemäßen Betrieb des Motors?`

Kontrolle der Anzeigegeräte auf:
Öldruck und Öltemperatur,

Kühlwassertemperatur,

Motorendrehzahl und

Batterieladung.
Außerdem auf Motorgeräusche, Vibrationen und Farbe der Auspuffgase achten.

# Nummer 49:

`Welche Maßnahmen treffen Sie nach dem Abstellen des Motors?`

Kraftstoffventil schließen.

Hauptstromschalter (Batterie) ausschalten.

Seeventile schließen.

# Nummer 50:

`Während Sie unter Maschine laufen, steigt plötzlich die Kühlwassertemperatur stark an. Ihre Yacht ist mit einem Saildrive-Antrieb ausgestattet.`
1. Welche typische Ursache hat der Temperaturanstieg, wenn eine technische Störung unwahrscheinlich ist?
2. Wie können Sie die Störung einfach beheben?

Fremdkörper (Folienstücke, Plastiktüten, Pflanzenteile o. Ä.) haben den Kühlwassereinlass verstopft.

Mehrmals abwechselnd vor- und zurückfahren, sodass sich die Fremdkörper vom Kühlwassereinlass lösen.

# Nummer 51:

`Welche Fehlerursachen kann eine blaue Färbung der Auspuffgase haben?`

Zweitaktmotor:
zu fettes Benzin-Öl-Gemisch. Es ist zu viel Schmieröl im Gemisch.

Viertaktmotor:
zu viel Schmieröl, Ölabstreifringe bzw. Kolbenringe defekt.

In beiden Fällen verbrennt/verdampft Schmieröl.

# Nummer 52:

`Erklären Sie die Arbeitsweise in Bezug auf die Zündung beim Ottomotor und Dieselmotor.`

Beim Ottomotor wird das zündfähige Benzin-Luft-Gemisch im Vergaser durch Einspritzung erzeugt und mit Fremdzündung durch die Zündkerze gezündet.

Beim Dieselmotor wird die angesaugte Luft hoch verdichtet und so erwärmt, dass der eingespritzte Dieselkraftstoff sich durch Eigenzündung in dieser komprimierten Luft entzündet.

# Nummer 53:

`Weshalb sollte dringend vermieden werden, dass beim Dieselmotor der Kraftstofftank leer gefahren wird?`

In die Einspritzpumpe/Kraftstoffleitung würde Luft gelangen. Startversuche nach dem Tanken wären erfolglos. Vor dem Tanken müssten die Leitung und die Einspritzpumpe entlüftet werden. Ablagerungen sowie Kondenswasserbildung durch Temperaturschwankungen können entstehen.

# Nummer 54:

`Während der Fahrt lässt plötzlich die Motordrehzahl abrupt nach und der Motor geht beim Zurücklegen des Gashebels gänzlich aus. Was kann die Ursache sein?`

Es ist möglicherweise ein schwimmender Fremdkörper (Leine, Trosse, Plane, Persening o. Ä.) in den Propeller geraten und behindert bzw. blockiert ihn.

# Nummer 55:

`In welchen Bereichen werden an Bord Batterien eingesetzt?`

Zum Starten und für das Bordnetz.

# Nummer 56:

`Was ist beim Laden von Batterien dringend zu beachten?`

Die Batteriekästen bzw. -räume müssen ausreichend be- und entlüftet sein.

# Nummer 57:

`1. Was bedeutet die Angabe einer Batteriekapazität "2 x 60 Ah"? (Begründung!) 2. Welche Nettokapazität steht in dem Fall zur Verfügung? (Begründung!)`

Es handelt sich um 2 Batterien (Akkus) mit jeweils 60 Amperestunden, insgesamt also 120 Ah Nennkapazität.

Dem entspricht eine Nettokapazität von etwa 72 Ah, da ein Akku kaum über 80 % seiner Nennkapazität geladen werden kann.

# Nummer 58:

`Geben Sie die benötigte Strommenge (in Amperestunden) an, um bei einer 12-Volt-Anlage zwei Verbraucher mit je 24 Watt 10 Stunden betreiben zu können (mit Angabe der Berechnung)!`

Benötigte Strommenge je Verbraucher:
24 : 12 = 2 Ampere mal Anzahl der Verbraucher mal Stunden ergibt:
2 x 2 A x 10 h = 40 Ah.

# Nummer 59:

`Was könnte zu möglichen Motorschäden bis hin zum Kolbenfresser führen?`

Zu wenig Kühlwasser, Dampfblasen im Kühlwasserschauglas,

Kühlwassertemperatur zu hoch,

zu niedriger oder stetig fallender Öldruck,

zu wenig oder nicht geeignetes Öl,

fallende Drehzahl, zitternde Nadel im Drehzahlmesser, klopfende Motorgeräusche.

# Nummer 60:

`Wie muss Tauwerk beschaffen sein, das für Festmacherleinen, Anker- und Schlepptrossen verwendet wird?`

Es muss bruchfest und elastisch sein.

# Nummer 61:

`Wodurch können Sie verhindern, dass Festmacherleinen durch Schamfilen in Klüsen oder an Kanten an der Pier beschädigt werden?`

Durch einen gegen Verrutschen gesicherten Plastikschlauch, der über den Festmacher an der Scheuerstelle gezogen wird, hilfsweise mit Tuchstreifen.

# Nummer 62:

`Was müssen Sie hinsichtlich der Festigkeit bedenken, wenn Sie Leinen zusammenknoten?`

Beim Knoten können Festigkeitsverluste bis zu 50 % auftreten.

# Nummer 63:

`Wodurch können Sie verhindern, dass bei Tauwerk aus unterschiedlichem Innen- und Außenmaterial die Seele in den Mantel rutscht?`

Durch einen genähten Takling.

# Nummer 64 (Abbildung fehlt):

`Wie sind längsseits liegende Fahrzeuge festzumachen?Ergänzen Sie die Skizze und benennen Sie die Leinen.`

Zeichnung zur Ergänzung Frage 64

Achterleine,
Achterspring,
Vorspring,
Vorleine.
Zeichnung nach Ergänzung Frage 64

# Nummer 65 (Abbildung fehlt):

`Wie können Sie mit Hilfe von zwei Fendern und einem Fenderbrett Ihr Boot festmachen, wenn die Pier mit vorspringenden Pfählen versehen ist? Ergänzen Sie die Skizze mit Leinen.`

Zeichnung vor Ergänzung Frage 65

 Zeichnung nach Ergänzung Frage 65

# Nummer 66:

`Was ist beim Reinigen eines mit Antifouling behandelten Unterwasserschiffes zu beachten?`

Umweltschutzbestimmungen beachten, d. h. das Schiff nur auf einem entsprechend ausgerüsteten Reinigungsplatz abspritzen und Wasser und Schmutz auffangen, also nicht in die Kanalisation lassen.

# Nummer 67:

`Ein funktionsfähiges elektrisches Gerät arbeitet an Bord nicht. Nennen Sie häufige Ursachen und was kann zur Behebung getan werden?`

Schlechte Kontakte und Korrosion.

Kontakte fest anziehen, korrodierte Stellen mit feinstem Schleifpapier säubern, Kontaktspray verwenden.

# Nummer 68:

`Warum müssen Schäden im Gelcoat unverzüglich beseitigt werden?`

Das Laminat unter der Gelcoatschicht nimmt Wasser auf und wird dadurch geschädigt.

# Nummer 69:

`Welche Daten sollten an Bord mindestens im Logbuch dokumentiert werden?`

Namen und Funktionen der Crewmitglieder,

Beginn und Ende einer Fahrt und

in angemessenen Zeitabständen: Position, Kurs, Geschwindigkeit, Strömung, Wetter, Luftdruck.

# Nummer 70:

`Welche Sicherheitsmaßnahmen müssen auch bei ruhigem Wetter bei Nachtfahrten beachtet werden?`

Bei jeder Tätigkeit an Deck Rettungswesten und Sicherheitsgurt tragen.

Nur mit am Schiff eingepickter Sicherheitsleine über Deck gehen.

# Nummer 71:

`Was verstehen Sie unter "Radeffekt" des Schiffspropellers (Schraube)?`

"Radeffekt" ist die seitliche Versetzung des Hecks durch die drehende Schraube.

# Nummer 72:

`Mit welchen 4 Angaben werden Propeller auf Yachten beschrieben?`

Anzahl der Flügel, Größe ihrer Fläche, Durchmesser und Steigung.

# Nummer 73:

`Wie wirkt der rechts- bzw. linksdrehende Propeller auf das Schiffsheck bei Rückwärtsfahrt?`

Ein rechtsdrehender Propeller versetzt das Heck bei Rückwärtsfahrt nach Backbord, ein linksdrehender nach Steuerbord.

# Nummer 74:

`1. Wie drehen bei einem Zweischrauber in der Regel die Propeller? 2. Welchen manövriertechnischen Vorteil haben Yachten mit 2 Propellern?`

Gegenläufig.

Mit 2 Propellern kann man nahezu auf der Stelle drehen, indem man einen Propeller vorwärts und einen Propeller rückwärts arbeiten lässt. Die Manövrierfähigkeit wird dadurch verbessert.

# Nummer 75:

`Warum gibt es bei einem Zweischrauben-Schiff (in der Regel) keinen Radeffekt?`

Die Drehrichtungen der Schrauben sind gegenläufig. So heben sich die jeweiligen Radeffekte gegenseitig auf.

# Nummer 76:

`Was ist ein Bugstrahlruder und wozu dient es?`

Eine im Bug einer Yacht befindliche Röhre mit einem Propeller, mit dem ein Querschub und damit ein Drehen des Buges bei geringen Vorausgeschwindigkeiten erreicht werden kann.

# Nummer 77:

`Wann sollte aus Gründen der Sicherheit auf Motoryachten der Außenfahrstand besetzt werden?`

Bei verminderter Sicht zum Wahrnehmen der Schallsignale.

Bei Manövern in engen Gewässern zur besseren Rundumsicht.

# Nummer 78:

`Warum empfiehlt sich beim Schleppen die Verwendung einer Hahnepot und wo sollte sie belegt werden?`

Eine Hahnepot verteilt die Zugkräfte auf mehrere Belegpunkte für die Schleppleine an der Durchführung an Bug und auf den beiden Bootsseiten.

Zum Belegen eignen sich zumeist kräftigere Klampen für die Vorspring auf den Bootsseiten.

# Nummer 79:

`Sie wollen in eine Box einlaufen. Wie bereiten Sie die Achterleinen vor und machen sie fest?`

Achterleinen mit Auge versehen. Möglichst fühzeitig über die Pfähle legen, bei seitlichem Wind zuerst über den Luvpfahl.

# Nummer 80:

`Welche Vorbereitung haben Sie für ein Anlegemanöver zu treffen?`

Crew für Manöver einteilen.

Leinen und Fender bereitlegen.

# Nummer 81:

`Beschreiben Sie das Fahrmanöver mit einem Zweischrauben-Schiff bei einer Drehung auf engem Raum über Steuerbord.`

Ruder hart Steuerbord, Steuerbordmaschine rückwärts, Backbordmaschine voraus.

# Nummer 82:

`Bei welchen Manövern können Sie ein Bugstrahlruder sinnvoll einsetzen?`

Beim An- und Ablegen.

Beim Drehen auf engem Raum.

# Nummer 83:

`Sie liegen längsseits mit der Stuerbordseite an einer Pier. Beschreiben Sie ein Ablegemanöver unter gleichzeitigem Einsatz von Bugstrahlruder und Maschine.`

Hebel für Bugstrahlruder nach Backbord legen, sodass der Bug von der Pier weggedrückt wird (nach Backbord schwenkt) und gleichzeitig

Ruderlage deutlich nach Steuerbord und langsame Fahrt voraus, sodass das Heck nach Backbord ausschwenkt.
So wird das Schiff fast parallel von der Pier abgedrückt.

# Nummer 84:

`Warum kann das Anlaufen eines Hafens bei auflandigem Starkwind bzw. schwerem Wetter gefährlich werden?`

Gefahr durch Grundseen bzw. Kreuzseen. Möglichkeit von Querstromwirbeln.

# Nummer 85:

`Sie werden in Küstennähe von einem Sturm überrascht. Wie verhalten Sie sich mit einer Motoryacht?`

Möglichst rasch versuchen, Hafen oder Landschutz anzulaufen.

Ggf. Motoryacht mit langsamer Fahrt gegen die See halten.

Sicherheitsmaßnahmen für die Besatzung treffen.

# Nummer 86:

`Worauf müssen Sie achten, wenn Sie in Tidengewässern längsseits einer Pier festgemacht haben?`

Die Wassertiefe muss auch bei Niedrigwasser ausreichen oder sicheres Aufsetzen muss gewährleistet sein.

Die Leinen müssen für den Tidenstieg oder -fall ausreichend lang sein. Bei größerem Tidenhub darf das Fahrzeug keinesfalls unbeaufsichtigt bleiben.

# Nummer 87:

`Sie fahren bei frischem Wind und mitlaufendem Strom (Wind gegen Strom) nach Luv. Welche Auswirkungen hat ein gegen den Wind setzender Strom auf den Seegang?`

Durch den Strom entsteht eine kurze, steile und kabbelige See.

# Nummer 88:

`Wie wirkt sich mitlaufender Strom auf die Fahrt eines Fahrzeugs und die Loganzeige aus?`

Der Strom erhöht die Fahrt über Grund.

Das Log zeigt dies nicht an.

# Nummer 89:

`Welcher Kurswinkel ist bei schwerer See am besten geeignet, das Aufschlagen des Bootes zu verringern?`

Ein Kurswinkel von 20 bis 25° bezogen auf die Seegangsrichtung ist am besten geeignet, hartes Aufschlagen zu verringern.

# Nummer 90:

`Welche Hilfsmittel können Sie einsetzen, um einen Überbordgefallenen an Deck zu bekommen?`

Bewegliche (und gesicherte) Badeleiter, beschwerte Trittschlinge, Rettungstalje, Bergegurt.

# Nummer 91:

`Welche Sofortmaßnahmen sind einzuleiten, wenn jemand über Bord gefallen ist?`

Ruf: "Mensch über Bord!",

Rettungsmittel zuwerfen,

Ausguck halten, Mann im Auge behalten,

Maschine starten,

"Mensch-über-Bord-Manöver" einleiten,

Notmeldung abgeben,

ggf. Markierungsblitzboje werfen,

ggf. MOB-Taste eines satellitengestützten Navigationsgerätes drücken,

Bergung durchführen.

# Nummer 92:

`Welche Maßnahmen können gegen das Überbordfallen getroffen werden?`

Sicherheitsgurte anlegen und einpicken.

Anbringen von Strecktau oder Laufleinen von Bug zum Heck.

Crew auf Befestigungspunkte (Einpickpunkte für Karabinerhaken) hinweisen.

# Nummer 93:

`Nennen Sie die grundsätzlichen Schritte und ihre Ziele zur Rettung einer über Bord gegangenen Person.`

Maschine starten,

Suche, Herstellung eines Sichtkontaktes zur über Bord gegangenen Person,

"Mensch-über-Bord-Manöver", Annäherung an die im Wasser treibende Person und Herstellung einer ersten Leinenverbindung,

Bergung, sicheres und schnelles Anbordnehmen der Person,

Erste Hilfe, Betreuung,

ggf. Notalarm abgeben.

# Nummer 94:

`Mit welchen Hilfsmitteln können Sie den Bezugspunkt (internationaler Begriff: Datum) für die Suche nach einem Überbordgefallenen sichern?`

Markierungsblitzboje,

MOB-Taste eines satellitengestützten Navigationsgerätes (z. B. GPS).

# Nummer 95:

`Was gehört u. a. zur Sicherheitsausrüstung z. B. einer 10-m-Yacht? Nennen Sie mindestens 6 Ausrüstungsgegenstände.`

Lenzpumpen und Pützen,

Lecksicherungsmaterial,

Feuerlöscher,

Werkzeug und Ersatzteile,

Seenotsignalmittel,

Handlampen,

Funkeinrichtung,

Anker,

Erste-Hilfe-Ausrüstung,

Radarreflektor und

Rettungsmittel.

# Nummer 96:

`Was gehört zur Sicherheitsausrüstung der Besatzung in der Küstenfahrt?`

Rettungsweste und Sicherheitsgurt (Lifebelt) für jedes Besatzungsmitglied,

Rettungsfloß (Rettungsinsel),

Rettungskragen mit Tag- und Nachtsignal und

Erste-Hilfe-Ausrüstung mit Anleitung.

# Nummer 97:

`Wie erhalten Sie Kenntnis über das nächste Wartungsdatum eines Rettungsfloßes?`

Die runde, auf der Insel klebende farbige Serviceplakette oder das bei der letzten Wartung mitgelieferte Zertifikat geben Auskunft über den nächsten Wartungstermin.

# Nummer 98:

`Worauf müssen Sie bei Ihren Automatikrettungswesten hinsichtlich der Funktionssicherheit achten?`

Auf regelmäßige Wartung. Wartungsfälligkeit erkennbar an der farbigen Serviceplakette.

# Nummer 99:

`Was ist auf Deck einer Yacht ein Strecktau (auch Laufleine genannt) und wozu dient es?`

Ein neben der Fußreling verlaufender Draht, Gurt oder eine starke Leine zwischen Cockpit und Vorschiff straff gespannt zum Einpicken der Sicherheitsleine (Lifebelts).

# Nummer 100:

`Welche Seenotsignalmittel sollten Sie an Bord haben? Nennen Sie mindestens 6 Beispiele.`

Handfackeln, rot,

Handraketen, rot,

Rauchfackeln oder Rauchtopf, orange,

Signalpistole mit Munition,

Seewasserfärber,

Signalflaggen N und C,

Signallampe,

Seenotfunkboje.

# Nummer 101:

`Welche Feuerlöscheinrichtungen sollten an Bord sein?`

Feuerlöscher (ABC-Pulverlöscher und eventuell CO2-Löscher),

Pütz zum Löschen von Bränden fester Stoffe,

Feuerlöschdecke,

Löschdurchlass für geschlossene Motorräume, der das Löschen von Bränden mit CO2-Löschern ohne Sauerstoffzutritt ermöglicht.

# Nummer 102:

`Welche Feuerlöscharten sind für Sportboote geeignet? Wie und wo sind sie an Bord unterzubringen?`

Der ABC-Pulverlöscher, für geschlossene Motorräume der CO2-Löscher.

Der Feuerlöscher muss gebrauchsfertig und leicht erreichbar sein, CO2-Löscher nicht im Schiffsinneren unterbringen (Erstickungsgefahr bei Leckage).

Er soll in der Nähe der Maschinenräume, der Kombüse sowie der Koch- oder Heizstelle montiert sein.

# Nummer 103:

`Wie wird die ständige Funktionssicherheit eines Feuerlöschers sichergestellt?`

Durch Einhaltung des vorgeschriebenen Prüftermins, ersichtlich aus der Prüfplakette.

Der Feuerlöscher muss vor Feuchtigkeit und Korrosion geschützt werden.

# Nummer 104:

`Wie wird ein Brand an Bord wirksam bekämpft?`

Alle Öffnungen schließen,

Brennstoffzufuhr (Hauptschalter) unterbrechen,

Feuerlöscher erst am Brandherd betätigen,

Feuer von unten und von vorn bekämpfen,

Löschdecke einsetzen,

Flüssigkeitsbrände nicht mit Wasser bekämpfen.

# Nummer 105:

`Was ist vor Reisebeginn beim Seeklarmachen zu überprüfen und zu beachten? Nennen Sie mindestens 6 Beispiele.`

Seetüchtigkeit der Yacht,

Treibstoffvorrat,

Navigationsunterlagen,

Sicherheitseinweisung der Besatzung,

Rettungsmittel,

Seenotsignale,

Trinkwasser- und Proviantvorräte,

Funktionsfähigkeit des Motors,

Funktionsfähigkeit der elektronischen Navigationsgeräte,

Lenzeinrichtungen,

Feuerlöscher,

Boots- und Personalpapiere.

# Nummer 106:

`Was gehört zur Sicherheitseinweisung der gesamten Besatzung vor Reisebeginn? Nennen Sie mindestens 6 Beispiele.`

Einweisung in Gebrauch und Bedienung

der Rettungswesten und Sicherheitsgurte,

des Rettungsfloßes,

der Signalmittel,

der Lenzpumpen,

der Seeventile und des Bord-WC,

der Kocheinrichtung,

der Feuerlöscher,

der Motoranlage,

der Elektroanlage,

des Rundfunkgerätes und der UKW-Seefunkanlage,

Verhalten bei "Mensch-über-Bord",

Erkennen der Seekrankheit und entsprechendes Verhalten.

# Nummer 107:

`In welche technischen Einrichtungen/Ausrüstungen muss der Schiffsführer die Besatzung vor Reiseantritt unbedingt einweisen? Nennen Sie mindestens 6 Beispiele.`

Ankergeschirr,

Lenzeinrichtung,

Feuerlöscheinrichtungen,

Motoranlage,

Seeventile,

UKW-Seefunkanlage,

MOB-Taste vom satellitengestützten Navigationsgerät (z. B. GPS),

Seenotsignalmittel,

Notrudereinrichtung.

# Nummer 108:

`Welche Sicherheitsmaßnahmen sind vor jedem Auslaufen durchzuführen? Nennen Sie mindestens 6 Beispiele.`

Wetterbericht einholen,

Kontrolle der Sicherheitsausrüstung,

Kontrolle von Motor und Schaltung,

Kontrolle der nautischen Geräte,

Kontrolle der Bilge,

Überprüfen des Wasser- und Kraftstoffvorrats,

Kontrolle der Schall- und Lichtsignaleinrichtung,

Kontrolle der Navigationslichter,

Bereitlegen der aktuellen Seekarten und nautischen Veröffentlichungen.

# Nummer 109:

`Warum sollten alle Crewmitglieder Lage und Funktion sämtlicher Pumpen und Ventile kennen?`

Damit im Bedarfsfall sie jeder bedienen kann.

# Nummer 110:

`Warum sollte die Crew in die Funktion des Bord-WC eingewiesen werden?`

Weil durch unsachgemäße Bedienung Wasser ins Bootsinnere gelangen kann.

# Nummer 111:

`Warum sollte die Crew vor Reisebeginn in die Funktion des Ankergeschirrs und die Durchführung eines Ankermanövers eingewiesen werden?`

Damit jeder den Anker sicher ausbringen und einholen kann.

# Nummer 112:

`Wie verhalten Sie sich, wenn Ihr Schiff leckgeschlagen ist?`

Meldung abgeben.

Je nach Erfordernissen Fahrt aus dem Schiff nehmen.

Lenzpumpen betätigen, Lecksuche, Leck mit Bordmitteln abdichten.

Küste bzw. flaches Wasser ansteuern.

Fahrzeug so trimmen, dass Leckstelle aus dem Wasser kommt bzw. möglichst wenig unter Wasser ist.

# Nummer 113:

`Was tun Sie, wenn Ihr Schiff leckgeschlagen ist und das Wasser im Schiff trotz aller Maßnahmen weiter steigt?`

Notzeichen geben, Funkmeldung abgeben, ggf. Radartransponder einschalten.

Verlassen des Bootes vorbereiten, Rettungswesten anlegen, Rettungsfloß klarmachen.

Wenn möglich, ruhiges Flachwasser anlaufen und Schiff auf Grund setzen.

# Nummer 114:

`Welche Folgen können Grundberührungen und harte Stöße, z. B. bei Anlegemanövern oder Kollisionen mit treibenden Gegenständen, haben?`

Eine Beschädigung der Bordwand kann eintreten.

Es kann Sinkgefahr entstehen.

# Nummer 115:

`Welche grundsätzliche Verhaltensweise sollte beachtet und welche Maßnahmen sollten ergriffen werden, wenn Ihr Schiff in Seenot kommt?`

Ruhe bewahren und überlegt handeln.

Notalarm abgeben, ggf. Radartransponder einschalten.

Rettungsfloß klarmachen.

Rettungsweste und Sicherheitsgurt anlegen.

So lange wie möglich an Bord bleiben.

Wärmende Kleidung anziehen.

# Nummer 116:

`Welche Maßnahmen treffen Sie, bevor Sie von Ihrem Fahrzeug in ein Rettungsfloß übersteigen?`

Rettungsweste und Sicherheitsgurt anlegen.

Wärmende Kleidung anziehen.

Nach Möglichkeit vorher reichlich warme Flüssigkeit trinken.

Soweit noch nicht geschehen, Proviant, Wasser, Seenotsignalmittel und ggf. Seenotfunkbake, Radartransponder und UKW-Handsprechfunkgeräte in das Rettungsfloß bringen.

# Nummer 117:

`Warum sollte ein sinkendes Schiff im Notfall so spät wie möglich verlassen werden?`

Die Überlebensmöglichkeiten sind auf dem Schiff größer.

Ein Schiff ist besser zu orten.

Einstieg in das Rettungsfloß und Aufenthalt können sehr schwierig sein.

# Nummer 118:

`Erklären Sie die Handhabung der Hubschrauberrettungsschlinge im Einsatz!`

Bei offener Rettungsschlinge: zuerst den Karabinerhaken einpicken.

Mit dem Kopf und beiden Armen in die Rettungsschlinge einsteigen.

Die Arme müssen nach unten gedrückt werden und die Hände sind zu schließen.

Das Windenseil muss frei hängen, es darf nicht an Bord befestigt werden.

# Nummer 119:

`Wann dürfen Notzeichen gegeben werden?`

Nach Feststellung des Notfalles auf Anordnung des Schiffsführers; bei unmittelbarer Gefahr für das Schiff oder die Besatzung, die ohne fremde Hilfe nicht überwunden werden kann.

# Nummer 120:

`Wann darf ein UKW-Sprechfunkgerät auch ohne entsprechenden Befähigungsnachweis benutzt werden?`

In Notfällen.

# Nummer 121:

`Worauf ist zu achten, wenn Crewmitglieder seekrank sind?`

Aufenthalt im Cockpit beaufsichtigen und Crewmitglieder gegen Überbordfallen sichern,

Flüssigkeitsverlust ausgleichen (Wasser),

Crewmitglied anhalten, zur Küste oder zum Horizont zu schauen,

mit Arbeiten beschäftigen.

# Nummer 122:

`Wozu dient ein Reitgewicht (Gleitgewicht, Ankergewicht) beim Ankern?`

Es soll die Ankertrosse auf den Grund ziehen, damit der Anker nicht durch einen zu steilen Winkel aus dem Grund gebrochen wird. Es wirkt ruckdämpfend.

# Nummer 123:

`Warum sollte beim Verwenden einer Ankertrosse ein Kettenvorlauf benutzt werden?`

Damit der Zug auf den Anker nicht zu steil wird.

# Nummer 124:

`Welcher Ankergrund ist für die üblichen Leichtgewichtsanker`
1. gut geeignet?
2. mäßig geeignet?
3. ungeeignet?

Sand, Schlick, weicher Ton und Lehm,

harter Ton und Lehm,

steinige, verkrautete und stark schlammige Böden.

# Nummer 125:

`Was müssen Sie bei der Auswahl eines Ankerplatzes beachten?`

Der Ankerplatz sollte Schutz vor Wind und Wellen bieten.

Auf ausreichenden Platz zum Schwojen achten.

Mögliche Winddrehungen einplanen.

# Nummer 126:

`Welchen Ankergrund sollten Sie nach Möglichkeit meiden?`

Steinige, verkrautete und stark schlammige Böden.

# Nummer 127:

`Wie können Sie die Haltekraft eines Ankers erhöhen, wenn Sie auf engem Raum (z. B. zwischen zwei Stegen) nicht die erforderliche Kettenlänge stecken können?`

Mit einem Reitgewicht, um so den Anker besser am Boden zu halten.

# Nummer 128:

`Sie ankern in einer Bucht. Wie können Sie bei zunehmendem Wind die Haltekraft Ihres Ankers verbessern?`

Mehr Trosse oder Kette stecken,

Reitgewicht verwenden.

# Nummer 129:

`Sie wollen auf verkrautetem Grund ankern. Ihnen steht ein Leichtgewichtsanker und ein Stockanker zur Verfügung. Welchen benutzen Sie und warum?`

Den Stockanker, weil er sich insbesondere auch aufgrund seines höheren Gewichtes besser eingräbt.

# Nummer 130:

`Wozu dient eine Ankerboje?`

Sie zeigt die Lage des Ankers an.

Mit der Trippleine kann das Bergen eines unklaren Ankers unterstützt werden.

# Nummer 131:

`Wie erkennen Sie, ob der Anker hält?`

Vibration von Kette oder Trosse prüfen,

Einrucken des Ankers prüfen,

durch wiederholte Peilungen und ggf. Schätzungen des Abstands zu anderen Schiffen oder zu Landmarken,

falls GPS vorhanden ist, die Ankeralarmfunktion einschalten.

# Nummer 132:

`Welche Ankerarten finden überwiegend auf Sportbooten Verwendung? Nennen Sie 3.`

Patentanker,

Stockanker (einklappbarer Stock),

Draggen (klappbare Flunken),

Pflugscharanker.

# Nummer 133:

`Nennen Sie 3 Ankertypen, die vom Germanischen Lloyd als Anker mit hoher Haltekraft anerkannt sind.`

Bruce-Anker, CQR-Anker, Danforth-Anker, D'Hone-Anker.

# Nummer 134:

`1. Welches sind die Vorteile einer Ankerkette gegenüber einer Ankerleine? 2. Wie kombiniert man auf Yachten häufig die Systeme?`

Die Kette unterstützt das Eingraben, verkleinert den Schwojeraum, wirkt ruckdämpfend, kann nicht an Steinen durchscheuern und erhöht die Haltekraft des Ankers.

Es wird zwischen Anker und Leine ein Kettenvorlauf von 3 bis 5 m gefahren.

# Nummer 135:

`1. Warum soll eine Ankerleine nicht an den Anker geknotet werden? 2. Warum muss die Ankerkette mit einem Taustropp am Schiff bzw. im Kettenkasten befestigt werden?`

Knoten reduzieren die Bruchlast einer Leine um bis zu 50 %.

Damit die Kette im Notfall schnell gekappt werden kann.

# Nummer 136:

`Sie wollen in einer Bucht ankern, in der das (ausreichend tiefe) Wasser unterschiedliche Färbungen zeigt. Wo wählen Sie den Ankerplatz? (Begründung!)`

Ich ankere auf hellem Wasser.

Begründung:
Der Grund ist hier sandig, der Anker hält gut. Dunkler Grund weist auf Bewuchs hin, wo der Anker schlecht hält.

# Nummer 137:

`Warum darf der Anker nicht zusammen mit seiner Leine am Ankerplatz über Bord geworfen werden?`

Die Leine könnte mit dem Anker vertörnen und dadurch das Eingraben des Ankers verhindern. Der Anker würde dann nicht halten.

# Nummer 138:

`Beschreiben Sie die Vorbereitung eines Ankermanövers.`

Auswählen eines geeigneten Ankerplatzes anhand der Seekarte bzw. des Seehandbuches (Meeresgrund/Wassertiefe geeignet?),

Ermitteln der Wind- und/oder Stromrichtung und -stärke,

Klarmachen des Ankergeschirrs und des Ankersignals,

mit langsamster Fahrt - Kurs gegen Strom bzw. Wind - einen Ankerplatz ansteuern.

# Nummer 139:

`Beschreiben Sie wichtige Elemente eines Ankermanövers.`

Geeigneten Ankerplatz festlegen/planen,

Ansteuerung planen (in der Regel gegen Wind und/oder Strom),

bei langsamer Fahrt rückwärts über Grund Anker fallen lassen,

Kette/Leine in Abhängigkeit vom Wetter/Strom bis auf das 3- bzw. 5fache der Wassertiefe stecken,

Ankerball/Ankerlicht setzen,

Ankerposition feststellen, dokumentieren und ausreichend kontrollieren.

# Nummer 140:

`Was müssen Sie bedenken, wenn ein großes Schiff auf Ihr Sportboot zukommt?`

Andere Manövrierfähigkeit (größere Drehkreise, längere Stoppstrecken),

u. U. eingeschränkte Sicht des anderen Fahrzeugs, insbesondere nach voraus,

Möglichkeit des Übersehenwerdens, weil man sich im Radarschatten befindet,

Beeinträchtigung durch Bugwellen des großen Schiffes.

# Nummer 141:

`Warum sollten Sie nicht zu dicht hinter dem Heck eines vorbeifahrenden Schiffes durchfahren?`

Sog und Hecksee kÖnnen das eigene Boot erheblich gefährden.

# Nummer 142:

`Was müssen Sie beim Passieren eines großen Schiffes bei dessen Kursänderungen, z. B. in einem kurvenreichen Fahrwasser, beachten?`

Bei einer Kursänderung schwenkt das Heck deutlich in die entgegengesetzte Richtung aus, also nach Backbord bei einer Kursänderung nach Steuerbord und umgekehrt.

# Nummer 143:

`Mit welchen Stoppstrecken und Stoppzeiten müssen Sie bei großen Schiffen in voller Fahrt rechnen und wovon hängen sie ab?`

Abhängig von Schiffstyp und -größe, Beladungszustand und Ausgangsgeschwindigkeit ist mit der 8- bis 12fachen Schiffslänge und bis zu 8 bis 12 Minuten Dauer (z. B. ein 300 m langes Containerschiff voll abgeladen mit 24 kn: Stoppstrecke ca. 2 sm, Stoppzeit ca. 12 Minuten) zu rechnen.

# Nummer 144:

`Wie reagiert ein großes Schiff, wenn bei ca. 20 kn Fahrt ein Ausweichmanöver durch Hartruderlage eingeleitet wird? Nach welcher Distanz verlässt es in etwa die alte Kurslinie?`

Der Steven bewegt sich in Richtung der Hartruderlage, das Heck schlägt relativ weit zur entgegengesetzten Richtung aus. Das Schiff verlässt mit seinem Heck erst nach mehreren Schiffslängen seine bisherige Kurslinie, bewegt sich also zunächst in der alten Kursrichtung fort. Diese Strecke kann bei 300 m langen Containerschiffen 1,5 bis 2,5 Schiffslängen, d. h. ca. 500 bis 600 m betragen.

# Nummer 145:

`Auf vielen großen Schiffen ist die Sicht nach vorne eingeschränkt. Welchen Abstand vor einem Schiff müssen Sie als nicht einsehbar mindestens berücksichtigen?`

Sichtbeschränkung nach voraus maximal 2 Schiffslängen oder 500 m.

# Nummer 146:

`Wie können Sie die Wahrscheinlichkeit erhöhen, im Radar von anderen Fahrzeugen gesehen zu werden?`

Durch einen möglichst hoch und fest angebrachten passiven Radarreflektor bzw. besser noch durch einen "aktiven" Radarreflektor.